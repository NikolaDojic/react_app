import React from "react";
import { connect } from "react-redux";
import { findForm } from "../../utils";

const ActiveTableIndicator = ({ tableName, dataCount }) => {
  return (
    <div className="ActiveTableIndicator">
      Aktivna tabela: {tableName}; Broj redova: {dataCount}
    </div>
  );
};

const mapStateToProps = state => ({
  tableName:
    (
      findForm(
        state.grid.activeTable || "",
        state.formGroups.formGroups || []
      ) || { title: "" }
    ).title || state.grid.activeTable,
  dataCount: (state.grid.data[state.grid.activeTable] || []).length
});

export default connect(mapStateToProps)(ActiveTableIndicator);
