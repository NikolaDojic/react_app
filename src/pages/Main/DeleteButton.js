import React from "react";
import { connect } from "react-redux";
import Button from "../../components/Button";
import deleteIcon from "../../assets/icons/delete.svg";
import { deleteSelected } from "../../actions/grid";
import { openModal } from "../../actions/modal";
import { findForm, isActionAllowed } from "../../utils";

const DeleteButton = ({
  onClick,
  isHidden,
  currentForm,
  tableName,
  isDisabled
}) => {
  return (
    <Button
      className={`DeleteButton${isDisabled ? " disabled" : ""}`}
      isHidden={isHidden}
      buttonKey="deleteButton"
      onClick={onClick(currentForm, tableName)}
    >
      Izbriši <img src={deleteIcon} alt="" />
    </Button>
  );
};
const mapStateToProps = state => ({
  currentForm: state.grid.currentTable,
  tableName: (
    findForm(state.grid.currentTable, state.formGroups.formGroups) || {
      title: ""
    }
  ).title,
  isHidden: !isActionAllowed(
    "d",
    state.grid.activeTable,
    state.formGroups.formGroups
  ),
  isDisabled: !state.grid.selectedRow[state.grid.activeTable]
});
const mapDispatchToProps = dispatch => ({
  onClick: (currentForm, tableName) => () => {
    dispatch(
      openModal({
        title: tableName,
        className: currentForm + "Delete",
        message: `Izbriši?`,
        iconType: "warning",
        buttons: [
          { text: "Ne", type: "cancel" },
          {
            text: "Da",
            type: "ok",
            action: () => dispatch(deleteSelected())
          }
        ]
      })
    );
  }
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeleteButton);
