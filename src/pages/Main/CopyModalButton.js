import React from "react";
import { connect } from "react-redux";
import Button from "../../components/Button";
import { copyItem } from "../../actions/grid";
import { closeModal } from "../../actions/modal";

const CopyModalButton = ({ isDisabled, copyItem, closeModal }) => {
  return (
    <Button
      className={`CopyModalButton${isDisabled ? " disabled" : ""}`}
      onClick={() => {
        copyItem();
        closeModal();
      }}
    >
      Kopiraj
    </Button>
  );
};

const mapStateToProps = state => ({
  isDisabled: (state.grid.primaryKey[state.grid.activeTable] || []).every(
    key => state.grid.editedItem[key] === state.grid.editedItem[key + "_to"]
  )
});

export default connect(
  mapStateToProps,
  { copyItem, closeModal }
)(CopyModalButton);
