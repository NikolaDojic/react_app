import React from "react";
import { connect } from "react-redux";
import { changeEditedItem } from "../../actions/grid";
import Form from "../../components/Form";
import Predmet from "../Predmet";
import Raspored from "../Raspored";

const GridForm = ({ data, changeEditedItem, columns, activeTable }) => {
  const forms = {
    v_predmet: () => <Predmet />,
    v_raspored: () => <Raspored />
  };
  return (
    <div className="GridForm">
      {forms.hasOwnProperty(activeTable) ? (
        forms[activeTable]()
      ) : (
        <Form
          columns={columns.map(col => ({
            ...col,
            currentValue: data[col.name],
            onChange: value => changeEditedItem({ [col.name]: value })
          }))}
        />
      )}
    </div>
  );
};

const mapStateToProps = (state, ownProps) => ({
  data: ownProps.data || state.grid.editedItem,
  columns:
    ownProps.columns ||
    (state.grid.columns[state.grid.activeTable] || []).filter(col => col.edit),
  activeTable: state.grid.activeTable
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  changeEditedItem:
    ownProps.onChange || (value => dispatch(changeEditedItem(value)))
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GridForm);
