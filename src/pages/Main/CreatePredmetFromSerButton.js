import React from "react";
import { connect } from "react-redux";
import {
  sertToPredmet,
  setCurrentTable,
  fetchColumns
} from "../../actions/grid";
import { predmetVisible } from "../../actions/predmeti";
import { isActionAllowed } from "../../utils";
import Button from "../../components/Button";
import history from "../../history";

const CreatePredmetFromSerButton = ({ onClick, isHidden, isDisabled }) => {
  return (
    <Button
      isHidden={isHidden}
      className={`CreateButton${isDisabled ? " disabled" : ""}`}
      buttonKey="editButton"
      onClick={onClick}
    >
      Kreiraj Predmet
    </Button>
  );
};

const mapStateToProps = state => ({
  isHidden:
    !isActionAllowed("i", "v_predmet", state.formGroups.formGroups) ||
    state.grid.activeTable !== "v_vozilo_sert",
  isDisabled: !state.grid.selectedRow[state.grid.activeTable]
});

const mapDispatchToProps = dispatch => ({
  onClick: () => {
    dispatch(sertToPredmet());
    dispatch(
      fetchColumns("v_predmet", () => {
        dispatch(setCurrentTable("v_predmet"));
        history.push("/Ispitivanja/v_predmet");
        dispatch(predmetVisible(true));
      })
    );
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreatePredmetFromSerButton);
