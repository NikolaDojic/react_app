import React, { useRef, useEffect } from "react";
import { connect } from "react-redux";
import { findForm, isActionAllowed } from "../../utils";
import { openModal } from "../../actions/modal";
import { predmetVisible, setSertVisible } from "../../actions/predmeti";
import { selectedToEdited, setEditedItem, saveItem } from "../../actions/grid";
import Button from "../../components/Button";
import editIcon from "../../assets/icons/edit.svg";
import Form from "./Form";

const EditButton = ({
  onClick,
  currentForm,
  tableName,
  isHidden,
  isDisabled
}) => {
  const editButtonEl = useRef(null);
  useEffect(() => {
    if (editButtonEl && editButtonEl.current) {
      window.openEditModal = () => editButtonEl.current.click();
    }
  }, [(editButtonEl || {}).current]); //eslint-disable-line
  return (
    <Button
      isHidden={isHidden}
      buttonRef={editButtonEl}
      id="editButton"
      className={`EditButton${isDisabled ? " disabled" : ""}`}
      buttonKey="editButton"
      onClick={onClick(currentForm, tableName)}
    >
      Izmeni <img src={editIcon} alt="" />
    </Button>
  );
};
const mapStateToProps = state => ({
  currentForm: state.grid.currentTable,
  tableName: (
    findForm(state.grid.currentTable, state.formGroups.formGroups) || {
      title: ""
    }
  ).title,
  isHidden: !isActionAllowed(
    "u",
    state.grid.activeTable,
    state.formGroups.formGroups
  ),
  isDisabled: !state.grid.selectedRow[state.grid.activeTable]
});

const mapDispatchToProps = dispatch => ({
  onClick: (currentForm, tableName) => () => {
    dispatch(selectedToEdited());
    if (window.tableName === "v_predmet") {
      dispatch(predmetVisible(true));
      setTimeout(() => {
        // var a = document.querySelector(".kl_naziv input");
      }, 0);
    } else if (window.activeTable === "v_vozilo_sert") {
      dispatch(setSertVisible(true));
      setTimeout(() => {
        // var a = document.querySelector(".kl_naziv input");
        // console.log(a);
        // a && a.focus();
      }, 0);
    } else {
      const modalData = {
        title: tableName,
        className: currentForm,
        template: <Form />,
        buttons: [
          { text: "Nazad", action: () => dispatch(setEditedItem({})) },
          { text: "Sačuvaj", action: () => dispatch(saveItem()) }
        ]
      };
      dispatch(openModal(modalData));
    }
  }
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditButton);
