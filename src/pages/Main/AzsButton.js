import React from "react";
import { connect } from "react-redux";
import { azsDefs, azsTables } from "../../utils/reports";
import { generateAzs } from "../../actions/reports";
import Button from "../../components/Button";
import { openModal } from "../../actions/modal";
import AzsModal from "./AzsModal";

const AzsButton = ({ isHidden, isDisabled, onClick, currentTable }) => {
  return (
    <Button
      className={`ReportButton${isDisabled ? " disabled" : ""}`}
      onClick={onClick(currentTable)}
    >
      AZS
    </Button>
  );
};

const mapStateToProps = state => {
  const currentTable = state.grid.currentTable;
  const selectedItem = state.grid.selectedRow[currentTable] || {};
  const aDef = azsDefs[currentTable];
  const hasAzsValue = Boolean(
    aDef && selectedItem[azsDefs[currentTable].stateKey]
  );
  return {
    isHidden: !azsTables.includes(currentTable),
    currentTable,
    isDisabled: !hasAzsValue
  };
};

const mapDispatchToProps = dispatch => ({
  onClick: currentTable => () => {
    const aDef = azsDefs[currentTable];
    if (aDef)
      if (aDef.types.length === 1) {
        dispatch(generateAzs(currentTable));
      } else {
        dispatch(
          openModal({
            title: "Izveštaji",
            template: <AzsModal azsTypes={aDef.types} />,
            buttons: [
              { type: "cancel", text: "Nazad" },
              {
                type: "ok",
                text: "Kopiraj tekst",
                action: () => dispatch(generateAzs())
              }
            ]
          })
        );
      }
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AzsButton);
