import React from "react";
import { connect } from "react-redux";
import Button from "../../components/Button";
import { openModal } from "../../actions/modal";
import { findForm, isActionAllowed } from "../../utils";
import { setEditedItem } from "../../actions/grid";
import { getCopyColumns } from "../../utils/grid";
import Form from "./Form";
import CopyModalButton from "./CopyModalButton.js";
const DeleteButton = ({
  columns,
  currentForm,
  getData,
  isDisabled,
  isHidden,
  onClick,
  tableName
}) => {
  return (
    <Button
      className={`CopyButton`}
      isHidden={isHidden}
      buttonKey="deleteButton"
      onClick={onClick(currentForm, tableName, columns, getData(), isDisabled)}
    >
      Kopiraj
    </Button>
  );
};
const mapStateToProps = state => {
  const activeTable = state.grid.activeTable;
  const primaryKeys = state.grid.primaryKey[activeTable] || [];
  const isCopyAllowed = isActionAllowed(
    "c",
    activeTable,
    state.formGroups.formGroups
  );

  return {
    currentForm: state.grid.currentTable,
    tableName: (
      findForm(activeTable, state.formGroups.formGroups) || {
        title: ""
      }
    ).title,
    isHidden: !isCopyAllowed,
    columns: isCopyAllowed
      ? getCopyColumns(
          [...(state.grid.columns[activeTable] || [])],
          [...(primaryKeys || [])],
          state.grid.activeTable,
          state.grid.selectedRow
        )
      : [...(state.grid.columns[activeTable] || [])],
    getData: () => {
      // let activeTable = state.grid.activeTable;
      // let primaryKeys = state.grid.primaryKey[activeTable] || [];
      let data =
        { ...(state.grid.selectedRow[state.grid.activeTable] || {}) } || {};
      // [...(primaryKeys || [])].forEach(key => (data[key + "_to"] = data[key]));
      return data;
    }
  };
};
const mapDispatchToProps = dispatch => ({
  onClick: (currentForm, tableName, columns, data, isDisabled) => () => {
    dispatch(setEditedItem(data));
    dispatch(
      openModal({
        title: tableName,
        className: currentForm,
        template: <Form columns={columns} />,
        buttons: [
          { text: "Nazad", action: () => dispatch(setEditedItem({})) },
          {
            template: <CopyModalButton key={""} />
          }
        ]
      })
    );
  }
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeleteButton);
