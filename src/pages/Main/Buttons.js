import React from "react";
import EditButton from "./EditButton";
import DeleteButton from "./DeleteButton";
import CreateButton from "./CreateButton";
import ClearFiltersButton from "./ClearFiltersButton";
import ActiveTableIndicator from "./ActiveTableIndicator";
import ReportButton from "./ReportButton";
import AzsButton from "./AzsButton";
import CopyButton from "./CopyButton";
import CreatePredmetFromSerButton from "./CreatePredmetFromSerButton";

const Buttons = () => {
  return (
    <div className="Buttons">
      <div>
        <DeleteButton />
        <ClearFiltersButton />
        <ReportButton />
        <AzsButton />
        <CopyButton />
      </div>
      <ActiveTableIndicator />
      {window.activeTable === "v_vozilo" ? (
        <div />
      ) : (
        <div>
          <CreatePredmetFromSerButton />
          <EditButton />
          <CreateButton />
        </div>
      )}
    </div>
  );
};

export default Buttons;
