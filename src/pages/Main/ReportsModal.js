import React from "react";
import { connect } from "react-redux";
import { setSelectedReport } from "../../actions/reports";
import RadioButtons from "../../components/RadioButtons";

const ReportsModal = ({ reportTypes, setSelectedReport, selectedReport }) => {
  return (
    <div className="ReportsModal">
      <RadioButtons
        buttons={reportTypes}
        currentValue={selectedReport}
        onClick={setSelectedReport}
        name="currentReport"
      />
    </div>
  );
};

const mapStateToProps = state => ({
  selectedReport: state.reports.selectedReport
});

export default connect(
  mapStateToProps,
  { setSelectedReport }
)(ReportsModal);
