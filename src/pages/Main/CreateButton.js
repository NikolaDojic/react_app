import React from "react";
import Button from "../../components/Button";
import { connect } from "react-redux";
import { setEditedItem, saveItem } from "../../actions/grid";
import { predmetVisible, setSertVisible } from "../../actions/predmeti";
import { findForm, isActionAllowed } from "../../utils";
import { openModal } from "../../actions/modal";
import Form from "./Form";

const EditButton = ({
  onClick,
  currentForm,
  tableName,
  isHidden,
  isDisabled
}) => {
  return (
    <Button
      isHidden={isHidden}
      className={`CreateButton${isDisabled ? " disabled" : ""}`}
      buttonKey="editButton"
      onClick={onClick(currentForm, tableName)}
    >
      Kreiraj novi
    </Button>
  );
};
const mapStateToProps = state => ({
  currentForm: state.grid.activeTable,
  tableName: (
    findForm(state.grid.activeTable, state.formGroups.formGroups) || {
      title: ""
    }
  ).title,
  isHidden: !isActionAllowed(
    "i",
    state.grid.activeTable,
    state.formGroups.formGroups
  ),
  isDisabled:
    !state.grid.columns[state.grid.activeTable] ||
    !state.grid.columns[state.grid.activeTable].length
});

const mapDispatchToProps = dispatch => ({
  onClick: (activeTable, tableName) => item => {
    dispatch(setEditedItem({}, activeTable));
    if (window.activeTable === "v_predmet") {
      dispatch(predmetVisible(true));
      setTimeout(() => {
        // var a = document.querySelector(".kl_naziv input");
        // console.log(a);
        // a && a.focus();
      }, 0);
    } else if (window.activeTable === "v_vozilo_sert") {
      dispatch(setSertVisible(true));
      setTimeout(() => {
        // var a = document.querySelector(".kl_naziv input");
        // console.log(a);
        // a && a.focus();
      }, 0);
    } else {
      const modalData = {
        title: tableName,
        className: activeTable,
        template: <Form />,
        buttons: [
          {
            text: "Nazad",
            action: () => dispatch(setEditedItem({}, activeTable))
          },
          { text: "Sačuvaj", action: () => dispatch(saveItem()) }
        ]
      };
      dispatch(openModal(modalData));
    }
  }
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditButton);
