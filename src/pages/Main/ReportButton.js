import React from "react";
import { connect } from "react-redux";
import { reportTables } from "../../utils/reports";
import { generateReport, setSelectedReport } from "../../actions/reports";
import Button from "../../components/Button";
import { openModal } from "../../actions/modal";
import ReportsModal from "./ReportsModal";

const ReportButton = ({
  isHidden,
  isDisabled,
  onClick,
  currentTable,
  reports
}) => {
  return (
    <Button
      className={`ReportButton${isDisabled ? " disabled" : ""}`}
      onClick={onClick(currentTable, reports)}
    >
      Izveštaj
    </Button>
  );
};

const mapStateToProps = state => {
  const currentTable = state.grid.currentTable;
  const selectedItem = state.grid.selectedRow[currentTable];
  const repDef = state.reports.reports[currentTable];
  const isDisabled = !repDef || !selectedItem;
  return {
    reports: repDef,
    isHidden: !reportTables.includes(currentTable),
    currentTable,
    isDisabled: isDisabled
  };
};

const mapDispatchToProps = dispatch => ({
  onClick: (currentTable, repDef) => () => {
    dispatch(setSelectedReport(repDef[0].value));
    if (repDef.length === 1) {
      dispatch(generateReport(currentTable));
    } else {
      dispatch(
        openModal({
          title: "Izveštaji",
          template: <ReportsModal reportTypes={repDef} />,
          buttons: [
            { type: "cancel", text: "Nazad" },
            {
              type: "ok",
              text: "Štampaj",
              action: () => dispatch(generateReport(currentTable))
            }
          ]
        })
      );
    }
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReportButton);
