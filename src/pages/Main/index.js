import React from "react";
import { connect } from "react-redux";
import Grid from "../../App/Grid";
import Buttons from "./Buttons";
import Overlay from "./Overlay";

import "./Main.css";

const Locations = ({ currentTable, details, selectedRow }) => {
  return (
    <div className="Main">
      <Grid tableName={currentTable} isDetail={false} />
      {details.map(detail => (
        <Grid key={detail.table} isDetail={true} tableName={detail.table} />
      ))}
      <Buttons />
      <Overlay />
    </div>
  );
};

const mapStateToProps = state => ({
  currentTable: state.grid.currentTable,
  details: state.grid.details,
  selectedRow: state.grid.selectedRow
});

export default connect(mapStateToProps)(Locations);
