import React from "react";
import { connect } from "react-redux";
import { setSelectedAzs } from "../../actions/reports";
import RadioButtons from "../../components/RadioButtons";

const AzssModal = ({ azsTypes, setSelectedAzs, selectedAzs }) => {
  return (
    <div className="AzsModal ReportsModal">
      <RadioButtons
        buttons={azsTypes}
        currentValue={selectedAzs}
        onClick={setSelectedAzs}
        name="currentAzs"
      />
    </div>
  );
};

const mapStateToProps = state => ({
  selectedAzs: state.reports.selectedAzs
});

export default connect(
  mapStateToProps,
  { setSelectedAzs }
)(AzssModal);
