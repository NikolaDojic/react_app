import React from "react";
import { connect } from "react-redux";
import Button from "../../components/Button";
import { clearFilters } from "../../actions/grid";

const ClearFiltersButton = ({ clearFilters }) => {
  return (
    <Button
      className="DeleteButton"
      onClick={() => {
        clearFilters();
        Object.keys(window.gridApi).forEach(key => {
          window.gridApi[key].setFilterModel({});
          window.gridApi[key].onFilterChanged({});
        });
      }}
    >
      Poništi filtere
    </Button>
  );
};

export default connect(
  null,
  { clearFilters }
)(ClearFiltersButton);
