import React from "react";
import { connect } from "react-redux";
import TableRow from "./TableRow";

const ROWS = [
  {
    coc: "0.1",
    vDozvola: "(D.1)",
    tkv: "Marka:",
    val: predmet => predmet.mr_naziv
  },
  {
    coc: "0.2",
    vDozvola: "(D.2)",
    tkv: "Tip/Varijanta/Verzija:",
    val: predmet =>
      [predmet.mdt_oznaka, predmet.mdvr_oznaka, predmet.mdvz_oznaka].join("")
  },
  {
    coc: "0.2.1",
    vDozvola: "(D.3)",
    tkv: "Komercijalna oznaka:",
    val: predmet => predmet.md_naziv_k
  },
  {
    coc: "-",
    vDozvola: "(B1)",
    tkv: "Procenjena godina proizvodnje:",
    val: predmet => predmet.md_naziv_k
  },
  {
    coc: "16.1",
    vDozvola: "(F.1)",
    tkv: "Najveća dozvoljena masa vozila (kg):",
    val: predmet => predmet.md_naziv_k
  },
  {
    coc: "13",
    vDozvola: "(G)",
    tkv: "Masa vozila spremnog za vožnju (kg):",
    val: predmet => predmet.md_naziv_k
  },
  {
    coc: "0.4",
    vDozvola: "(L)",
    tkv: "Kategorija vozila:",
    val: predmet => predmet.md_naziv_k
  },
  {
    coc: "38",
    vDozvola: "(J.1)",
    tkv: "Oznaka oblika za karoseriju:",
    val: predmet => predmet.md_naziv_k
  },
  {
    coc: "1",
    vDozvola: "(L)",
    tkv: "Broj osovina i točkova:",
    val: predmet => predmet.md_naziv_k
  },
  {
    coc: "5",
    vDozvola: "(5)",
    tkv: "Dužina vozila (mm):",
    val: predmet => predmet.md_naziv_k
  },
  {
    coc: "6",
    vDozvola: "(6)",
    tkv: "Širina vozila (mm):",
    val: predmet => predmet.md_naziv_k
  },
  {
    coc: "7",
    vDozvola: "(7)",
    tkv: "Visina vozila (mm):",
    val: predmet => predmet.md_naziv_k
  },
  {
    coc: "35",
    vDozvola: "(35)",
    tkv: "Pneumatik/naplatak kombinacija:",
    val: predmet => predmet.md_naziv_k
  },
  {
    coc: "21",
    vDozvola: "(P)",
    tkv: "Oznaka motora:",
    val: predmet => predmet.md_naziv_k
  },
  {
    coc: "25",
    vDozvola: "(P.1)",
    tkv: "Radna zapremina motora(cm3):",
    val: predmet => predmet.md_naziv_k
  },
  {
    coc: "27",
    vDozvola: "(P.2)",
    tkv: "Najveća neto snaga motora (kW):",
    val: predmet => predmet.md_naziv_k
  },
  {
    coc: "26",
    vDozvola: "(P.3)",
    tkv: "Pogonsko gorivo:",
    val: predmet => predmet.md_naziv_k
  },
  {
    coc: "26.1",
    vDozvola: "(Q)",
    tkv: "Najveća neto snaga/masa vozila (samo za motocikle) (kW/kg):",
    val: predmet => predmet.md_naziv_k
  },
  {
    coc: "42",
    vDozvola: "(S.1)",
    tkv: "Broj mesta za sedenje:",
    val: predmet => predmet.md_naziv_k
  },
  {
    coc: "43",
    vDozvola: "(S.2)",
    tkv: "Broj mesta za stajanje (za vozila vrste M2 i M3):",
    val: predmet => predmet.md_naziv_k
  },
  {
    coc: "43.1",
    vDozvola: "(43.1)",
    tkv: "Uređaj za spajanje vučnog i priključnog vozila:",
    val: predmet => predmet.md_naziv_k
  },
  {
    coc: "44",
    vDozvola: "(T)",
    tkv: "Najveća brzina (za vozila vrste L) (km/h):",
    val: predmet => predmet.md_naziv_k
  },
  {
    coc: "49",
    vDozvola: "(V.7)",
    tkv: "Nivo izduvne emisije (g/km):",
    val: predmet => predmet.md_naziv_k
  },
  {
    coc: "16.2",
    vDozvola: "(N)",
    tkv: "Najveće dozvoljeno osovinsko opterećenje (kg):",
    val: predmet => predmet.md_naziv_k
  }
];

const PotvrdaPreview = ({ predmet }) => {
  return (
    <div className="PotvrdaPreview">
      <div className="potvrdaTable">
        <div className="row header">
          <div className="coc">COC</div>
          <div className="vDozvola">Vozačka dozvola</div>
          <div className="tkv">Tehničke karakteristike vozila</div>
          <div className="tblVals" />
        </div>
        {ROWS.map(row => (
          <TableRow {...row} val={row.val(predmet)} />
        ))}
      </div>
    </div>
  );
};

const mapStateToProps = state => ({});

export default connect(mapStateToProps)(PotvrdaPreview);
