import React from "react";

const TableRow = ({ coc, vDozvola, tkv, val }) => {
  return (
    <div className="TableRow row">
      <div className="coc">{coc}</div>
      <div className="vDozvola">{vDozvola}</div>
      <div className="tkv">{tkv}</div>
      <div className="tblVals"> {val} </div>
    </div>
  );
};

export default TableRow;
