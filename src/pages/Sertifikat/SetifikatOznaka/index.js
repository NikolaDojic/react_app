import React from "react";
import { connect } from "react-redux";
import { changeEditedItem } from "../../../actions/grid";
import PredmetiInput from "../../../App/PredmetiInput";
import { getLabel, filterObjectByKeys } from "../../../utils";

const COLUMNS = ["vzs_oznaka"];
const SertifikatOznaka = ({ data, columns }) => {
  return (
    <div className="SertifikatOznaka">
      <PredmetiInput
        className={`predmetInput`}
        label={getLabel("vzs_oznaka", columns)}
        onChange={value => changeEditedItem({ vzs_oznaka: value })}
        initialValue={data.vzs_oznaka || ""}
      />
    </div>
  );
};

const mapStateToProps = state => ({
  columns: (state.grid.columns[state.grid.currentTable] || []).filter(
    col => col.edit && COLUMNS.includes(col.name)
  ),
  data: filterObjectByKeys(COLUMNS, { ...state.grid.editedItem })
});

export default connect(mapStateToProps)(SertifikatOznaka);
