import { connect } from "react-redux";
import Button from "../../components/Button";
import { saveItem } from "../../actions/predmeti";

const mapStateToProps = state => ({
  children: "Snimi"
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  onClick:
    ownProps.onClick ||
    (() => dispatch(saveItem(ownProps.columns, ownProps.tableName)))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Button);
