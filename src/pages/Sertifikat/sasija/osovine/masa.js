import React from "react";
import { connect } from "react-redux";
import { changeEditedItem } from "../../../../actions/grid";
import { fetchOptions } from "../../../../actions/predmeti";
import { getLabel } from "../../../../utils";
import PredmetiInput from "../../../../App/PredmetiInput";
import { filterObjectByKeys } from "../../../../utils";

const COLUMNS = ["vzs_masa_max", "vzs_nosivost", "vzs_masa"];
const RELEVANT_DATA = [...COLUMNS, "vzk_oznaka", "vzpv_oznaka"];
const teretneOznake = ["N1", "N2", "N3", "O1", "O2", "O3", "O4"];
const izuzeciVrste = ["BC", "BX", "S*"];
const Klijent = ({
  changeEditedItem,
  columns,
  data,
  fetchOptions,
  options
}) => {
  const calculateNosivost = (colName, value) => {
    let nosivost = data["vzs_nosivost"];
    const isTeretno = teretneOznake.includes(data["vzpv_oznaka"]);
    const isIzuzetak =
      izuzeciVrste.includes(data["vzk_oznaka"]) ||
      (data["vzk_oznaka"] || "").startsWith("S");
    if (isTeretno && !isIzuzetak) {
      if (
        !data["vzs_nosivost"] ||
        Number(!data["vzs_nosivost"]) ||
        data["vzs_nosivost"] == data["vzs_masa_max"] - data["vzs_masa"] //eslint-disable-line
      ) {
        const otherCol = colName === "vzs_masa" ? "vzs_masa_max" : "vzs_masa";
        nosivost = value - data[otherCol];
        nosivost = colName === "vzs_masa" ? nosivost * -1 : nosivost;
      }
    }
    return nosivost ? String(nosivost) : nosivost;
  };

  return (
    <div className="Klijent SasijaDimenzije">
      <PredmetiInput
        key={"vzs_masa_max"}
        label={getLabel("vzs_masa_max", columns)}
        onChange={(value, type) => {
          let nosivost = calculateNosivost("vzs_masa_max", value);
          changeEditedItem({
            vzs_masa_max: value,
            vzs_nosivost: nosivost
          });
        }}
        initialValue={data["vzs_masa_max"]}
        inputAttrs={{
          type: "number"
        }}
      />
      <PredmetiInput
        key={"vzs_masa"}
        label={getLabel("vzs_masa", columns)}
        onChange={(value, type) => {
          const nosivost = calculateNosivost("vzs_masa", value);
          changeEditedItem({ vzs_masa: value, vzs_nosivost: nosivost });
        }}
        initialValue={data["vzs_masa"]}
        inputAttrs={{
          type: "number"
        }}
      />
      <PredmetiInput
        key={"vzs_nosivost"}
        label={getLabel("vzs_nosivost", columns)}
        onChange={(value, type) => {
          changeEditedItem({ vzs_nosivost: value });
        }}
        initialValue={data["vzs_nosivost"]}
        implicitChange={true}
        inputAttrs={{
          type: "number"
        }}
      />
    </div>
  );
};

const mapStateToProps = state => ({
  columns: (state.grid.columns[state.grid.currentTable] || []).filter(
    col => col.edit && COLUMNS.includes(col.name)
  ),
  options: { ...state.predmeti.selectOptions },
  data: filterObjectByKeys(RELEVANT_DATA, { ...state.grid.editedItem })
});

export default connect(
  mapStateToProps,
  {
    changeEditedItem,
    fetchOptions
  }
)(Klijent);
