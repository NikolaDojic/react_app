import React, { useEffect } from "react";
import { connect } from "react-redux";
import { changeEditedItem } from "../../../../actions/grid";
import { getLabel } from "../../../../utils";
import PredmetiInput from "../../../../App/PredmetiInput";
const COLUMNS = [
  "vzs_os_broj",
  "vzs_os_tockova",
  "vzs_os_pneumatici",
  "vzs_os_nosivost"
];

const Osovina = ({ index, data, columns, changeEditedItem, brojOs }) => {
  useEffect(() => {
    if (!data || !data[0]) {
      changeEditedItem({ vzs_osovine: [{}] });
    }
  }, []); //eslint-disable-line
  if (!data) {
    data = [{}];
  }
  return (
    <div key={index} className="osovina">
      <PredmetiInput
        key={"vzs_os_nosivost" + index}
        label={getLabel("vzs_os_nosivost", columns) + " " + (index + 1)}
        onChange={(value, type) => {
          let newData = [...data];
          newData[index] = newData[index] || {};
          newData[index]["vzos_nosivost"] = Number(value);
          changeEditedItem({
            vzs_osovine: newData
          });
        }}
        initialValue={Number((data[index] || {})["vzos_nosivost"]) || ""}
        inputAttrs={{
          type: "number"
        }}
      />
      <PredmetiInput
        key={"vzs_os_tockova"}
        label={getLabel("vzs_os_tockova", columns) + " " + (index + 1)}
        implicitChange={true}
        onChange={(value, type) => {
          let newData = [...data];
          if (newData.length < brojOs) {
            const missingOs = new Array(brojOs - newData.length).fill({});
            newData = [...newData, ...missingOs];
          }
          newData[index] = newData[index] || {};

          if (index === 0) {
            const tcValue = newData[index].vzos_tockova;
            newData = newData.map(data => {
              if ((data && data.vzos_tockova === tcValue) || !data.vzos_tockova)
                data.vzos_tockova = value;
              return data;
            });
            changeEditedItem({
              vzs_osovine: newData
            });
          } else {
            newData[index]["vzos_tockova"] = Number(value);
            changeEditedItem({
              vzs_osovine: newData
            });
          }
        }}
        initialValue={(data[index] || {})["vzos_tockova"] || ""}
        inputAttrs={{
          type: "number"
        }}
      />
      <PredmetiInput
        key={"vzs_os_pneumatici"}
        label={getLabel("vzs_os_pneumatici", columns) + " " + (index + 1)}
        implicitChange={true}
        onChange={(value, type) => {
          let newData = [...data];
          if (newData.length < brojOs) {
            const missingOs = new Array(brojOs - newData.length).fill({});
            newData = [...newData, ...missingOs];
          }
          newData[index] = newData[index] || {};
          if (index === 0) {
            const pnValue = newData[index].vzos_pneumatik;
            newData = newData.map(data => {
              if (
                (data && data.vzos_pneumatik === pnValue) ||
                !data.vzos_pneumatik
              )
                data.vzos_pneumatik = value;
              return data;
            });
            changeEditedItem({
              vzs_osovine: newData
            });
          } else {
            newData[index]["vzos_pneumatik"] = value;
            changeEditedItem({
              vzs_osovine: newData
            });
          }
        }}
        initialValue={(data[index] || {})["vzos_pneumatik"] || ""}
      />
    </div>
  );
};

const mapStateToProps = state => ({
  columns: (state.grid.columns[state.grid.currentTable] || []).filter(
    col => col.edit && COLUMNS.includes(col.name)
  ),
  data: state.grid.editedItem.vzs_osovine,
  brojOs: state.grid.editedItem.vzs_os_broj
});

export default connect(
  mapStateToProps,
  {
    changeEditedItem
  }
)(Osovina);
