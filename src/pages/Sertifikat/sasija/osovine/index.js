import React, { useState, useEffect, useRef } from "react";
import { connect } from "react-redux";
import { changeEditedItem } from "../../../../actions/grid";
import { fetchOptions } from "../../../../actions/predmeti";
import { getLabel, filterObjectByKeys } from "../../../../utils";
import PredmetiInput from "../../../../App/PredmetiInput";
import Osovina from "./osovina";
import Masa from "./masa";

const COLUMNS = ["vzs_os_broj"];

const RELEVANT_DATA = ["vzs_os_broj"];

const Klijent = ({ changeEditedItem, columns, data, fetchOptions }) => {
  const [values, setValues] = useState({});
  const brojOsovinaEl = useRef(null);
  useEffect(() => {
    if (data.vzs_os_broj) {
      brojOsovinaEl.current.value = data.vzs_os_broj;
    }
    if (data.vzpv_oznaka) {
      const osovine = data.vzs_os_broj || 2;
      changeEditedItem({ vzs_os_broj: osovine });
      setValues({ ...values, vzs_os_broj: osovine });
    }
  }, [data.vzpv_oznaka, data.vzs_os_broj]); //eslint-disable-line

  return (
    <div className="Klijent">
      <PredmetiInput
        key={"vzs_os_broj"}
        label={getLabel("vzs_os_broj", columns)}
        onChange={(value, type) => {
          changeEditedItem({
            vzs_os_broj: Number(value)
          });
          setValues({
            ...values,
            vzs_os_broj: Number(value)
          });
        }}
        initialValue={data["vzs_os_broj"]}
        inputAttrs={{
          type: "number",
          ref: brojOsovinaEl,
          max: 6
        }}
      />
      {Array(Number(values["vzs_os_broj"]) || Number(data["vzs_os_broj"]) || 1)
        .fill()
        .map((el, index) => {
          return <Osovina key={index} index={index} />;
        })}
      <Masa />
    </div>
  );
};

const mapStateToProps = state => ({
  columns: (state.grid.columns[state.grid.currentTable] || []).filter(
    col => col.edit && COLUMNS.includes(col.name)
  ),
  data: filterObjectByKeys(RELEVANT_DATA, { ...state.grid.editedItem })
});

export default connect(
  mapStateToProps,
  {
    changeEditedItem,
    fetchOptions
  }
)(Klijent);
