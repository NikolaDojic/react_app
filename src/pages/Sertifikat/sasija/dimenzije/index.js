import React from "react";
import { connect } from "react-redux";
import { changeEditedItem } from "../../../../actions/grid";
import { fetchOptions } from "../../../../actions/predmeti";
import { getLabel } from "../../../../utils";
import PredmetiInput from "../../../../App/PredmetiInput";
import { filterObjectByKeys } from "../../../../utils";

const COLUMNS = [
  "vzs_mesta_sedenje",
  "vzs_mesta_stajanje",
  "vzs_duzina",
  "vzs_sirina",
  "vzs_visina",
  "vzs_kuka_sert"
];
const Dimenzije = ({
  changeEditedItem,
  columns,
  data,
  fetchOptions,
  options
}) => {
  return (
    <div className="Klijent SasijaDimenzije">
      <PredmetiInput
        key={"vzs_mesta_sedenje"}
        label={getLabel("vzs_mesta_sedenje", columns)}
        onChange={(value, type) => {
          changeEditedItem({ vzs_mesta_sedenje: value });
        }}
        initialValue={data["vzs_mesta_sedenje"]}
        inputAttrs={{
          type: "number"
        }}
      />
      <PredmetiInput
        key={"vzs_mesta_stajanje"}
        label={getLabel("vzs_mesta_stajanje", columns)}
        onChange={(value, type) => {
          changeEditedItem({ vzs_mesta_stajanje: value });
        }}
        initialValue={data["vzs_mesta_stajanje"]}
        inputAttrs={{
          type: "number"
        }}
      />

      <PredmetiInput
        key={"vzs_duzina"}
        label={getLabel("vzs_duzina", columns)}
        onChange={(value, type) => {
          changeEditedItem({ vzs_duzina: value });
        }}
        initialValue={data["vzs_duzina"]}
        inputAttrs={{
          type: "number"
        }}
      />
      <PredmetiInput
        key={"vzs_sirina"}
        label={getLabel("vzs_sirina", columns)}
        onChange={(value, type) => {
          changeEditedItem({ vzs_sirina: value });
        }}
        initialValue={data["vzs_sirina"]}
        inputAttrs={{
          type: "number"
        }}
      />
      <PredmetiInput
        key={"vzs_visina"}
        label={getLabel("vzs_visina", columns)}
        onChange={(value, type) => {
          changeEditedItem({ vzs_visina: value });
        }}
        initialValue={data["vzs_visina"]}
        inputAttrs={{
          type: "number"
        }}
      />
      <PredmetiInput
        className={`predmetInput`}
        label={getLabel("vzs_kuka_sert", columns)}
        initialValue={data.vzs_kuka_sert}
        onChange={value => changeEditedItem({ vzs_kuka_sert: value })}
      />
    </div>
  );
};

const mapStateToProps = state => ({
  columns: (state.grid.columns[state.grid.currentTable] || []).filter(
    col => col.edit && COLUMNS.includes(col.name)
  ),
  options: { ...state.predmeti.selectOptions },
  data: filterObjectByKeys(COLUMNS, { ...state.grid.editedItem })
});

export default connect(
  mapStateToProps,
  {
    changeEditedItem,
    fetchOptions
  }
)(Dimenzije);
