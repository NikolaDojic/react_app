import { connect } from "react-redux";
import Button from "../../components/Button";
import { setDefaultValues, setIsCreating } from "../../actions/predmeti";

const mapStateToProps = state => ({
  children: "Odbaci"
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  onClick: () => {
    dispatch(setDefaultValues(ownProps.columns));
    dispatch(setIsCreating({ [ownProps.tableName]: false }));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Button);
