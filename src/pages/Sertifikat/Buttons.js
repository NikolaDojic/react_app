import React from "react";
import SaveButton from "./SaveButton";
import DiscardButton from "./DiscardButton";

const Buttons = ({ isHidden, columns, tableName }) => {
  return !isHidden ? (
    <div className="Buttons">
      <SaveButton columns={columns} tableName={tableName} />
      <DiscardButton columns={columns} tableName={tableName} />
    </div>
  ) : null;
};

export default Buttons;
