import { connect } from "react-redux";
import Button from "../../../components/Button";
import { formToFilter } from "../../../actions/grid";
import { setSertVisible } from "../../../actions/predmeti";

const mapStateToProps = state => ({
  children: "Filtriraj"
});

const mapDispatchToProps = dispatch => ({
  onClick: () => {
    dispatch(formToFilter());
    dispatch(setSertVisible(false));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Button);
