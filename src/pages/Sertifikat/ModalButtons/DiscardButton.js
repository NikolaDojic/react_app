import { connect } from "react-redux";
import Button from "../../../components/Button";
import { setEditedItem } from "../../../actions/grid";
import { setSertVisible } from "../../../actions/predmeti";

const mapStateToProps = state => ({
  children: "Odbaci"
});

const mapDispatchToProps = dispatch => ({
  onClick: () => {
    dispatch(setEditedItem({}));
    dispatch(setSertVisible());
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Button);
