import { connect } from "react-redux";
import Button from "../../../components/Button";
import {
  saveItem,
  setEditedItem,
  changeEditedItem
} from "../../../actions/grid";
import { setSertVisible } from "../../../actions/predmeti";

const mapStateToProps = state => ({
  children: "Kreiraj novi"
});

const mapDispatchToProps = dispatch => ({
  onClick: () => {
    dispatch(changeEditedItem({ vzs_id: "", vzs_broj: "" }));
    dispatch(
      saveItem(null, null, () => {
        dispatch(setEditedItem({}));
        dispatch(setSertVisible());
      })
    );
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Button);
