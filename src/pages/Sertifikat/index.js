import React from "react";
import { connect } from "react-redux";
import "./Predmet.css";
import MarkaModel from "./markaModel";
import Motor from "./motor";
import Razno from "./razno";
import SasijaDimenzije from "./sasija/dimenzije";
import SasijaOsovine from "./sasija/osovine";
import TipVarijantaVerzija from "./tipVarijantaVerzija";
import VoziloTip from "./voziloTip";
import ModalButtons from "./ModalButtons";
import ModalDialog from "../../components/ModalDialog";
import SertifikatOznaka from "./SetifikatOznaka";
import Overlay from "../../components/Overlay";

const Predmet = ({ isSertVisible, sertOznaka }) => {
  return isSertVisible ? (
    <div tabIndex="0" className={`modalWrapper predmetFormaWrapper`}>
      <Overlay>
        <ModalDialog
          buttons={[]}
          title={
            <div className="predmetFormHeader">Sertifikat {sertOznaka}</div>
          }
          className="v_vozilo_sert"
          template={
            <div className="Predmet">
              <div>
                <SertifikatOznaka />
                <VoziloTip />
                <MarkaModel />
                <TipVarijantaVerzija />
                <SasijaOsovine />
              </div>
              <div>
                <SasijaDimenzije />
                <Motor />
                <Razno />
              </div>
              <ModalButtons />
            </div>
          }
        />
      </Overlay>
    </div>
  ) : null;
};

const mapStateToProps = state => ({
  isSertVisible:
    state.predmeti.isSertVisible &&
    state.router.location.pathname.endsWith("v_vozilo_sert"),
  sertOznaka: state.grid.editedItem.vzs_oznaka || ""
});

export default connect(mapStateToProps)(Predmet);
