import React from "react";
import { connect } from "react-redux";
import Overlay from "../../components/Overlay";
import Loader from "../../components/Overlay/Loader";

const mapStateToProps = state => ({
  isHidden: !state.login.isFetching,
  children: <Loader />
});

export default connect(mapStateToProps)(Overlay);
