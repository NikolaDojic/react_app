import React, { useEffect, useRef } from "react";
import { connect } from "react-redux";
import { login } from "../../actions/login";
import Overlay from "./Overlay";
import Button from "../../components/Button";
import "./Login.css";

const Login = ({ login }) => {
  const usernameInput = useRef({});
  useEffect(() => {
    if (usernameInput.current) {
      usernameInput.current.focus();
    }
  }, [usernameInput.current]); //eslint-disable-line
  const passwordInput = useRef(null);
  return (
    <div
      className="Login"
      onKeyPress={e => {
        if (e.charCode === 13) {
          login(usernameInput.current.value, passwordInput.current.value);
        }
      }}
    >
      <div className="loginForm">
        <label htmlFor="">
          <span>Korisnicko ime:</span>
          <input
            ref={usernameInput}
            type="text"
            onKeyPress={e => {
              if (e.charCode === 13) {
                login(usernameInput.current.value, passwordInput.current.value);
              }
            }}
          />
        </label>
        <label htmlFor="">
          <span>Lozinka:</span>
          <input
            type="password"
            ref={passwordInput}
            onKeyPress={e => {
              if (e.charCode === 13) {
                login(usernameInput.current.value, passwordInput.current.value);
              }
            }}
          />
        </label>
        <div className="buttonWrapper">
          <Button
            onClick={() =>
              login(usernameInput.current.value, passwordInput.current.value)
            }
          >
            Login
          </Button>
        </div>
      </div>
      <Overlay />
    </div>
  );
};

export default connect(
  null,
  { login }
)(Login);
