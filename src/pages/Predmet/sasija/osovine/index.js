import React, { useState, useEffect, useRef } from "react";
import { connect } from "react-redux";
import { changeEditedItem } from "../../../../actions/grid";
import { fetchOptions } from "../../../../actions/predmeti";
import { getLabel, filterObjectByKeys } from "../../../../utils";
import PredmetiInput from "../../../../App/PredmetiInput";
import Osovina from "./osovina";

const COLUMNS = ["vz_os_broj", "vz_masa", "vz_nosivost", "vz_masa_max"];

const RELEVANT_DATA = [
  "vz_os_broj",
  "vz_masa",
  "vz_nosivost",
  "vz_masa_max",
  "vzk_oznaka",
  "vzpv_oznaka"
];

const teretneOznake = ["N1", "N2", "N3", "O1", "O2", "O3", "O4"];
const izuzeciVrste = ["BC", "BX", "S*"];
const SasijaMasa = ({ changeEditedItem, columns, data, fetchOptions }) => {
  const [values, setValues] = useState({});
  const brojOsovinaEl = useRef(null);
  useEffect(() => {
    if (data.vz_os_broj) {
      brojOsovinaEl.current.value = data.vz_os_broj;
    }
    if (data.vzpv_oznaka) {
      const osovine = data.vz_os_broj || 2;
      changeEditedItem({ vz_os_broj: osovine });
      setValues({ ...values, vz_os_broj: osovine });
    }
  }, [data.vzpv_oznaka, data.vz_os_broj]); //eslint-disable-line

  const calculateNosivost = (colName, value) => {
    let nosivost = data["vz_nosivost"];
    const isTeretno = teretneOznake.includes(data["vzpv_oznaka"]);
    const isIzuzetak =
      izuzeciVrste.includes(data["vzk_oznaka"]) ||
      (data["vzk_oznaka"] || "").startsWith("S");
    if (isTeretno && !isIzuzetak) {
      if (
        !data["vz_nosivost"] ||
        Number(!data["vz_nosivost"]) ||
        data["vz_nosivost"] == data["vz_masa_max"] - data["vz_masa"] //eslint-disable-line
      ) {
        const otherCol = colName === "vz_masa" ? "vz_masa_max" : "vz_masa";
        nosivost = value - data[otherCol];
        nosivost = colName === "vz_masa" ? nosivost * -1 : nosivost;
      }
    }
    return nosivost;
  };

  return (
    <div className="Klijent SasijaOsovine">
      <PredmetiInput
        key={"vz_os_broj"}
        label={getLabel("vz_os_broj", columns)}
        onChange={(value, type) => {
          changeEditedItem({
            vz_os_broj: Number(value)
          });
          setValues({
            ...values,
            vz_os_broj: Number(value)
          });
        }}
        initialValue={data["vz_os_broj"]}
        inputAttrs={{
          type: "number",
          ref: brojOsovinaEl,
          max: 6
        }}
      />
      {Array(Number(values["vz_os_broj"]) || Number(data["vz_os_broj"]) || 1)
        .fill()
        .map((el, index) => {
          return <Osovina key={index} index={index} />;
        })}
      <PredmetiInput
        key={"vz_masa_max"}
        label={getLabel("vz_masa_max", columns)}
        onChange={(value, type) => {
          const nosivost = calculateNosivost("vz_masa_max", Number(value));
          changeEditedItem({
            vz_masa_max: Number(value),
            vz_nosivost: nosivost
          });
        }}
        initialValue={data["vz_masa_max"]}
        inputAttrs={{
          type: "number"
        }}
      />
      <PredmetiInput
        key={"vz_masa"}
        label={getLabel("vz_masa", columns)}
        onChange={(value, type) => {
          const nosivost = calculateNosivost("vz_masa_max", Number(value));
          changeEditedItem({
            vz_masa: Number(value),
            vz_nosivost: nosivost
          });
        }}
        initialValue={data["vz_masa"]}
        inputAttrs={{
          type: "number"
        }}
      />
      <PredmetiInput
        key={"vz_nosivost"}
        implicitChange={true}
        label={getLabel("vz_nosivost", columns)}
        onChange={(value, type) => {
          changeEditedItem({
            vz_nosivost: Number(value)
          });
        }}
        initialValue={data["vz_nosivost"]}
        inputAttrs={{
          type: "number"
        }}
      />
    </div>
  );
};

const mapStateToProps = state => ({
  columns: (state.grid.columns[state.grid.currentTable] || []).filter(
    col => col.edit && COLUMNS.includes(col.name)
  ),
  data: filterObjectByKeys(RELEVANT_DATA, { ...state.grid.editedItem })
});

export default connect(
  mapStateToProps,
  {
    changeEditedItem,
    fetchOptions
  }
)(SasijaMasa);
