import React, { useEffect } from "react";
import { connect } from "react-redux";
import { changeEditedItem } from "../../../../actions/grid";
import { getLabel } from "../../../../utils";
import PredmetiInput from "../../../../App/PredmetiInput";
const COLUMNS = [
  "vz_os_broj",
  "vz_os_tockova",
  "vz_os_pneumatici",
  "vz_os_nosivost"
];

const Osovina = ({ index, data, columns, changeEditedItem, brojOs }) => {
  useEffect(() => {
    if (!data || !data[0]) {
      changeEditedItem({ vz_osovine: [{}] });
    }
  }, []); //eslint-disable-line
  if (!data) {
    data = [{}];
  }
  return (
    <div key={index} className="osovina">
      <PredmetiInput
        key={"vz_os_nosivost" + index}
        label={getLabel("vz_os_nosivost", columns) + " " + (index + 1)}
        implicitChange={true}
        onChange={(value, type) => {
          let newData = [...data];
          newData[index] = newData[index] || {};
          newData[index]["vzo_nosivost"] = Number(value);
          changeEditedItem({
            vz_osovine: newData
          });
        }}
        initialValue={Number((data[index] || {})["vzo_nosivost"]) || ""}
        inputAttrs={{
          type: "number"
        }}
      />
      <PredmetiInput
        key={"vz_os_tockova"}
        label={getLabel("vz_os_tockova", columns) + " " + (index + 1)}
        implicitChange={true}
        onChange={(value, type) => {
          let newData = [...data];
          if (newData.length < brojOs) {
            const missingOs = new Array(brojOs - newData.length).fill({});
            newData = [...newData, ...missingOs];
          }
          newData[index] = newData[index] || {};

          if (index === 0) {
            const tcValue = newData[index].vzo_tockova;
            newData = newData.map(data => {
              if ((data && data.vzo_tockova === tcValue) || !data.vzo_tockova)
                data.vzo_tockova = value;
              return data;
            });
            changeEditedItem({
              vz_osovine: newData
            });
          } else {
            newData[index]["vzo_tockova"] = Number(value);
            changeEditedItem({
              vz_osovine: newData
            });
          }
        }}
        initialValue={(data[index] || {})["vzo_tockova"] || ""}
        inputAttrs={{
          type: "number"
        }}
      />
      <PredmetiInput
        key={"vz_os_pneumatici"}
        label={getLabel("vz_os_pneumatici", columns) + " " + (index + 1)}
        implicitChange={true}
        onChange={(value, type) => {
          let newData = [...data];
          if (newData.length < brojOs) {
            const missingOs = new Array(brojOs - newData.length).fill({});
            newData = [...newData, ...missingOs];
          }
          newData[index] = newData[index] || {};
          if (index === 0) {
            const pnValue = newData[index].vzo_pneumatik;
            newData = newData.map(data => {
              if (
                (data && data.vzo_pneumatik === pnValue) ||
                !data.vzo_pneumatik
              )
                data.vzo_pneumatik = value;
              return data;
            });
            changeEditedItem({
              vz_osovine: newData
            });
          } else {
            newData[index]["vzo_pneumatik"] = value;
            changeEditedItem({
              vz_osovine: newData
            });
          }
        }}
        initialValue={(data[index] || {})["vzo_pneumatik"] || ""}
      />
    </div>
  );
};

const mapStateToProps = state => ({
  columns: (state.grid.columns[state.grid.currentTable] || []).filter(
    col => col.edit && COLUMNS.includes(col.name)
  ),
  data: state.grid.editedItem.vz_osovine,
  brojOs: state.grid.editedItem.vz_os_broj
});

export default connect(
  mapStateToProps,
  {
    changeEditedItem
  }
)(Osovina);
