import React from "react";
import { connect } from "react-redux";
import { changeEditedItem } from "../../../../actions/grid";
import { fetchOptions } from "../../../../actions/predmeti";
import { getLabel } from "../../../../utils";
import PredmetiInput from "../../../../App/PredmetiInput";

const COLUMNS = [
  "vz_mesta_sedenje",
  "vz_mesta_stajanje",
  "vz_duzina",
  "vz_sirina",
  "vz_visina",
  "vz_kuka",
  "vz_kuka_sert"
];
const Klijent = ({
  changeEditedItem,
  columns,
  data,
  fetchOptions,
  options
}) => {
  return (
    <div className="Klijent">
      {COLUMNS.filter(col => !col.includes("kuka")).map(colName => (
        <PredmetiInput
          key={colName}
          label={getLabel(colName, columns)}
          onChange={(value, type) => {
            changeEditedItem({ [colName]: value });
          }}
          initialValue={data[colName]}
          inputAttrs={{
            type: "number"
          }}
        />
      ))}

      <PredmetiInput
        className={`predmetInput`}
        label={getLabel("vz_kuka", columns)}
        onChange={value => changeEditedItem({ vz_kuka: value })}
        initialValue={data.vz_kuka}
        setFirst={true}
        type={"select"}
        selectAttrs={{
          items: [
            { id: "N", name: "Ne", value: "N" },
            { name: "Da", value: "D", id: "D" }
          ]
        }}
      />
      <PredmetiInput
        className={`predmetInput`}
        label={getLabel("vz_kuka_sert", columns)}
        initialValue={data.vz_kuka_sert}
        onChange={value => changeEditedItem({ vz_kuka_sert: value })}
      />
    </div>
  );
};

const mapStateToProps = state => ({
  columns: (state.grid.columns[state.grid.currentTable] || []).filter(
    col => col.edit && COLUMNS.includes(col.name)
  ),
  options: { ...state.predmeti.selectOptions },
  data: { ...state.grid.editedItem }
});

export default connect(
  mapStateToProps,
  {
    changeEditedItem,
    fetchOptions
  }
)(Klijent);
