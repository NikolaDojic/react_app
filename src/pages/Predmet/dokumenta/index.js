import React from "react";
import { changeEditedItem } from "../../../actions/grid";
import { connect } from "react-redux";
import { getLabel } from "../../../utils";

const COLUMNS = ["pvd_oznaka"];
const Dokumenta = ({ data, changeEditedItem, columns, documents }) => {
  const values = (data || "").split("/");

  return (
    <div className="Dokumenta">
      <div className="checkboxLabel">{getLabel("pvd_oznaka", columns)}:</div>
      {documents.map(doc => (
        <label key={doc.vzd_oznaka} htmlFor={doc.vzd_oznaka}>
          <label key={doc.vzd_oznaka} htmlFor={doc.vzd_oznaka}>
            {doc.vzd_naziv}
          </label>
          <input
            type="checkbox"
            checked={values.includes(doc.vzd_oznaka)}
            id={doc.vzd_oznaka}
            value={doc.vzd_oznaka}
            name={doc.vzd_oznaka}
            readOnly={true}
            onClick={e => {
              const value = e.target.value;
              let newValues;
              if (values.includes(value)) {
                newValues = values.filter(val => val !== value);
              } else {
                newValues = [...values, value];
              }
              newValues = newValues.sort(
                (a, b) =>
                  (documents.find(doc => doc.vzd_oznaka === a) || { vzd_id: 0 })
                    .vzd_id -
                  (documents.find(doc => doc.vzd_oznaka === b) || { vzd_id: 0 })
                    .vzd_id
              );
              changeEditedItem({ pvd_oznaka: newValues.join("/") });
            }}
          />
        </label>
      ))}
    </div>
  );
};

const mapStateToProps = state => ({
  data: state.grid.editedItem.pvd_oznaka,
  documents: [...(state.documents || [])],
  columns: (state.grid.columns[state.grid.currentTable] || []).filter(
    col => col.edit && COLUMNS.includes(col.name)
  )
});

export default connect(
  mapStateToProps,
  { changeEditedItem }
)(Dokumenta);
