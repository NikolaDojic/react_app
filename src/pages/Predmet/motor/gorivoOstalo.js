import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { changeEditedItem, saveItem } from "../../../actions/grid";
import {
  fetchOptions,
  fetchAllOptions,
  addOption,
  clearOptions,
  selectOption,
  setIsCreating
} from "../../../actions/predmeti";
import {
  customSelectStyles,
  getLabel,
  filterObjectByKeys
} from "../../../utils";
import PredmetiInput from "../../../App/PredmetiInput";
import Select from "react-select";

const COLUMNS = [
  "vz_motor",
  "mto_oznaka",
  "mt_cm3",
  "mt_kw",
  "gr_naziv",
  "em_naziv",
  "vz_co2",
  "vz_elektro",
  "vz_max_brzina",
  "vz_kw_kg"
];

const Motor = ({
  addOption,
  changeEditedItem,
  clearOptions,
  columns,
  data,
  fetchOptions,
  options,
  selectOption,
  setIsCreating,
  isCreatingMotor
}) => {
  const [currentValue, setCurrentValue] = useState({});
  useEffect(() => {
    fetchOptions("em_naziv");
    fetchOptions("gr_naziv");
  }, []); //eslint-disable-line
  useEffect(() => {
    let newValue = {};
    Object.keys(data).forEach(key => {
      newValue[key] = { label: data[key], value: data[key] };
    });
    setCurrentValue(newValue);
  }, [data.mto_oznaka]); //eslint-disable-line
  return (
    <div className="KLijent Motor">
      <div className={`comboWrapper  ${"gr_naziv"}`} key={"gr_naziv"}>
        <span className="label comboLabel">
          {getLabel("gr_naziv", columns)} :
        </span>
        <Select
          className="reactSelect"
          key={"gr_naziv"}
          styles={customSelectStyles}
          onChange={option => {
            const item = {
              gr_naziv: option.label,
              gr_id: option.value
            };
            changeEditedItem(item);
            setCurrentValue({ ...currentValue, gr_naziv: option });
          }}
          onInputChange={(value, type) => {
            if (type.action === "input-change") {
              changeEditedItem({
                gr_naziv: value,
                gr_id: ""
              });
            }
          }}
          options={options["gr_naziv"] || []}
          value={currentValue["gr_naziv"]}
        />
      </div>
      <div className={`comboWrapper  ${"gr_naziv"}`} key={"em_naziv"}>
        <span className="label comboLabel">
          {getLabel("em_naziv", columns)} :
        </span>
        <Select
          className="predmetInput reactSelect"
          key={"em_naziv"}
          styles={customSelectStyles}
          label={getLabel("em_naziv", columns)}
          onChange={option => {
            const item = {
              em_naziv: option.label,
              em_id: option.value
            };
            changeEditedItem(item);
            setCurrentValue({ ...currentValue, em_naziv: option });
          }}
          onInputChange={(value, type) => {
            if (type.action === "input-change") {
              changeEditedItem({
                em_naziv: value,
                em_id: ""
              });
            }
          }}
          options={options["em_naziv"] || []}
          value={currentValue["em_naziv"]}
        />
      </div>
      <PredmetiInput
        className={`predmetInput`}
        label={getLabel("vz_elektro", columns)}
        onChange={value => changeEditedItem({ vz_elektro: value })}
        initialValue={data.vz_elektro}
        setFirst={true}
        type={"select"}
        selectAttrs={{
          items: [
            { id: "N", name: "Ne", value: "N" },
            { name: "Da", value: "D", id: "D" }
          ]
        }}
      />
      {["vz_co2", "vz_max_brzina", "vz_kw_kg"].map(colName => (
        <PredmetiInput
          key={colName}
          label={getLabel(colName, columns)}
          onChange={(value, type) => {
            changeEditedItem({ [colName]: value });
          }}
          initialValue={data[colName]}
          inputAttrs={{
            type: "number"
          }}
        />
      ))}
    </div>
  );
};

const mapStateToProps = state => ({
  columns: (state.grid.columns[state.grid.currentTable] || []).filter(
    col => col.edit && COLUMNS.includes(col.name)
  ),
  options: filterObjectByKeys(COLUMNS, { ...state.predmeti.selectOptions }),
  data: filterObjectByKeys(COLUMNS, { ...state.grid.editedItem }),
  isCreatingMotor: state.predmeti.isCreating.motor || false
});

export default connect(
  mapStateToProps,
  {
    addOption,
    changeEditedItem,
    clearOptions,
    fetchOptions,
    fetchAllOptions,
    saveItem,
    selectOption,
    setIsCreating
  }
)(Motor);
