import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { changeEditedItem } from "../../../actions/grid";
import {
  fetchOptions,
  fetchAllOptions,
  addOption,
  selectOption,
  saveItem,
  setIsCreating
} from "../../../actions/predmeti";
import {
  customSelectStyles,
  getLabel,
  filterObjectByKeys
} from "../../../utils";
import PredmetiInput from "../../../App/PredmetiInput";
import CreatableSelect from "react-select/creatable";
import Buttons from "../Buttons";
import fetch from "../../../utils/fetch";
import Config from "../../../config";

const MOTOR_COLUMNS = ["mto_oznaka", "mt_cm3", "mt_kw"];
const COLUMNS = [
  "vz_motor",
  "mto_oznaka",
  "mt_cm3",
  "mt_kw",
  "mto_id",
  "mt_id"
];

const BrojOznaka = ({
  addOption,
  data,
  columns,
  options,
  changeEditedItem,
  saveItem,
  setIsCreating,
  selectOption,
  isCreatingMotor
}) => {
  const [currentValue, setCurrentValue] = useState({});
  const [filteredValues, setFilteredValues] = useState([]);
  const fetchOptions = value =>
    fetch(Config.API.predmeti, {
      method: "POST",
      body: JSON.stringify({
        values: { text: value },
        activeColumn: "mto_oznaka",
        altFnc: true
      })
    })
      .then(res => res.json())
      .then(json => {
        setFilteredValues(json);
      });
  useEffect(() => {
    setCurrentValue({
      ...currentValue,
      mto_oznaka: { value: data.mt_id, label: data.mto_oznaka }
    });
  }, [data.mto_oznaka, data.mt_id]); //eslint-disable-line

  return (
    <div>
      <PredmetiInput
        className={`predmetInput`}
        label={getLabel("vz_motor", columns)}
        onChange={value => changeEditedItem({ vz_motor: value })}
        initialValue={data.vz_motor}
      />
      <div className={`comboWrapper  mto_oznaka`} key={"mto_oznaka"}>
        <span className="label comboLabel">
          {getLabel("mto_oznaka", columns)} :
        </span>
        <CreatableSelect
          className="reactSelect"
          styles={customSelectStyles}
          key={"mto_oznaka"}
          isCreatable={true}
          onChange={option => {
            const colName = "mto_oznaka";
            if (option.isNew) {
              const newItem = {
                label: option.realLabel,
                value: option.realValue
              };
              changeEditedItem({ [colName]: option.realLabel });
              setIsCreating({ v_motor: true });
              addOption(newItem, colName);
              setCurrentValue({ ...currentValue, [colName]: newItem });
              saveItem(MOTOR_COLUMNS, "v_motor");
            } else {
              const motor = filteredValues.find(
                motor => motor.mt_id === option.value
              );
              delete motor.col2display;
              setCurrentValue({ ...currentValue, mto_oznaka: option });

              changeEditedItem(motor);
            }
          }}
          onInputChange={(value, type) => {
            if (type.action === "input-change") {
              fetchOptions(value);
            }
          }}
          options={filteredValues.map(val => ({
            value: val.mt_id,
            label: val.col2display
          }))}
          value={currentValue["mto_oznaka"]}
          getNewOptionData={(inputValue, optionLabel) => ({
            value: inputValue,
            label: "Kreiraj motor " + inputValue,
            isNew: true,
            realValue: "NEW_ITEM",
            realLabel: inputValue
          })}
        />
      </div>
      <PredmetiInput
        className={`predmetInput`}
        label={getLabel("mt_cm3", columns)}
        onChange={value => changeEditedItem({ mt_cm3: Number(value) || 0 })}
        initialValue={data.mt_cm3}
        isActive={isCreatingMotor}
        inputAttrs={{
          type: "number"
        }}
      />
      <PredmetiInput
        className={`predmetInput`}
        label={getLabel("mt_kw", columns)}
        onChange={value => changeEditedItem({ mt_kw: Number(value) || 0 })}
        initialValue={data.mt_kw}
        isActive={isCreatingMotor}
        inputAttrs={{
          type: "number"
        }}
      />
      <Buttons
        isHidden={!isCreatingMotor}
        columns={MOTOR_COLUMNS}
        tableName={"v_motor"}
      />
    </div>
  );
};

const mapStateToProps = state => ({
  columns: (state.grid.columns[state.grid.currentTable] || []).filter(
    col => col.edit && COLUMNS.includes(col.name)
  ),
  data: filterObjectByKeys(COLUMNS, { ...state.grid.editedItem }),
  isCreatingMotor: state.predmeti.isCreating.v_motor || false
});

export default connect(
  mapStateToProps,
  {
    addOption,
    changeEditedItem,
    fetchOptions,
    fetchAllOptions,
    saveItem,
    selectOption,
    setIsCreating
  }
)(BrojOznaka);
