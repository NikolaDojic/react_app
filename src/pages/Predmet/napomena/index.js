import React from "react";
import { connect } from "react-redux";
import { changeEditedItem } from "../../../actions/grid";
import { getLabel, filterObjectByKeys } from "../../../utils";

const COLUMNS = ["pr_napomena", "pr_primedbe", "pr_zakljucak"];
const Napomena = ({ columns, data, changeEditedItem }) => {
  return (
    <div className="Napomena">
      {COLUMNS.map(colName => (
        <div className={"textAreaWrapper"} key={colName}>
          <span className="label">{getLabel(colName, columns)} :</span>
          <textarea
            id=""
            onChange={e => {
              console.log(e.target.value);
              let value = e.target.value;
              changeEditedItem({ [colName]: value });
            }}
            name=""
            cols="80"
            rows="8"
            value={data[colName]}
          />
        </div>
      ))}
    </div>
  );
};

const mapStateToProps = state => ({
  columns: (state.grid.columns[state.grid.currentTable] || []).filter(
    col => col.edit && COLUMNS.includes(col.name)
  ),
  data: filterObjectByKeys(COLUMNS, { ...state.grid.editedItem })
});

export default connect(
  mapStateToProps,
  { changeEditedItem }
)(Napomena);
