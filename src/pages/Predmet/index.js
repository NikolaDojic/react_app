import React from "react";
import { connect } from "react-redux";
import "./Predmet.css";
import Dokumenta from "./dokumenta";
import Klijent from "./klijent";
import MarkaModel from "./markaModel";
import MotorGorivo from "./motor/gorivoOstalo";
import MotorBroj from "./motor/brojOznaka";
import Napomena from "./napomena";
import Razno from "./razno";
import SasijaDimenzije from "./sasija/dimenzije";
import SasijaOsovine from "./sasija/osovine";
import TipVarijantaVerzija from "./tipVarijantaVerzija";
import Usluge from "./usluge";
import VoziloOpste from "./voziloOpste";
import VoziloTip from "./voziloTip";
import ModalButtons from "./ModalButtons";
import ModalDialog from "../../components/ModalDialog";
import Overlay from "../../components/Overlay";
import Status from "./status";
import Sertifikat from "./sertifikat";

const Predmet = ({ isVisible, prNo }) => {
  return isVisible ? (
    <div tabIndex="0" className={`modalWrapper predmetFormaWrapper`}>
      <Overlay>
        <ModalDialog
          buttons={[]}
          title={
            <div className="predmetFormHeader">
              Predmet {prNo} <Status />
            </div>
          }
          className="v_predmet"
          template={
            <div className="Predmet">
              <div>
                <Sertifikat />
                <Dokumenta />
                <Klijent />
                <VoziloOpste />
                <VoziloTip />
                <MarkaModel />
                <TipVarijantaVerzija />
                <MotorBroj />
              </div>
              <div>
                <Usluge />
                <MotorGorivo />
                <SasijaOsovine />
                <SasijaDimenzije />
                <Razno />
              </div>
              <Napomena />
              <ModalButtons />
            </div>
          }
        />
      </Overlay>
    </div>
  ) : null;
};

const mapStateToProps = state => ({
  isVisible:
    state.predmeti.isVisible &&
    state.router.location.pathname.endsWith("v_predmet"),
  prNo: state.grid.editedItem.pr_broj || ""
});

export default connect(mapStateToProps)(Predmet);
