import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { changeEditedItem } from "../../../actions/grid";
import {
  fetchOptions,
  addOption,
  setIsCreating,
  clearOptions,
  selectOption
} from "../../../actions/predmeti";
import { customSelectStyles } from "../../../utils";
import PredmetiInput from "../../../App/PredmetiInput";
import CreatableSelect from "react-select/creatable";
import Buttons from "../Buttons";

const KLIJENT_COLUMNS = ["kl_naziv", "kl_adresa", "kl_telefon", "kl_firma"];
const primaryKey = "kl_id";
const Klijent = ({
  addOption,
  changeEditedItem,
  clearOptions,
  columns,
  data,
  fetchOptions,
  isCreatingClient,
  isFetchingClient = false,
  isPredmet,
  options,
  selectOption,
  setIsCreating
}) => {
  const colName = "kl_naziv";
  const tableName = "klijent";
  const [currentValue, setCurrentValue] = useState("");

  useEffect(() => {
    setCurrentValue({ value: data[primaryKey], label: data[colName] });
  }, []); //eslint-disable-line

  return (
    <div className="Klijent">
      <div className={`comboWrapper ${colName}`} key={colName}>
        <span className="label comboLabel">Klijent :</span>
        <CreatableSelect
          className="reactSelect"
          styles={customSelectStyles}
          key={colName}
          isCreatable={true}
          isDisabled={isFetchingClient}
          isLoading={isFetchingClient}
          onChange={option => {
            if (option.isNew) {
              const newItem = {
                label: option.realLabel,
                value: option.realValue
              };
              setIsCreating({ klijent: true });
              changeEditedItem({ [colName]: option.realLabel });
              KLIJENT_COLUMNS.forEach(col => {
                clearOptions(col);
              });
              addOption(newItem, colName);
              setCurrentValue(newItem);
              changeEditedItem({
                kl_adresa: "",
                kl_telefon: "",
                kl_firma: "D"
              });
            } else {
              const item = {
                [colName]: option.label,
                [primaryKey]: option.value
              };
              changeEditedItem(item);
              KLIJENT_COLUMNS.forEach(col => {
                selectOption(col, option.value);
              });
              setCurrentValue(option);
            }
          }}
          onInputChange={(value, type) => {
            if (type.action === "input-change") {
              changeEditedItem({ [colName]: value });
              if (value.length > 1 && !isCreatingClient) {
                fetchOptions(colName);
              }
            }
          }}
          options={options[colName] || []}
          getNewOptionData={(inputValue, optionLabel) => ({
            value: inputValue,
            label: "Kreiraj klijenta " + inputValue,
            isNew: true,
            realValue: "NEW_ITEM",
            realLabel: inputValue
          })}
          value={currentValue}
        />
      </div>
      <PredmetiInput
        className={`predmetInput adresa`}
        label={"Adresa"}
        onChange={value => changeEditedItem({ kl_adresa: value })}
        initialValue={data.kl_adresa}
        isActive={isCreatingClient}
      />
      <PredmetiInput
        className={`predmetInput telefon`}
        label={"Telefon"}
        onChange={value => changeEditedItem({ kl_telefon: value })}
        initialValue={data.kl_telefon}
        isActive={isCreatingClient}
      />
      <PredmetiInput
        className={`predmetInput firma`}
        isActive={isCreatingClient}
        label={"Firma"}
        type={"select"}
        setFirst={true}
        selectAttrs={{
          items: [
            { name: "Da", value: "D", id: "D" },
            { id: "N", name: "Ne", value: "N" }
          ]
        }}
        initialValue={data.kl_firma}
        onChange={value => changeEditedItem({ kl_firma: value })}
      />
      <Buttons
        isHidden={!isCreatingClient}
        columns={KLIJENT_COLUMNS}
        tableName={tableName}
      />
    </div>
  );
};

const mapStateToProps = state => ({
  columns: (state.grid.columns[state.grid.currentTable] || []).filter(
    col => col.edit && KLIJENT_COLUMNS.includes(col.name)
  ),
  options: { ...state.predmeti.selectOptions },
  data: { ...state.grid.editedItem },
  isCreatingClient: state.predmeti.isCreating.klijent || false
});

export default connect(
  mapStateToProps,
  {
    addOption,
    changeEditedItem,
    clearOptions,
    fetchOptions,
    selectOption,
    setIsCreating
  }
)(Klijent);
