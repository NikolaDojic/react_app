import React from "react";
import { changeEditedItem } from "../../../actions/grid";
import { getLabel } from "../../../utils";
import { connect } from "react-redux";

const COLUMNS = ["pus_oznaka"];
const Usluge = ({ data, changeEditedItem, columns, usluge }) => {
  const values = (data || "").split("/").filter(Boolean);
  return (
    <div className="Dokumenta Usluge">
      <div className="checkboxLabel">{getLabel("pus_oznaka", columns)}:</div>
      {usluge.map(usl => (
        <label key={usl.us_oznaka} htmlFor={usl.us_oznaka}>
          <label key={usl.us_oznaka} htmlFor={usl.us_oznaka}>
            {usl.us_naziv}
          </label>
          <input
            tabIndex="-1"
            type="checkbox"
            checked={values.includes(usl.us_oznaka)}
            id={usl.us_oznaka}
            value={usl.us_oznaka}
            name={usl.us_oznaka}
            readOnly={true}
            onClick={e => {
              const value = e.target.value;
              let newValues;
              if (values.includes(value)) {
                newValues = values.filter(val => val !== value);
              } else {
                newValues = [...values, value];
              }
              newValues = newValues.sort(
                (a, b) =>
                  usluge.find(usl => usl.us_oznaka === a).us_id -
                  usluge.find(usl => usl.us_oznaka === b).us_id
              );
              changeEditedItem({ pus_oznaka: newValues.join("/") });
            }}
          />
        </label>
      ))}
    </div>
  );
};

const mapStateToProps = state => ({
  columns: (state.grid.columns[state.grid.currentTable] || []).filter(
    col => col.edit && COLUMNS.includes(col.name)
  ),
  data: state.grid.editedItem.pus_oznaka,
  usluge: [...(state.usluge || [])]
});

export default connect(
  mapStateToProps,
  { changeEditedItem }
)(Usluge);
