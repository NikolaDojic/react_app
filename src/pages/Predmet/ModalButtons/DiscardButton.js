import { connect } from "react-redux";
import Button from "../../../components/Button";
import { setEditedItem } from "../../../actions/grid";
import { predmetVisible } from "../../../actions/predmeti";

const mapStateToProps = state => ({
  children: "Odbaci"
});

const mapDispatchToProps = dispatch => ({
  onClick: () => {
    dispatch(setEditedItem({}));
    dispatch(predmetVisible());
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Button);
