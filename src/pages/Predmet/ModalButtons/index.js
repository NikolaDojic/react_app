import React from "react";
import { connect } from "react-redux";
// import FilterButton from "./FilterButton";
import SaveButton from "./SaveButton";
import DiscardButton from "./DiscardButton";
import CreateNewButton from "./CreateNewButton";

const Buttons = ({ ...props }) => {
  return (
    <div className="Buttons PredmetModalButtons">
      {/* <FilterButton /> */}
      <div />
      <div>
        <CreateNewButton />
        <SaveButton />
        <DiscardButton />
      </div>
    </div>
  );
};

const mapStateToProps = state => ({});

export default connect(mapStateToProps)(Buttons);
