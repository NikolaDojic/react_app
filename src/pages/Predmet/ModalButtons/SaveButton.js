import { connect } from "react-redux";
import Button from "../../../components/Button";
import { saveItem, setEditedItem } from "../../../actions/grid";
import { predmetVisible } from "../../../actions/predmeti";

const mapStateToProps = state => ({
  children: "Izmeni",
  isHidden: !state.grid.editedItem.pr_id
});

const mapDispatchToProps = dispatch => ({
  onClick: () =>
    dispatch(
      saveItem(null, null, () => {
        dispatch(setEditedItem({}));
        dispatch(predmetVisible(false));
      })
    )
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Button);
