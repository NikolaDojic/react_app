import { connect } from "react-redux";
import Button from "../../../components/Button";
import {
  saveItem,
  setEditedItem,
  changeEditedItem
} from "../../../actions/grid";
import { predmetVisible } from "../../../actions/predmeti";

const mapStateToProps = state => ({
  children: "Kreiraj novi"
});

const mapDispatchToProps = dispatch => ({
  onClick: () => {
    dispatch(changeEditedItem({ pr_id: "", pr_broj: "" }));
    dispatch(
      saveItem(null, null, () => {
        dispatch(setEditedItem({}));
        dispatch(predmetVisible());
      })
    );
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Button);
