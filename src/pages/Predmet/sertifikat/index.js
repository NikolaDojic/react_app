import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import Select from "react-select";
import { customSelectStyles } from "../../../utils";
import { sToP, restToEdit } from "../../../utils/predmet";
import { changeEditedItem, fetchDataPromise } from "../../../actions/grid";
import { selectOption } from "../../../actions/predmeti";

const KLIJENT_COLUMNS = ["vzs_oznaka", "vzs_id"];
const primaryKey = "vzs_id";
const Sertifikat = ({ columns, data, changeEditedItem, selectOption }) => {
  const colName = "vzs_oznaka";
  const tableName = "v_vozilo_sert";
  const [currentValue, setCurrentValue] = useState("");
  const [options, setOptions] = useState([]);
  const [serts, setSerts] = useState([]);
  useEffect(() => {
    setCurrentValue({ value: data[primaryKey], label: data[colName] });
  }, []); //eslint-disable-line
  return (
    <div className="Sertifikat">
      <span className="label comboLabel">Sertifikat :</span>
      <Select
        className="reactSelect"
        styles={customSelectStyles}
        key={colName}
        isCreatable={true}
        onChange={option => {
          const item = {
            [colName]: option.label,
            [primaryKey]: option.value
          };
          let sert = serts.find(sert => sert.vzs_id === option.value) || {};
          sert = sToP(sert);
          sert = restToEdit(sert);

          const predmet = { ...data };
          Object.keys(predmet).forEach(key => {
            if (predmet[key]) {
              delete predmet[key];
            }
          });
          changeEditedItem({ ...sert, ...data, ...item });
          setCurrentValue(option);
        }}
        onInputChange={(value, type) => {
          if (type.action === "input-change") {
            changeEditedItem({ [colName]: value });
            if (value.length > 1) {
              fetchDataPromise(tableName, { [colName]: value }).then(json => {
                setSerts(json);
                setOptions(
                  json.map(sert => ({
                    value: sert.vzs_id,
                    label: sert.vzs_oznaka
                  }))
                );
              });
            }
          }
        }}
        options={options || []}
        value={currentValue}
      />
    </div>
  );
};

const mapStateToProps = state => ({
  columns: (state.grid.columns[state.grid.currentTable] || []).filter(
    col => col.edit && KLIJENT_COLUMNS.includes(col.name)
  ),
  data: { ...state.grid.editedItem }
});

export default connect(
  mapStateToProps,
  {
    changeEditedItem,
    selectOption
  }
)(Sertifikat);
