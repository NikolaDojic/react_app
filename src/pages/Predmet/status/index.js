import { connect } from "react-redux";
import { changeEditedItem } from "../../../actions/grid";
import Select from "../../../components/Select";

const mapStateToProps = state => ({
  items: state.statusi,
  getItemName: item => item.prs_naziv,
  getItemValue: item => item.prs_oznaka,
  getItemId: item => item.prs_id,
  initialValue: state.grid.editedItem.prs_oznaka,
  noLabel: true
});

const mapDispatchToProps = dispatch => ({
  onChange: (itemValue, items) => {
    const item = items.find(item => item.prs_oznaka === itemValue);
    dispatch(changeEditedItem(item));
  }
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  onChange: value => dispatchProps.onChange(value, stateProps.items)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(Select);
