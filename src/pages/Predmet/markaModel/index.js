import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { changeEditedItem } from "../../../actions/grid";
import {
  fetchAllOptions,
  fetchOptions,
  addOption,
  setIsCreating,
  clearOptions,
  selectOption,
  saveItem
} from "../../../actions/predmeti";
import {
  getLabel,
  customSelectStyles,
  filterObjectByKeys
} from "../../../utils";
import CreatableSelect from "react-select/creatable";

const COLUMNS = ["mr_naziv", "md_naziv_k"];

const TABLES = {
  mr: "marka",
  md: "model"
};
const MarkaModel = ({
  columns,
  options,
  data,
  addOption,
  changeEditedItem,
  clearOptions,
  fetchAllOptions,
  fetchOptions,
  saveItem,
  selectOption,
  setIsCreating,
  isFetchingMarka = false,
  isFetchingModel = false
}) => {
  const [currentValue, setCurrentValue] = useState("");
  useEffect(() => {
    let newValue = {};
    Object.keys(data).forEach(key => {
      newValue[key] = { label: data[key], value: data[key] };
    });
    setCurrentValue(newValue);
  }, [data]);

  const [newValues, setNewValues] = useState({});
  return (
    <div className="Klijent MarkaModel">
      <div className={`comboWrapper  mr_naziv`} key={"mr_naziv"}>
        <span className="label comboLabel">
          {getLabel("mr_naziv", columns)} :
        </span>
        <CreatableSelect
          className="reactSelect"
          isClearable
          styles={customSelectStyles}
          key={"mr_naziv"}
          onChange={(option, meta) => {
            if (!option) {
              option = { label: "", value: "" };
            }
            const colName = "mr_naziv";
            const prefix = colName.split("_")[0];
            const primaryKey = prefix + "_id";
            if (option.isNew) {
              const newItem = {
                label: option.realLabel,
                value: option.realValue
              };
              setIsCreating({ marka: true });
              addOption(newItem, "mr_naziv");
              setCurrentValue({ ...currentValue, mr_naziv: newItem });
              saveItem(["mr_naziv"], TABLES[prefix], {
                mr_naziv: newValues.mr_naziv
              });
            } else {
              const item = {
                [colName]: option.label,
                [primaryKey]: option.value,
                md_id: null,
                md_naziv_k: ""
              };
              changeEditedItem(item);
              setCurrentValue({ ...currentValue, mr_naziv: option });
              fetchOptions("md_naziv_k", item);
            }
          }}
          onInputChange={(value, type) => {
            const colName = "mr_naziv";
            if (type.action === "input-change") {
              setNewValues({ ...newValues, mr_naziv: value });
              fetchOptions(colName, newValues);
            }
          }}
          options={options["mr_naziv"]}
          value={currentValue["mr_naziv"]}
          getNewOptionData={(inputValue, optionLabel) => ({
            value: inputValue,
            label: "Kreiraj marku " + inputValue,
            isNew: true,
            realValue: "NEW_ITEM",
            realLabel: newValues.mr_naziv
          })}
        />
      </div>
      <div className={`comboWrapper  md_naziv_k`} key={"md_naziv_k"}>
        <span className="label comboLabel">
          {getLabel("md_naziv_k", columns)} :
        </span>
        <CreatableSelect
          isClearable
          className="reactSelect"
          key={"md_naziv_k"}
          isCreatable={true}
          styles={customSelectStyles}
          onChange={option => {
            if (!option) {
              option = { label: "", value: "" };
            }
            const colName = "md_naziv_k";
            const prefix = colName.split("_")[0];
            const primaryKey = prefix + "_id";
            if (option.isNew) {
              const newItem = {
                label: option.realLabel,
                value: option.realValue
              };
              setIsCreating({ marka: true });
              addOption(newItem, "md_naziv_k");
              setCurrentValue({ ...currentValue, md_naziv_k: newItem });
              saveItem(["md_naziv_k", "mr_id"], TABLES[prefix], {
                md_naziv_k: newValues.md_naziv_k
              });
            } else {
              const item = {
                [colName]: option.label,
                [primaryKey]: option.value
              };
              changeEditedItem(item);
            }
          }}
          onInputChange={(value, type) => {
            const colName = "md_naziv_k";
            if (type.action === "input-change") {
              setNewValues({ ...newValues, [colName]: value });
              fetchOptions(colName, newValues);
            }
          }}
          options={options["md_naziv_k"] || []}
          value={currentValue["md_naziv_k"]}
          getNewOptionData={(inputValue, optionLabel) => ({
            value: inputValue,
            label: "Kreiraj model " + inputValue,
            isNew: true,
            realValue: "NEW_ITEM",
            realLabel: newValues.md_naziv_k
          })}
        />
      </div>
    </div>
  );
};

const mapStateToProps = state => ({
  columns: (state.grid.columns[state.grid.currentTable] || []).filter(
    col => col.edit && COLUMNS.includes(col.name)
  ),
  options: filterObjectByKeys(COLUMNS, { ...state.predmeti.selectOptions }),
  data: filterObjectByKeys(COLUMNS, { ...state.grid.editedItem })
});

export default connect(
  mapStateToProps,
  {
    addOption,
    changeEditedItem,
    clearOptions,
    fetchAllOptions,
    fetchOptions,
    saveItem,
    selectOption,
    setIsCreating
  }
)(MarkaModel);
