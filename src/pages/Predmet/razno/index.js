import React from "react";
import { connect } from "react-redux";
import { changeEditedItem } from "../../../actions/grid";
import { getLabel, filterObjectByKeys } from "../../../utils";
import PredmetiInput from "../../../App/PredmetiInput";

const COLUMNS = ["vz_sert_hmlg_tip", "vz_sert_emisija", "vz_sert_buka"];
const Razno = ({ columns, data, changeEditedItem }) => {
  return (
    <div className="Razno">
      {COLUMNS.map(colName => (
        <PredmetiInput
          key={colName}
          label={getLabel(colName, columns)}
          onChange={(value, type) => {
            changeEditedItem({ [colName]: value });
          }}
          initialValue={data[colName]}
        />
      ))}
    </div>
  );
};

const mapStateToProps = state => ({
  columns: (state.grid.columns[state.grid.currentTable] || []).filter(
    col => col.edit && COLUMNS.includes(col.name)
  ),
  data: filterObjectByKeys(COLUMNS, { ...state.grid.editedItem })
});

export default connect(
  mapStateToProps,
  { changeEditedItem }
)(Razno);
