import React from "react";
import { connect } from "react-redux";
import { changeEditedItem } from "../../../actions/grid";
import { fetchOptions } from "../../../actions/predmeti";
import { getLabel } from "../../../utils";
import PredmetiInput from "../../../App/PredmetiInput";

const COLUMNS = ["vz_sasija", "vz_reg", "vz_godina", "vz_km"];
const Klijent = ({ changeEditedItem, columns, data, fetchOptions }) => {
  const colName = "vz_sasija";

  return (
    <div className="Klijent">
      <PredmetiInput
        key={colName}
        label={getLabel("vz_sasija", columns)}
        onChange={(value, type) => {
          changeEditedItem({ [colName]: value });
        }}
        initialValue={data[colName]}
        warning={sasijaWarning(data[colName])}
      />
      <PredmetiInput
        className={`predmetInput vz_reg`}
        label={getLabel("vz_reg", columns)}
        onChange={value => changeEditedItem({ vz_reg: value })}
        initialValue={data.vz_reg}
      />
      <PredmetiInput
        className={`predmetInput`}
        label={getLabel("vz_godina", columns)}
        onChange={value => changeEditedItem({ vz_godina: value })}
        initialValue={data.vz_godina}
        inputAttrs={{
          type: "number"
        }}
      />
      <PredmetiInput
        className={`predmetInput`}
        label={getLabel("vz_km", columns)}
        inputAttrs={{
          type: "number"
        }}
        initialValue={data.vz_km}
        onChange={value => changeEditedItem({ vz_km: value })}
      />
    </div>
  );
};

const sasijaWarning = _brojSasije => {
  const brojSasije = _brojSasije || "";
  const lengthWarning =
    (brojSasije || "").length && brojSasije.length !== 17 //TODO BUG
      ? `Broj sasije je dugacak ${brojSasije.length} karaktera.\n`
      : "";

  let forbidenLetters = ["I", "O", "Q"].filter(fLetter =>
    brojSasije.toUpperCase().includes(fLetter)
  );
  const letterWarning = forbidenLetters.length
    ? `Broj šasije sadrži zabranjene karaktere ${forbidenLetters.map(
        letter => `"${letter}"`
      )}`
    : "";
  return lengthWarning + letterWarning;
};

const mapStateToProps = state => ({
  columns: (state.grid.columns[state.grid.currentTable] || []).filter(
    col => col.edit && COLUMNS.includes(col.name)
  ),
  data: { ...state.grid.editedItem }
});

export default connect(
  mapStateToProps,
  {
    changeEditedItem,
    fetchOptions
  }
)(Klijent);
