import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { changeEditedItem } from "../../../actions/grid";
import {
  fetchOptions,
  setIsCreating,
  clearOptions
} from "../../../actions/predmeti";
import {
  getLabel,
  customSelectStyles,
  filterObjectByKeys
} from "../../../utils";
import Select from "react-select";

const COLUMNS = [
  "vzpv_oznaka",
  "vzpv_naziv",
  "vzpv_id",
  "vzk_oznaka",
  "vzk_naziv",
  "vzk_id",
  "vzdo_oznaka",
  "vzdo_naziv",
  "vzdo_id",
  "vzkl_oznaka",
  "vzkl_naziv",
  "vzkl_id"
];

const COLUMNS_TO_CLEAR = [
  "vzk_oznaka",
  "vzk_naziv",
  "vzk_id",
  "vzdo_oznaka",
  "vzdo_naziv",
  "vzdo_id",
  "vzkl_oznaka",
  "vzkl_naziv",
  "vzkl_id"
];

const COLS_MULTI = ["vzk_oznaka", "vzdo_oznaka"];

const VoziloTip = ({
  changeEditedItem,
  columns,
  clearOptions,
  data,
  fetchOptions,
  isFetchingClient = false,
  isPredmet,
  options,
  setIsCreating,
  filterCols
}) => {
  const [currentValues, setCurrentValue] = useState("");

  useEffect(() => {
    let newValue = {};
    COLUMNS.filter(col => col.endsWith("oznaka")).forEach(col => {
      const isMulti = COLS_MULTI.includes(col);
      const prefix = col.split("_")[0];
      const primaryKey = prefix + "_id";
      if (isMulti) {
        // const labels = (data[prefix + "_naziv"] || "").split("/");
        const labelsPrefix = (data[col] || "").split("/");
        newValue[col] = labelsPrefix
          .map((labelPrefix, index) => ({
            value: labelPrefix || "",
            label: labelPrefix //+ " " + (labels[index] || "")
          }))
          .filter(option => option.label.trim());
      } else {
        newValue[col] = {
          value: data[primaryKey],
          label: data[col] || "" // + " " + (data[prefix + "_naziv"] || "")
        };
      }
    });
    setCurrentValue(newValue);
    //eslint-disable-next-line
  }, [
    data.vzpv_oznaka,
    data.vzkl_oznaka,
    data.vzk_oznaka,
    data.vzdo_oznaka,
    data.vzk_naziv,
    data.vzdo_naziv
  ]); //eslint-disable-line

  useEffect(() => {
    fetchOptions("vzk_oznaka", { vzk_oznaka: "" });
    fetchOptions("vzdo_oznaka", { vzdo_oznaka: "" });
    fetchOptions("vzkl_oznaka", { vzkl_oznaka: "" });
  }, [data.vzpv_oznaka]); //eslint-disable-line

  useEffect(() => {
    fetchOptions("vzpv_oznaka", { vzpv_oznaka: "" });
    fetchOptions("vzk_oznaka", { vzk_oznaka: "" });
    fetchOptions("vzdo_oznaka");
    fetchOptions("vzkl_oznaka");
  }, []); //eslint-disable-line

  useEffect(() => {
    COLUMNS.filter(colName => colName.endsWith("oznaka")).forEach(col => {
      if (COLS_MULTI.includes(col)) {
        const colNaziv = col.replace("oznaka", "naziv");
        if (currentValues[col] && options[col] && options[colNaziv]) {
          const newValues = currentValues[col]
            .map(valueOption => {
              const stateOption = (options[col] || []).find(
                stateOption =>
                  stateOption.label.trim() === valueOption.label.trim()
              );
              const nazivOption =
                stateOption &&
                (options[colNaziv] || []).find(
                  nameOption => stateOption.value === nameOption.value
                );
              return nazivOption ? nazivOption.label : "";
            })
            .filter(Boolean);
          newValues.length &&
            changeEditedItem({ [colNaziv]: newValues.join(" / ") });
        }
      }
    });
  }, [(options["vzk_oznaka"] || []).length]); //eslint-disable-line

  const multiSelectEl = (colName, prefix, primaryKey) => (
    <Select
      placeholder="Odaberi"
      isMulti={true}
      hideSelectedOptions={true}
      key={colName}
      isClearable={false}
      styles={customSelectStyles}
      className="multiSelect reactSelect"
      onChange={options => {
        let item = {};
        if (!options) {
          item = {
            [colName]: "",
            [prefix + "_naziv"]: ""
          };
        } else {
          const oznake = options.map(opt => opt.label).join("/");
          // const nazivi = options.map(opt => opt.label.split(" ")[1]).join("/");
          item = {
            [colName]: oznake
            // [prefix + "_naziv"]: nazivi
          };
        }
        changeEditedItem(item);
      }}
      options={
        (options[colName] || [])
          .map(option => ({
            ...option,
            label: option.label //+
            // " " +
            // options[prefix + "_naziv"].find(opt => opt.value === option.value)
            //   .label
          }))
          .filter(option => {
            return !(currentValues[colName] || []).some(
              opt => opt.label === option.label
            );
          }) || []
      }
      value={currentValues[colName]}
    />
  );
  return (
    <div className="Klijent VoziloTip">
      {COLUMNS.filter(col => col.endsWith("oznaka")).map(colName => {
        const isMulti = COLS_MULTI.includes(colName);
        const prefix = colName.split("_")[0];
        const primaryKey = prefix + "_id";

        return (
          <div className={`comboWrapper  ${colName}`} key={colName}>
            <span className="label comboLabel">
              {getLabel(colName, columns)} :
            </span>
            {isMulti ? (
              multiSelectEl(colName, prefix, primaryKey)
            ) : (
              <Select
                className="reactSelect"
                placeholder="Odaberi"
                key={colName}
                isCreatable
                isDisabled={isFetchingClient}
                isLoading={isFetchingClient}
                styles={customSelectStyles}
                onChange={option => {
                  const [oznakaValue, nazivValue] = option.label.split(" ");
                  const item = {
                    [colName]: oznakaValue,
                    [primaryKey]: option.value,
                    [prefix + "_naziv"]: nazivValue
                  };
                  changeEditedItem(item);
                  if (colName === "vzpv_oznaka") {
                    COLUMNS_TO_CLEAR.forEach(col => {
                      changeEditedItem({ [col]: "" });
                    });
                  }
                }}
                onInputChange={(value, type) => {
                  if (type.action === "input-change") {
                    // const [oznakaValue, nazivValue] = value.split(" ");
                    // const colNaziv = prefix + "_naziv";
                    // fetchOptions(colName);
                  }
                }}
                options={
                  (options[colName] || [])
                    .map(option => ({
                      ...option,
                      label:
                        option.label +
                        " " +
                        options[prefix + "_naziv"].find(
                          opt => opt.value === option.value
                        ).label
                    }))
                    .filter(option => option.label) || []
                }
                value={currentValues[colName]}
              />
            )}
          </div>
        );
      })}
    </div>
  );
};

const mapStateToProps = state => ({
  columns: (state.grid.columns[state.grid.currentTable] || []).filter(
    col => col.edit && COLUMNS.includes(col.name)
  ),
  options: filterObjectByKeys(COLUMNS, { ...state.predmeti.selectOptions }),
  data: filterObjectByKeys(COLUMNS, { ...state.grid.editedItem }),
  filterCols: Object.keys(state.predmeti.filterDescriptions)
});

export default connect(
  mapStateToProps,
  {
    changeEditedItem,
    fetchOptions,
    setIsCreating,
    clearOptions
  }
)(VoziloTip);
