import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { changeEditedItem } from "../../../actions/grid";
import {
  fetchAllOptions,
  fetchOptions,
  addOption,
  setIsCreating,
  clearOptions,
  selectOption,
  saveItem
} from "../../../actions/predmeti";
import {
  getLabel,
  customSelectStyles,
  filterObjectByKeys
} from "../../../utils";
import CreatableSelect from "react-select/creatable";

const COLUMNS = ["mdt_oznaka", "mdvr_oznaka", "mdvz_oznaka"];

const TABLES = {
  mdvr: "model_varijanta",
  mdt: "model_tip",
  mdvz: "model_verzija"
};

const MarkaModel = ({
  addOption,
  changeEditedItem,
  clearOptions,
  columns,
  data,
  fetchAllOptions,
  fetchOptions,
  isFetchingTip = false,
  isFetchingVarijanta = false,
  isFetchingVerzija = false,
  options,
  saveItem,
  selectOption,
  setIsCreating
}) => {
  const [currentValue, setCurrentValue] = useState(data);
  const [newValues, setNewValues] = useState({});
  useEffect(() => {
    let newValue = {};
    Object.keys(data).forEach(key => {
      newValue[key] = { label: data[key], value: data[key] };
    });
    setCurrentValue(newValue);
  }, [data]);
  return (
    <div className="Klijent MarkaModel">
      <div className={`comboWrapper  mdt_oznaka`} key={"mdt_oznaka"}>
        <span className="label comboLabel">
          {getLabel("mdt_oznaka", columns)} :
        </span>
        <CreatableSelect
          isClearable
          className="reactSelect"
          styles={customSelectStyles}
          key={"mdt_oznaka"}
          isCreatable={true}
          isDisabled={isFetchingTip}
          isLoading={isFetchingTip}
          onChange={option => {
            if (!option) {
              option = { label: "", value: "" };
            }
            const colName = "mdt_oznaka";
            const prefix = colName.split("_")[0];
            const primaryKey = prefix + "_id";
            if (option.isNew) {
              const newItem = {
                label: option.realLabel,
                value: option.realValue
              };
              setIsCreating({ marka: true });
              addOption(newItem, "mdt_oznaka");
              setCurrentValue({ ...currentValue, mdt_oznaka: newItem });
              saveItem([colName], TABLES[prefix], {
                mdt_oznaka: newValues[colName]
              });
            } else {
              const item = {
                [colName]: option.label,
                [primaryKey]: option.value
              };

              changeEditedItem(item);
            }
          }}
          onInputChange={(value, type) => {
            const colName = "mdt_oznaka";
            if (type.action === "input-change") {
              setNewValues({ ...newValues, [colName]: value });
              fetchOptions(colName, { [colName]: value });
            }
          }}
          options={options["mdt_oznaka"] || []}
          value={currentValue["mdt_oznaka"]}
          getNewOptionData={(inputValue, optionLabel) => ({
            value: inputValue,
            label: "Kreiraj tip " + inputValue,
            isNew: true,
            realValue: "NEW_ITEM",
            realLabel: inputValue
          })}
        />
      </div>
      <div className={`comboWrapper  mdvr_oznaka`} key={"mdvr_oznaka"}>
        <span className="label comboLabel">
          {getLabel("mdvr_oznaka", columns)} :
        </span>
        <CreatableSelect
          isClearable
          className="reactSelect"
          styles={customSelectStyles}
          key={"mdvr_oznaka"}
          isCreatable={true}
          isDisabled={isFetchingVarijanta}
          isLoading={isFetchingVarijanta}
          onChange={option => {
            if (!option) {
              option = { label: "", value: "" };
            }
            const colName = "mdvr_oznaka";
            const prefix = colName.split("_")[0];
            const primaryKey = prefix + "_id";
            if (option.isNew) {
              const newItem = {
                label: option.realLabel,
                value: option.realValue
              };
              setIsCreating({ marka: true });
              addOption(newItem, "mdvr_oznaka");
              setCurrentValue({ ...currentValue, mdvr_oznaka: newItem });
              saveItem(["mdvr_oznaka"], TABLES[prefix], {
                [colName]: newValues[colName]
              });
            } else {
              const item = {
                [colName]: option.label,
                [primaryKey]: option.value
              };

              changeEditedItem(item);
            }
          }}
          onInputChange={(value, type) => {
            const colName = "mdvr_oznaka";
            if (type.action === "input-change") {
              setNewValues({ ...newValues, [colName]: value });
              fetchOptions(colName, { [colName]: value });
            }
          }}
          options={options["mdvr_oznaka"] || []}
          value={currentValue["mdvr_oznaka"]}
          getNewOptionData={(inputValue, optionLabel) => ({
            value: inputValue,
            label: "Kreiraj varijantu " + inputValue,
            isNew: true,
            realValue: "NEW_ITEM",
            realLabel: inputValue
          })}
        />
      </div>

      <div className={`comboWrapper  mdvz_oznaka`} key={"mdvz_oznaka"}>
        <span className="label comboLabel">
          {getLabel("mdvz_oznaka", columns)} :
        </span>
        <CreatableSelect
          isClearable
          className="reactSelect"
          styles={customSelectStyles}
          key={"mdvz_oznaka"}
          isCreatable={true}
          isDisabled={isFetchingVerzija}
          isLoading={isFetchingVerzija}
          onChange={option => {
            if (!option) {
              option = { label: "", value: "" };
            }
            const colName = "mdvz_oznaka";
            const prefix = colName.split("_")[0];
            const primaryKey = prefix + "_id";
            if (option.isNew) {
              const newItem = {
                label: option.realLabel,
                value: option.realValue
              };
              setIsCreating({ marka: true });
              addOption(newItem, "mdvz_oznaka");
              setCurrentValue({ ...currentValue, mdvz_oznaka: newItem });
              saveItem(["mdvz_oznaka"], TABLES[prefix], {
                [colName]: newValues[colName]
              });
            } else {
              const item = {
                [colName]: option.label,
                [primaryKey]: option.value
              };

              changeEditedItem(item);
            }
            fetchAllOptions();
          }}
          onInputChange={(value, type) => {
            const colName = "mdvz_oznaka";
            if (type.action === "input-change") {
              setNewValues({
                [colName]: value
              });
              fetchOptions(colName, { [colName]: value });
            }
          }}
          options={options["mdvz_oznaka"] || []}
          value={currentValue["mdvz_oznaka"]}
          getNewOptionData={(inputValue, optionLabel) => ({
            value: inputValue,
            label: "Kreiraj verziju " + inputValue,
            isNew: true,
            realValue: "NEW_ITEM",
            realLabel: inputValue
          })}
        />
      </div>
    </div>
  );
};

const mapStateToProps = state => ({
  columns: (state.grid.columns[state.grid.currentTable] || []).filter(
    col => col.edit && COLUMNS.includes(col.name)
  ),
  options: filterObjectByKeys(COLUMNS, { ...state.predmeti.selectOptions }),
  data: filterObjectByKeys(COLUMNS, { ...state.grid.editedItem })
});

export default connect(
  mapStateToProps,
  {
    addOption,
    changeEditedItem,
    clearOptions,
    fetchAllOptions,
    fetchOptions,
    saveItem,
    selectOption,
    setIsCreating
  }
)(MarkaModel);
