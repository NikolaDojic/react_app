import React from "react";
import { connect } from "react-redux";

const Users = ({ users }) => {
  return (
    <div className="Users">
      {users.map(user => (
        <div>
          <span>{user.ime}</span> <span>{user.prezime}</span>{" "}
        </div>
      ))}
    </div>
  );
};

const mapStateToProps = state => ({
  users: state.users.users
});

export default connect(mapStateToProps)(Users);
