import React, { useEffect } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { generateReport } from "../../actions/reports";
import { setCurrentTable } from "../../actions/grid";
import { isActionAllowed } from "../../utils/index";
import Overlay from "./Overlay";
import "./Home.css";

const Home = ({
  history,
  generateReport,
  setCurrentTable,
  isPredmetAllowed
}) => {
  useEffect(() => setCurrentTable(""), []); //eslint-disable-line
  return (
    <div className="Home">
      <div className="homePageOptions">
        {isPredmetAllowed ? (
          <div
            className="homeOption"
            onClick={() => {
              history.push("/Ispitivanja/v_predmet");
              setCurrentTable("v_predmet");
            }}
          >
            <span>Predmeti</span>
          </div>
        ) : null}
        {isPredmetAllowed ? (
          <div
            className="homeOption"
            onClick={async () => {
              history.push("/Ispitivanja/v_predmet");
              setCurrentTable("v_predmet");
              clickButton();
            }}
          >
            <span>Novi Predmet</span>
          </div>
        ) : null}
        <div
          className="homeOption"
          onClick={() => {
            generateReport("v_kalendar", "raspored", {
              pc_kn_datum: new Date().toLocaleDateString("de-DE")
            });
          }}
        >
          <span>Raspored</span>
        </div>
      </div>
      <Overlay />
    </div>
  );
};

const clickButton = () => {
  setTimeout(() => {
    const createButton = document.querySelector(".CreateButton");
    createButton && !createButton.classList.contains("disabled")
      ? createButton.click()
      : clickButton();
  }, 100);
};

const mapStateToProps = state => ({
  isPredmetAllowed: isActionAllowed(
    "v",
    "v_predmet",
    state.formGroups.formGroups
  )
});

export default withRouter(
  connect(
    mapStateToProps,
    { generateReport, setCurrentTable }
  )(Home)
);
