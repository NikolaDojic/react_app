import React, { useState, useEffect, useRef } from "react";
import { connect } from "react-redux";
import { fetchDataPromise, changeEditedItem } from "../../actions/grid";
import DateInput from "../../components/DateInput";
import Select from "../../components/Select";
import { filterObjectByKeys } from "../../utils";

const RasporedForm = ({ data, changeEditedItem }) => {
  const [users, setUsers] = useState(null);
  const [locations, setLocations] = useState(null);
  const napomenaEl = useRef(null);
  useEffect(() => {
    fetchDataPromise("v_korisnik").then(json => {
      const activeUsers = json
        .filter(usr => usr.kr_aktivan === "D")
        .map(user =>
          filterObjectByKeys(["kr_id", "kr_ime", "kr_prezime"], user)
        );
      setUsers(activeUsers);
    });
    fetchDataPromise("lokacija").then(json =>
      setLocations(
        json
          .filter(lk => lk.lk_aktivna === "D")
          .map(user => filterObjectByKeys(["lk_naziv", "lk_id"], user))
      )
    );
  }, []);
  useEffect(() => {
    if (napomenaEl && napomenaEl.current) {
      napomenaEl.current.value = data.kr_napomena || "";
    }
  }, [napomenaEl]); //eslint-disable-line

  return locations && users ? (
    <div className="RasporedForm Form">
      <label htmlFor="" key="kn_datum">
        <span>Datum:</span>
        <DateInput
          dateString={data["kn_datum"]}
          onChange={value => changeEditedItem({ kn_datum: value })}
        />
      </label>
      <label htmlFor="" key="lk_id">
        <span>Lokacija:</span>
        <Select
          setFirst={true}
          noLabel={true}
          initialValue={data["lk_id"]}
          value={data["lk_id"]}
          getItemId={item => item.lk_id}
          getItemValue={item => item.lk_id}
          getItemName={item => item.lk_naziv}
          items={locations || []}
          onChange={lk_id => {
            const location = locations.find(lct => lct.lk_id == lk_id); //eslint-disable-line
            changeEditedItem(location);
          }}
        />
      </label>
      <label htmlFor="" key="kr_id">
        <span>Korisnik:</span>
        <Select
          setFirst={true}
          noLabel={true}
          initialValue={data["kr_id"]}
          value={data["kr_id"]}
          getItemId={item => item.kr_id}
          getItemValue={item => item.kr_id}
          getItemName={item =>
            (item.kr_ime || "") + " " + (item.kr_prezime || "")
          }
          items={users || []}
          onChange={kr_id => {
            const user = users.find(usr => usr.kr_id == kr_id); //eslint-disable-line
            changeEditedItem(user);
          }}
        />
      </label>
      <label htmlFor="" key="rs_napomena">
        <span>Napomena:</span>
        <input
          onChange={e => changeEditedItem({ rs_napomena: e.target.value })}
          ref={napomenaEl}
        />
      </label>
    </div>
  ) : null;
};

const mapStateToProps = state => ({ data: state.grid.editedItem });

export default connect(
  mapStateToProps,
  { changeEditedItem }
)(RasporedForm);
