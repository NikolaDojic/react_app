import thunkMiddleware from "redux-thunk";
import { createStore, compose, applyMiddleware } from "redux";
import { routerMiddleware } from "connected-react-router";
import history from "./history";
import reducers from "./reducers";

const middleware = routerMiddleware(history);
var createStoreWithMiddleware = compose(
  applyMiddleware(middleware, thunkMiddleware)
)(createStore);
const store = createStoreWithMiddleware(
  reducers(history),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;
