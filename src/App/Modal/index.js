import React from "react";
import ModalDialog from "../../components/ModalDialog";
import Overlay from "../../components/Overlay";
import { connect } from "react-redux";
import { closeModal } from "../../actions/modal";

const Modal = ({
  buttons, //{text, type, doNotClose, action, className}
  className,
  closeModal,
  disableButtonActions = {}, // {ESCAPE, SPACE, ENTER},
  iconType,
  isHidden,
  message,
  renderInnerHtml = false,
  template,
  title,
  translateData
}) => {
  buttons = buttons.length ? buttons : [{ text: "ok", type: "ok" }];
  buttons = buttons.map(button => {
    const actionHolder = button.action;
    if (!button.hasOwnProperty("doClose")) {
      button.doClose = true;
    }
    button.action = button.doClose
      ? () => {
          closeModal();
          actionHolder && actionHolder();
        }
      : actionHolder || closeModal;
    return button;
  });
  return (
    !isHidden && (
      <div
        onLoad={e => document.querySelector(".modalWrapper").focus()}
        onKeyDown={e =>
          okCloseOnKeypress(buttons, closeModal, e, disableButtonActions)
        }
        tabIndex="0"
        className={`modalWrapper`}
      >
        <Overlay>
          <ModalDialog
            title={title}
            message={message}
            translateData={translateData}
            iconType={iconType}
            buttons={buttons}
            template={template}
            className={className}
            renderInnerHtml={renderInnerHtml}
          />
        </Overlay>
      </div>
    )
  );
};

const okCloseOnKeypress = (buttons, closeModal, e, disableButtonActions) => {
  if (
    e.target.tagName === "INPUT" &&
    !disableButtonActions.hasOwnProperty("SPACE")
  ) {
    disableButtonActions.SPACE = true;
  }
  const ESCAPE = 27;
  const ENTER = 13;
  const SPACE = 32;
  const okAction = buttons.reduce(
    (acc, button) => (button.type === "ok" ? button.action : acc),
    null
  );
  const cancelAction =
    buttons.reduce(
      (acc, button) => (button.type === "cancel" ? button.action : acc),
      null
    ) || closeModal;
  const isEscapePressed = !disableButtonActions.ESCAPE && e.keyCode === ESCAPE;
  const isEnterPressed = !disableButtonActions.ENTER && e.keyCode === ENTER;
  const isSpacePressed = !disableButtonActions.SPACE && e.keyCode === SPACE;
  try {
    if (isEscapePressed) cancelAction();
    else if (isEnterPressed || isSpacePressed) okAction();
  } catch (error) {
    console.log(
      "Modal could be not closed on key press, check if button objects have a type property"
    );
  }
};

const mapStateToProps = state => ({
  title: state.modal.title,
  message: state.modal.message,
  translateData: state.modal.translateData,
  iconType: state.modal.iconType,
  buttons: state.modal.buttons,
  isHidden: state.modal.isHidden,
  template: state.modal.template,
  className: state.modal.className,
  renderInnerHtml: state.modal.renderInnerHtml
});

export default connect(
  mapStateToProps,
  { closeModal }
)(Modal);
