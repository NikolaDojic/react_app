import React from "react";
import { connect } from "react-redux";
import { openModal } from "../../actions/modal";
import DateFilterModal from "./DateFilterModal.js";

const DateFilter = ({ openDatePicker, dateFrom, dateTo, tableName }) => {
  return (
    <div
      onClick={e => {
        openDatePicker();
      }}
      className="dateFilterValueWrapper"
    >
      <div className="dateFilterInput">
        {dateFrom}
        {tableName !== "v_kalendar" && dateFrom && dateTo ? " do " : null}
        {tableName !== "v_kalendar" ? dateTo : null}
      </div>
    </div>
  );
};

const mapStateToProps = (state, ownProps) => {
  const keyFrom = ownProps.column.colId;
  const keyTo = ownProps.column.colId + "_to";
  return {
    dateFrom: (state.grid.filters[state.grid.activeTable] || {})[keyFrom],
    dateTo: (state.grid.filters[state.grid.activeTable] || {})[keyTo]
  };
};
const mapDispatchToProps = (dispatch, ownProps) => {
  const keyFrom = ownProps.column.colId;
  const keyTo = ownProps.column.colId + "_to";
  return {
    openDatePicker: () =>
      dispatch(
        openModal({
          title: "Datum",
          className: "noButtonsModal dateFilterModal",
          template: (
            <DateFilterModal
              keyFrom={keyFrom}
              keyTo={keyTo}
              tableName={ownProps.tableName || ""}
            />
          )
        })
      )
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DateFilter);
