import React, { Component } from "react";
import { connect } from "react-redux";

const FloatingFilter = ({
  api,
  col,
  type,
  tableName,
  setFilters,
  onFloatingFilterChanged,
  value,
  agGridReact
}) => {
  return (
    <div
      className="PickerFilter ag-wrapper ag-input-wrapper ag-text-field-input-wrapper"
      onKeyDown={e => {
        if (e.key === "Tab") {
          let nextColIndex = 0;
          const displayedColumns = agGridReact.columnApi.getAllDisplayedColumns();
          displayedColumns.some((column, index) => {
            if (column.colId === col) {
              const direction = e.shiftKey ? -1 : 1;
              const nextIndex = index + direction;
              if (nextIndex < displayedColumns.length) {
                nextColIndex = nextIndex;
              }
              return true;
            }
            return false;
          });
          const nextColId = displayedColumns[nextColIndex].colId;
          !nextColIndex &&
            document
              .querySelector(".ag-body-horizontal-scroll-viewport")
              .scroll({
                top: 0,
                left: 0
              });
          setTimeout(
            () => document.getElementById(nextColId + "_filter").focus(),
            300
          );
        }
      }}
    >
      <input
        id={col + "_filter"}
        className="ag-floating-filter-input labelsFilterInput ag-input-field-input ag-text-field-input"
        value={value}
        onChange={e => {
          window.tableName = tableName;
          const filterModel = api.getFilterModel();
          filterModel[col] = {
            filterType: type === "c" ? "text" : "number",
            filter: e.target.value
          };
          (api || window.gridApi[tableName]).setFilterModel(filterModel);
        }}
        type={type === "c" ? "text" : "number"}
      />
    </div>
  );
};

const mapStateToProps = (state, ownProps) => ({
  value: (state.grid.filters[ownProps.tableName] || {})[ownProps.col] || ""
});

let ConnectedFilter = connect(mapStateToProps)(FloatingFilter);

//This is just a wrapper to avoid warnings from ag-grid library
class FilterWrapper extends Component {
  onParentModelChanged(a) {
    //Because ag grid will raise a warning if method not present
  }

  destroy(e) {
    console.log(e);
  }
  render() {
    return (
      <ConnectedFilter
        onFloatingFilterChanged={this.props.onFloatingFilterChanged}
        {...this.props}
      />
    );
  }
}

export default FilterWrapper;
