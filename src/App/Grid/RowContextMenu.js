import React from "react";
import { connect } from "react-redux";
import { MenuItem } from "react-contextmenu";
import { openModal } from "../../actions/modal";
import SlikeModal from "./SlikeModal";

const RowContextMenu = ({ predmet, openModal, onHide }) => {
  if (!predmet) return null;
  return (
    <div>
      <div className="contextMenuHeader">Predmet {predmet.pr_broj}</div>
      <MenuItem
        key={"slike"}
        data={{ id: predmet.pr_id }}
        onClick={() => {
          onHide();
          openModal({
            title: `Slike predmeta ${predmet.pr_broj}`,
            className: "slikeModal",
            template: (
              <SlikeModal
                type="slike"
                pr_id={predmet.pr_id}
                pr_broj={predmet.pr_broj}
              />
            )
          });
        }}
        className="showHideColumn"
        preventClose={false}
      >
        <span>Slike vozila</span>
      </MenuItem>
      <MenuItem
        key={"dokumenta"}
        data={{ id: predmet.pr_id }}
        onClick={() => {
          onHide();
          openModal({
            title: `Slike predmeta ${predmet.pr_broj}`,
            className: "slikeModal",
            template: (
              <SlikeModal
                type="doc"
                pr_id={predmet.pr_id}
                pr_broj={predmet.pr_broj}
              />
            )
          });
        }}
        className="showHideColumn"
        preventClose={false}
      >
        <span>Priložena dokumenta</span>
      </MenuItem>
    </div>
  );
};

const mapStateToProps = (state, ownProps) => ({
  predmet: state.grid.selectedRow[ownProps.tableName]
});

export default connect(
  mapStateToProps,
  { openModal }
)(RowContextMenu);
