import React from "react";
import { connect } from "react-redux";
import Overlay from "../../components/Overlay";
import Loader from "../../components/Overlay/Loader";

const mapStateToProps = (state, ownProps) => ({
  isHidden: !(
    Boolean(state.grid.isFetching) &&
    ownProps.tableName === state.grid.activeTable
  ),
  children: <Loader />,
  className: "gridOverlay"
});

export default connect(mapStateToProps)(Overlay);
