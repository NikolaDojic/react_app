import React, { useState } from "react";
import { MenuItem } from "react-contextmenu";

const ColumnContextMenu = ({ columns, colApi, tableName }) => {
  const [localVisible, setVisible] = useState({});
  return (
    <div className="ColumnContextMenu">
      <div className="contextMenuHeader">Podesi vidljive kolone:</div>
      {columns
        .sort((a, b) => a.headerName.localeCompare(b.headerName))
        .map((col, index) => {
          const { visible, colId } = colApi.getColumn(col.colId) || {};
          return (
            <MenuItem
              key={colId || index}
              data={{ id: colId, isVisible: visible }}
              onClick={(e, data) => {
                colApi.setColumnVisible(data.id, !data.isVisible);
                setVisible({ ...localVisible, [data.id]: !data.isVisible });
              }}
              className="showHideColumn"
              selected={localVisible[colId]}
              preventClose={true}
            >
              <span key={index + colId}>{col.headerName}</span>
              {(localVisible.hasOwnProperty(colId) ? (
                localVisible[colId]
              ) : (
                visible
              )) ? (
                <span> &#10003; </span>
              ) : null}
            </MenuItem>
          );
        })}
    </div>
  );
};

export default ColumnContextMenu;
