import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { defaultGridOptions, defaultColDef } from "./DefaultGridOptions";
import { AgGridReact } from "ag-grid-react";
import { gridColumns, gridData } from "../../utils";
import { closeModal } from "../../actions/modal";
import {
  deleteColumns,
  deleteData,
  deleteSelectedRow,
  fetchColumns,
  fetchData,
  scrollFetchData,
  setActiveTable,
  setEditedItem,
  setFilters,
  setSelectedRow
} from "../../actions/grid";
import ShowHideColumns from "./ShowHideColumns";
import { setSertVisible, predmetVisible } from "../../actions/predmeti";

import { ContextMenuTrigger } from "react-contextmenu";
import { copyToClipboard } from "../../utils";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import DateFilter from "./DateFilter.js";
import "./Grid.css";
import FloatingFilter from "./FloatingFilter";
import GridOverlays from "./GridOverlays";

const Grid = ({
  clearTable,
  columns,
  data,
  defaultGridOptions,
  fetchColumns,
  fetchData,
  filters,
  onRowDoubleClicked,
  primaryKey,
  scrollFetchData,
  selectedRow,
  setActiveTable,
  setFilters,
  setSelectedRow,
  tableName,
  isDetail = false
}) => {
  const [gridApi, setGridApi] = useState(null); //eslint-disable-line
  const [columnApi, setColumnApi] = useState(null);
  const [rowData, setRowData] = useState(gridData(data, primaryKey));
  const [columnDefs, setColumDefs] = useState(gridColumns(columns, tableName));
  const [rowRightClick, setRowRightClick] = useState(false);

  useEffect(() => {
    setRowData(gridData(data, primaryKey));
  }, [data, primaryKey]);

  useEffect(() => {
    if (
      rowData &&
      rowData.length &&
      (!selectedRow || !Object.keys(selectedRow))
    ) {
      const node = gridApi.getDisplayedRowAtIndex(0);

      node && node.setSelected(true);
    }
  }, [rowData, gridApi]); //eslint-disable-line

  useEffect(() => {
    setColumDefs(gridColumns(columns, tableName));
  }, [columns, tableName]);

  useEffect(() => {
    if (gridApi) {
      window.gridApi = window.gridApi || {};
      window.gridApi[window.tableName] = gridApi;
    }
  }, [gridApi, tableName]);

  useEffect(() => {
    if (columnApi) {
      window.columnApi = window.columnApi || {};
      window.columnApi[window.tableName] = columnApi;
    }
  }, [columnApi]);

  const saveTableState = columnApi => {
    if (columnApi) {
      const colState = columnApi.getColumnState();
      const gridSettings = { [window.tableName]: colState };
      localStorage.setItem(window.gridSettingsId, JSON.stringify(gridSettings));
    }
  };
  const loadTableState = columnApi => {
    if (columnApi) {
      const gridSettings = JSON.parse(
        localStorage.getItem(window.gridSettingsId) || "{}"
      );
      const colState = gridSettings[window.tableName] || {};
      if (colState)
        columnApi.applyColumnState({
          state: colState,
          applyOrder: true
        });
    }
  };

  const onSelectionChanged = e => {
    try {
      const selectedRow = e.api.getSelectedNodes()[0] || {};
      if (selectedRow.data) setSelectedRow(selectedRow.data || {});
    } catch (err) {
      console.log(err);
      console.log("Changing page");
    }
  };

  const onFilterChanged = e => {
    clearTimeout(window.filterDebounceTimer);
    const gridFilters = e.api.getFilterModel();
    let stateFilters = {};
    Object.keys(gridFilters).forEach(
      key => (stateFilters[key] = gridFilters[key].filter)
    );
    setFilters(stateFilters);
    window.filterDebounceTimer = setTimeout(
      () => fetchData(window.tableName),
      500
    );
  };

  const onGridReady = params => {
    setGridApi(params.api);
    setColumnApi(params.columnApi);
    params.api.sizeColumnsToFit();
    console.log("grid ready");
    let allColumnIds = [];
    params.columnApi.getAllColumns().forEach(function(column) {
      allColumnIds.push(column.colId);
    });
    params.columnApi.autoSizeColumns(allColumnIds, true);
  };

  const onNewColumnsLoaded = (e, a) => {
    loadTableState(e.columnApi, window.tableName);
  };

  const onColumnResized = e => {
    saveTableState(e.columnApi, window.tableName);
  };

  const onColumnVisible = e => {
    saveTableState(e.columnApi, window.tableName);
  };

  const onBodyScroll = e => {
    if (e.top > 0 && window.gridScrollSize - e.top < 1000) {
      const rowCnt = e.api.getDisplayedRowCount();
      if (rowCnt >= 100) {
        scrollFetchData();
      }
    }
  };

  const onCellClicked = params => {
    try {
      if (params.event.ctrlKey) {
        const data = params.data[params.column.colId];
        copyToClipboard(data);
      }
    } catch (e) {
      console.log(e);
    }
  };

  const onViewportChanged = e => {
    window.gridScrollSize = (
      document.querySelector("div.ag-center-cols-viewport") || {
        scrollHeight: -1
      }
    ).scrollHeight;
    if (window.gridScrollSize > 0) {
      const viewportSize = (
        document.querySelector("div.ag-body-viewport") || {
          offsetHeight: 0
        }
      ).offsetHeight;
      window.gridScrollSize -= viewportSize;
    }
  };

  const navigateToNextCell = params => {
    let previousCell = params.previousCellPosition;
    let suggestedNextCell = params.nextCellPosition;

    const KEY_UP = 38;
    const KEY_DOWN = 40;
    const KEY_LEFT = 37;
    const KEY_RIGHT = 39;

    switch (params.key) {
      case KEY_DOWN:
        previousCell = params.previousCellPosition;
        const nextSelectedRowIndex = previousCell.rowIndex + 1;
        const selectedRow = window.gridApi[
          window.tableName
        ].getDisplayedRowAtIndex(nextSelectedRowIndex);
        selectedRow && selectedRow.setSelected(true);
        return suggestedNextCell;
      case KEY_UP:
        previousCell = params.previousCellPosition;
        window.gridApi[window.tableName].forEachNode(function(node) {
          if (previousCell.rowIndex - 1 === node.rowIndex) {
            node.setSelected(true);
          }
        });
        return suggestedNextCell;
      case KEY_LEFT:
      case KEY_RIGHT:
        return suggestedNextCell;
      default:
        throw "this will never happen, navigation is always one of the 4 keys above"; //eslint-disable-line
    }
  };

  useEffect(() => {
    if (tableName && !isDetail) {
      window.tableName = tableName;
      fetchData(tableName);
      fetchColumns(tableName);
    }
    return () => {
      clearTable();
    };
  }, [tableName]); //eslint-disable-line

  return (
    <ContextMenuTrigger
      holdToDisplay={-1}
      id={`setColumns${tableName}`}
      disableIfShiftIsPressed={true}
      attributes={{ className: "asdfasdf" }}
    >
      <div
        className="GridWrapper ag-theme-balham"
        style={{ width: "100%", height: "100%" }}
        onMouseDown={e => setActiveTable(tableName)}
      >
        {columnApi ? (
          <ShowHideColumns
            tableName={tableName}
            colApi={columnApi}
            columns={gridColumns(columns)}
            onHide={() => setRowRightClick(false)}
            isRowMenu={rowRightClick && tableName === "v_predmet"}
          />
        ) : null}
        <AgGridReact
          gridOptions={{
            ...defaultGridOptions,
            onSelectionChanged,
            context: { tableName },
            onGridReady,
            onFilterChanged,
            onColumnResized,
            onColumnVisible,
            onNewColumnsLoaded,
            onRowDoubleClicked,
            navigateToNextCell,
            onBodyScroll,
            onViewportChanged,
            onCellClicked,
            onCellContextMenu: gridEvent => {
              setRowRightClick(true);
              gridEvent.node.setSelected(true);
              gridEvent.event.stopPropagation();
              gridEvent.event.preventDefault();
            }
          }}
          rowData={rowData}
          immutableData={true}
          columnDefs={columnDefs}
          defaultColDef={defaultColDef}
          frameworkComponents={{
            dateFilter: DateFilter,
            floatingFilter: FloatingFilter
          }}
          enableCellTextSelection={true}
          ensureDomOrder={true}
        />
        <GridOverlays tableName={tableName} />
      </div>
    </ContextMenuTrigger>
  );
};

const mapStateToProps = (state, { tableName }) => ({
  tableName: tableName || state.grid.currentTable,
  data: [...(state.grid.data[tableName] || [])],
  columns: [...(state.grid.columns[tableName] || [])],
  primaryKey: state.grid.primaryKey[tableName] || "",
  filters: state.grid.filters[tableName],
  selectedRow: state.grid.selectedRow[tableName]
});

const mapDispatchToProps = (dispatch, { tableName, onRowDoubleClicked }) => ({
  defaultGridOptions: defaultGridOptions(dispatch),
  setSelectedRow: payload => dispatch(setSelectedRow(payload)),
  clearTable: () => {
    dispatch(deleteColumns(tableName));
    dispatch(deleteData(tableName));
    dispatch(deleteSelectedRow(tableName));
    dispatch(setFilters({}));
    dispatch(setEditedItem({}));
    dispatch(closeModal());
    dispatch(predmetVisible(false));
    dispatch(setSertVisible(false));
  },
  fetchColumns: tableName => dispatch(fetchColumns(tableName)),
  fetchData: tableName => dispatch(fetchData(tableName)),
  setFilters: filters => dispatch(setFilters(filters)),
  setActiveTable: tableName => dispatch(setActiveTable(tableName)),
  onRowDoubleClicked:
    onRowDoubleClicked || (e => document.getElementById("editButton").click()),
  scrollFetchData: () => dispatch(scrollFetchData())
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Grid);
