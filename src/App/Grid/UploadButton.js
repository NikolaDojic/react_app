import React from "react";
import { connect } from "react-redux";
import Button from "../../components/Button";
import { uploadFiles } from "../../actions/predmeti";
import "./UploadButton.css";

const UploadButton = ({
  type = "slike",
  label,
  uploadFiles,
  prId,
  onUpload
}) => {
  return (
    <Button buttonKey="uploadButton" className="UploadButton">
      <span>
        {label || type === "slike" ? "Dodaj Slike" : "Dodaj dokumenta"}
        <input
          type="file"
          accept="image/png, image/jpeg, image/jpg, .pdf"
          multiple={true}
          onChange={e => {
            uploadFiles(e.target.files, type, prId, onUpload);
          }}
        />
      </span>
    </Button>
  );
};

const mapStateToProps = (state, ownProps) => ({
  type: ownProps.type,
  label: ownProps.label,
  onUpload: ownProps.onUpload
});

const mapDispatchToProps = dispatch => ({
  uploadFiles: (files, type, prId, callback) =>
    dispatch(uploadFiles(files, type, prId, callback))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UploadButton);
