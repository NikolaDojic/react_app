import React, { useEffect, useState, useRef } from "react";
import { connect } from "react-redux";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { closeModal } from "../../actions/modal";
import { Carousel } from "react-responsive-carousel";
import UploadButton from "./UploadButton";
import fetch from "../../utils/fetch";
import Config from "../../config";

const SlikeModal = ({ type, pr_id, pr_broj, closeModal }) => {
  const [slike, setSlike] = useState({ slike: [], doc: [] });
  const contEl = useRef(null);
  useEffect(() => {
    fetch(`${Config.API.images}?pr_id=${pr_id}`)
      .then(res => res.json())
      .then(json => {
        console.log(json);
        setSlike(json);
      });
  }, [pr_id]);

  return (
    <div className="SlikeModal" ref={contEl}>
      <div className="SlikeModalBody">
        <Carousel>
          {slike[type] && slike[type].length ? (
            slike[type].map((src, index) => {
              src = "./imgs/" + src;
              if (src.endsWith("pdf")) {
                return (
                  <div key={index} className={"carouselFrame"}>
                    <iframe
                      title={index}
                      key={index}
                      height={
                        contEl.current
                          ? `${contEl.current.offsetHeight - 40}px`
                          : "100%"
                      }
                      width={
                        contEl.current
                          ? `${contEl.current.offsetWidth}px`
                          : "100%"
                      }
                      src={src}
                      scrolling="auto"
                    />
                  </div>
                );
              } else {
                return (
                  <div key={index} className={"carouselFrame"}>
                    <img key={index} src={src} alt="slika predmeta" />
                  </div>
                );
              }
            })
          ) : (
            <div className="noImages">
              Trenutno nema slika za predmet broj {pr_id}
            </div>
          )}
        </Carousel>
      </div>
      <div className="Buttons">
        <button
          className={`defaultButton deleteImgButton${
            !slike[type].length ? " disabled" : ""
          }`}
          onClick={() => {
            const img = document.querySelector(".slide.selected div > *");
            const fileName = img.src.split("/").pop();
            let status;
            const arrow = document.querySelector(
              ".carousel.carousel-slider > button:not(.control-disabled)"
            );
            fetch(
              `${
                Config.API.images
              }?pr_id=${pr_id}&f_type=${type}&filename=${fileName}`,
              { method: "DELETE" }
            )
              .then(res => {
                status = res.status;
                return res.json();
              })
              .then(json => {
                console.log(json);
                if (status === 200) {
                  arrow && arrow.click();
                  setSlike(json);
                }
              });
          }}
        >
          Obriši
        </button>
        <UploadButton
          onUpload={data => setSlike({ ...slike, ...data })}
          type={type}
          prId={pr_id}
        />
        <button
          className={`defaultButton${!slike[type].length ? " disabled" : ""}`}
          onClick={e => {
            const img = document.querySelector(".slide.selected div > *");
            console.log(img.src);
            var elem = window.document.createElement("a");
            elem.target = "_blank";
            elem.href = img.src;
            elem.download = pr_broj + "." + img.src.split(".").pop();
            document.body.appendChild(elem);
            elem.click();
            URL.revokeObjectURL(elem.href);
            document.body.removeChild(elem);
          }}
        >
          Sačuvaj
        </button>
        <button className="defaultButton" onClick={() => closeModal()}>
          Zatvori
        </button>
      </div>
    </div>
  );
};

const mapStateToProps = state => ({});

export default connect(
  mapStateToProps,
  { closeModal }
)(SlikeModal);
