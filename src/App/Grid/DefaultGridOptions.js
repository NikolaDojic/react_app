import { setSelectedRow } from "../../actions/grid";
import "./Grid.css";

const ROW_HEIGHT = 30;
const HEADER_HEIGHT = 30;
export const defaultGridOptions = dispatch => {
  const gridOptions = {
    onGridReady: params => {
      gridOptions.api = params.api;
      gridOptions.columnApi = params.ColumnApi;
      // gridOptions.api.sizeColumnsToFit();
      console.log("grid ready");
    },
    onColumnMoved: e => {
      window.activeTable !== "v_predmet" && e.api.sizeColumnsToFit();
    },
    onNewColumnsLoaded: e => {
      console.log("new columns loaded", e);
      e.api.sizeColumnsToFit();
    },
    rowHeight: ROW_HEIGHT,
    rowSelection: "single",
    getRowNodeId: data =>
      data.hasOwnProperty("id") && typeof data.id !== "undefined"
        ? data.id
        : console.warn("no row id:", data) || JSON.stringify(data),
    onSelectionChanged: e => {
      try {
        const selectedRow = (gridOptions.api.getSelectedNodes()[0] || {}).id;
        dispatch(setSelectedRow(selectedRow));
      } catch (err) {
        console.log("Changing page");
      }
    },
    onGridSizeChanged: e => {
      setTimeout(() => {
        try {
          gridOptions && gridOptions.api && gridOptions.api.sizeColumnsToFit();
        } catch (err) {
          console.log(err);
        }
      }, 0);
    },
    onRowDataChanged: e => {},
    headerHeight: HEADER_HEIGHT,
    floatingFiltersHeight: 35,
    suppressDragLeaveHidesColumns: true,
    suppressCellSelection: false,
    rowBuffer: 1000,
    suppressMovableColumns: false
  };
  return gridOptions;
};

export const defaultColDef = {
  animateRows: true,
  animateHeader: true,
  minWidth: 50,
  sortable: true,
  resizable: true,
  floatingFilter: true,
  comparator: (valueA, valueB, nodeA, nodeB, isInverted) =>
    valueA.name < valueB.name ? -1 : valueA.name > valueB.name ? 1 : 0,
  filter: true,
  filterParams: {
    suppressMenu: true,
    suppressFilterButton: true,
    textCustomComparator: (operator, value, filterText) => {
      return true; //Return every row because filtering is on the be
    }
  },
  filterValueGetter: ({ data, colDef }) => {
    return data[colDef.colId] || "";
  },
  suppressMenu: true,
  suppressFilterButton: true,
  suppressDragLeaveHidesColumns: true,
  suppressCellFlash: true,
  suppressHorizontalScroll: true
};
