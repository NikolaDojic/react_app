import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import DatePicker from "react-datepicker";
import { closeModal } from "../../actions/modal";
import { setFilters, fetchData } from "../../actions/grid";
import { localeStringToDate } from "../../utils";
import Button from "../../components/Button";
import "react-datepicker/dist/react-datepicker.css";

const DateFilterModal = ({
  dateFrom,
  dateTo,
  keyFrom,
  keyTo,
  setFilters,
  fetchData,
  closeModal,
  tableName
}) => {
  const [currentDateFrom, setCurrentDateFrom] = useState(
    dateFrom || new Date().toLocaleDateString("de-DE")
  );
  const [currentDateTo, setCurrentDateTo] = useState(
    dateTo || new Date().toLocaleDateString("de-DE")
  );
  useEffect(() => {
    setCurrentDateFrom(dateFrom);
    setCurrentDateTo(dateTo);
  }, []); //eslint-disable-line
  return (
    <div className="DateFilterModal">
      <div className="dateInputWrapper">
        <div className="od">
          {tableName === "v_kalendar" ? null : <span>Od:</span>}
          <DatePicker
            selected={localeStringToDate(currentDateFrom)}
            onChange={(e, a) => {
              console.log(e);
              const localString = e.toLocaleDateString("de-DE");
              setCurrentDateFrom(localString);
            }}
            dateFormat="dd.MM.yyyy"
          />
        </div>
        {tableName === "v_kalendar" ? null : (
          <div className="do">
            <span>Do:</span>
            <DatePicker
              selected={localeStringToDate(currentDateTo)}
              onChange={(e, a) => {
                const localString = e.toLocaleDateString("de-DE");
                setCurrentDateTo(localString);
              }}
              dateFormat="dd.MM.yyyy"
            />
          </div>
        )}
      </div>
      <div className="modalButtons">
        <Button onClick={() => closeModal()}>Odbaci</Button>
        <Button
          onClick={() => {
            setFilters({ [keyFrom]: currentDateFrom, [keyTo]: currentDateTo });
            fetchData();
            closeModal();
          }}
        >
          Snimi
        </Button>
      </div>
    </div>
  );
};

const mapStateToProps = (state, ownProps) => {
  const keyFrom = ownProps.keyFrom;
  const keyTo = ownProps.keyTo || ownProps.key + "_to";
  return {
    dateFrom: state.grid.filters[ownProps.tableName][keyFrom],
    dateTo: state.grid.filters[ownProps.tableName][keyTo],
    keyFrom,
    keyTo
  };
};

export default connect(
  mapStateToProps,
  { setFilters, closeModal, fetchData }
)(DateFilterModal);
