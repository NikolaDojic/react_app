import React, { useState } from "react";
import { ContextMenu } from "react-contextmenu";
import RowContextMenu from "./RowContextMenu";
import ColumnContextMenu from "./ColumnContextMenu.js";

const ShowHideColumns = ({
  colApi,
  columns = [],
  tableName,
  predmet,
  isRowMenu,
  onHide
}) => {
  const [isMenuHidden, setMenuHidden] = useState(true);
  if (!colApi) return null;
  return (
    <ContextMenu
      onHide={e => {
        setMenuHidden(true);
        onHide && onHide();
      }}
      onShow={e => setMenuHidden(false)}
      id={`setColumns${tableName}`}
      className={`listWrapper contextMenu${isMenuHidden ? " menuHidden" : ""}`}
    >
      {isRowMenu ? (
        <RowContextMenu tableName={tableName} onHide={onHide} />
      ) : (
        <ColumnContextMenu
          columns={columns}
          colApi={colApi}
          tableName={tableName}
        />
      )}
    </ContextMenu>
  );
};

export default ShowHideColumns;
