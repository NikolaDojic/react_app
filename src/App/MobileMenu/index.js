import React, { useState } from "react";
import MenuButton from "../../components/MenuButton";
import MenuList from "./MenuList";

function MobileMenu() {
  const [isExpanded, setIsExpanded] = useState(false);
  return (
    <div className="dropdown">
      <MenuButton onClick={() => setIsExpanded(!isExpanded)} />
      <div className="mobileMenuWrapper">
        <MenuList
          closeMobileMenu={() => setIsExpanded(false)}
          className={`mobileMenuList ${
            isExpanded ? "menuExpanded" : "menuCollapsed"
          }`}
        />
      </div>
    </div>
  );
}

export default MobileMenu;
