import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { setCurrentTable } from "../../../actions/grid";

const MenuItems = ({
  closeMobileMenu,
  history,
  isItemSelected,
  items,
  category,
  setCurrentTable
}) => {
  return (
    <ul className="list">
      {items.map(item => (
        <li
          key={item.path}
          id={item}
          onClick={() => {
            history.push(`/${category}/${item.path}`);
            setCurrentTable(item.path);
            closeMobileMenu && closeMobileMenu();
          }}
          className={`menuListItem listItem${
            isItemSelected(item.path) ? " selected" : ""
          }`}
        >
          <div>{item.label}</div>
        </li>
      ))}
    </ul>
  );
};

const mapStateToProps = (state, ownProps) => {
  const { history } = ownProps;
  return {
    isItemSelected: item => item === history.location.pathname.split("/").pop()
  };
};
export default withRouter(
  connect(
    mapStateToProps,
    { setCurrentTable }
  )(MenuItems)
);
