import React, { useState } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import AnimateHeight from "react-animate-height";
import MenuItems from "./MenuItems";
import "./MenuList.css";

const Menu = ({
  isItemSelected,
  className,
  history,
  closeMobileMenu,
  categories,
  isHidden
}) => {
  const [expandedCategories, setExpandedCategories] = useState([]);

  categories = categories.filter(category => (category.items || []).length);
  return !isHidden ? (
    <div className={`MenuList ${className}`}>
      <h1 className="Srbolab" onClick={() => history.push("/home")}>
        Srbolab
      </h1>
      <div>
        {categories.map(category => {
          const isCategorySelected =
            (category.label === "plugins" &&
              history.location.pathname.slice(1) === "plugins") ||
            category.items.some(item => isItemSelected(item.path));
          if (
            isCategorySelected &&
            !expandedCategories.includes(category.label)
          ) {
            setExpandedCategories([...expandedCategories, category.label]);
          }
          const isExpanded = expandedCategories.includes(category.label);
          return (
            <AnimateHeight
              key={category.label}
              height={isExpanded ? "auto" : 40}
              style={{ flexShirnk: 0 }}
              className={`${isCategorySelected ? "selectedAccordion" : ""}${
                isExpanded ? " expanded" : ""
              }`}
            >
              <div
                className={`category${
                  isCategorySelected ? " selectedCategory" : ""
                }`}
                onClick={() =>
                  !isCategorySelected &&
                  (isExpanded
                    ? setExpandedCategories(
                        expandedCategories.filter(cat => cat !== category.label)
                      )
                    : setExpandedCategories([
                        ...expandedCategories,
                        category.label
                      ]))
                }
              >
                <div>{category.label}</div>
              </div>
              <MenuItems
                items={category.items}
                closeMobileMenu={closeMobileMenu}
                category={category.label}
              />
            </AnimateHeight>
          );
        })}
      </div>
    </div>
  ) : null;
};

const mapStateToProps = (state, ownProps) => {
  const { history } = ownProps;

  return {
    isHidden: !state.login.loggedIn,
    isItemSelected: item => item === history.location.pathname.split("/").pop(),
    categories:
      state.formGroups.formGroups.map(fg => ({
        label: fg.title,
        items: (fg.forms || []).map(d => ({
          label: d.title,
          path: d.tables.source
        }))
      })) || []
  };
};
export default withRouter(connect(mapStateToProps)(Menu));
