import React from "react";
import TextBox from "../../components/TextBox";
import Select from "../../components/Select";
import "./PredmetiInput.css";

const PredmetiInput = ({
  initialValue,
  onChange,
  className,
  label,
  inputAttrs,
  type = "text",
  isActive = true,
  selectAttrs = { items: [] },
  warning = "",
  error = "",
  implicitChange = false
}) => {
  const input =
    type === "text" ? (
      <TextBox
        label={label}
        onChange={onChange}
        initialValue={initialValue}
        inputAttrs={inputAttrs}
        warning={warning}
        error={error}
        implicitChange={implicitChange}
      />
    ) : (
      <Select
        label={label}
        onChange={onChange}
        initialValue={initialValue}
        {...selectAttrs}
        inputAttrs={inputAttrs}
      />
    );

  const fakeInput = (
    <div className={`fakeInput ${className}`}>
      <span key={"label"} className="label">
        {label} :
      </span>
      <span key={"value"} className="value">
        {type === "select"
          ? (selectAttrs.items.find(item => item.value === initialValue) || {})
              .name || initialValue
          : initialValue}
      </span>
    </div>
  );
  return <div className="PredmetiInput">{isActive ? input : fakeInput}</div>;
};

export default PredmetiInput;
