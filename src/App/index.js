import React from "react";
import { connect } from "react-redux";
import Modal from "./Modal";
import Logout from "./Header/Logout";
import { Route, Switch, Redirect } from "react-router";
import { ConnectedRouter } from "connected-react-router";
import { isActionAllowed } from "../utils";
import Login from "../pages/Login";
import Main from "../pages/Main";
import Home from "../pages/Home";
import Predmet from "../pages/Predmet";
import Sertifikat from "../pages/Sertifikat";
import MenuList from "./MobileMenu/MenuList";
import "./App.css";

const App = ({ location, history, loggedIn, isPageAllowed }) => {
  const renderProtectedPage = page => {
    const table = history.location.pathname.split("/").pop();
    if (!loggedIn) {
      return <Redirect to={"/login"} />;
    } else if (table !== "home" && table !== "login" && !isPageAllowed(table)) {
      setTimeout(() => history.push("/home"), 0);
      return <Home />;
    } else {
      return page;
    }
  };
  return (
    <div id="App" className="App">
      <ConnectedRouter history={history}>
        <Logout />
        <main>
          <div id="pageContainer">
            <MenuList className="desktopMenu" />
            <Switch>
              <Route path="/login" component={Login} />
              <Route
                path="/home"
                render={() => renderProtectedPage(<Home />)}
              />
              <Route path="/" render={() => renderProtectedPage(<Main />)} />
              <Route path="*" render={() => <Redirect to="/login" />} />
            </Switch>
          </div>
          <Modal />
          <Predmet />
          <Sertifikat />
        </main>
      </ConnectedRouter>
    </div>
  );
};
const mapStateToProps = state => ({
  //forece update on location change
  location: state.router.location.pathname,
  loggedIn: state.login.loggedIn,
  isPageAllowed: page => isActionAllowed("v", page, state.formGroups.formGroups)
});

export default connect(mapStateToProps)(App);
