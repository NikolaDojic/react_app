import React from "react";
import "./Header.css";
import Menu from "./Menu";
import Logout from "./Logout";
import { withRouter } from "react-router";

const Header = ({ history }) => {
  return (
    <header>
      <div className="logoContainer">
        <span className="headerText" onClick={() => history.push("/home")}>
          srbolab
        </span>
      </div>
      <Menu />
      <Logout />
    </header>
  );
};

export default withRouter(Header);
