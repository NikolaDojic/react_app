import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { generateUrl } from "../../utils";
import { setCurrentTable } from "../../actions/grid";

const Menu = ({ isHidden, pages, history, location, setCurrentTable }) => {
  return isHidden ? null : (
    <div className="HeaderMenu">
      {pages.map(page => (
        <span
          className={`menuItem ${
            page.url.split("/")[1] === location ? "selectedMenuItem" : ""
          }`}
          key={page.url}
          onClick={() => {
            history.push(page.url);
            setCurrentTable(page.url.split("/").pop());
          }}
        >
          {page.title}
        </span>
      ))}
    </div>
  );
};

const mapStateToProps = state => {
  return {
    pages: state.formGroups.formGroups
      .filter(form => form.forms.length)
      .map(page => ({
        url: generateUrl(page),
        title: page.title
      })),
    location: state.router.location.pathname.slice(1).split("/")[0],
    isHidden: !state.login.loggedIn
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    { setCurrentTable }
  )(Menu)
);
