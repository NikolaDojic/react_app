import React from "react";
import { connect } from "react-redux";
import { logout } from "../../actions/login.js";

const Logout = ({ logout, isHidden }) => {
  return isHidden ? null : (
    <div className="Logout" onClick={logout}>
      Odjava
    </div>
  );
};
const mapStateToProps = state => ({
  isHidden: !state.login.loggedIn
});
export default connect(
  mapStateToProps,
  { logout }
)(Logout);
