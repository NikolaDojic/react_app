import {
  FINISHED_LOGIN_REQUEST,
  RECEIVE_LOGIN,
  REQUEST_LOGIN,
  SET_LOGIN_PASSWORD,
  SET_LOGIN_USERNAME,
  SET_LOGGED_IN,
  SET_IS_REFRESHING_TOKEN,
  SET_CLIENT_IP
} from "./types";
import { fetchForms } from "./forms";
import { generateUrl } from "../utils"; //eslint-disable-line
import { parseJwt, isTokenExpired } from "../utils/jwt"; //eslint-disable-line
import { fetchDocuments } from "./documents";
import { fetchUsluge } from "./usluge";
import { fetchStatusi } from "./statusi";
import { fetchFormFilters } from "./predmeti";
import { openModal } from "./modal";
// import { setCurrentTable } from "./grid";
import publicIp from "public-ip";
import history from "../history";
import Config from "../config";

export const getClientIp = async () =>
  await publicIp.v4({
    fallbackUrls: ["https://ifconfig.co/ip"]
  });

export const setClientIp = ip => ({
  type: SET_CLIENT_IP,
  payload: ip
});

export const requestLogin = () => ({
  type: REQUEST_LOGIN
});

export const receiveLogin = credentials => ({
  type: RECEIVE_LOGIN,
  payload: credentials
});

export const setUsername = username => ({
  type: SET_LOGIN_USERNAME,
  payload: username
});

export const setPassword = password => ({
  type: SET_LOGIN_PASSWORD,
  payload: password
});

export const setLoggedIn = loggedIn => ({
  type: SET_LOGGED_IN,
  payload: loggedIn
});

export const setIsRefreshing = isRefreshing => ({
  type: SET_IS_REFRESHING_TOKEN,
  payload: isRefreshing
});

export const requestFinished = () => ({
  type: FINISHED_LOGIN_REQUEST
});

export const logoutLocally = () => dispatch => {
  history.push("/login");
  dispatch(setLoggedIn(false));
  dispatch(receiveLogin({ access_token: "", refresh_token: "" }));
  dispatch(setUsername(""));
  dispatch(setPassword(""));
};

export const login = (username, password) => {
  return (dispatch, getState) => {
    dispatch(requestLogin());
    username = username || getState().login.username;
    password = password || getState().login.password;
    let ok = false;
    const publicAddress = getState().login.publicAddress;
    fetch(Config.API.logIn, {
      method: "POST",
      headers: { "Content-Type": "application/json", publicAddress },
      body: JSON.stringify({ username, password })
    })
      .then(response => {
        ok = response.ok;
        if (response.ok) {
          dispatch(setLoggedIn(true));
        } else {
          dispatch(setLoggedIn(false));
        }
        return response.json();
      })
      .then(json => {
        if (!ok) throw json;

        const token = parseJwt(json.access_token);
        window.gridSettingsId = token.identity;
        dispatch(receiveLogin(json));
        dispatch(fetchFormFilters());
        setAutoLogout(dispatch, getState);
        const defaultUrl = "/home";
        //~TODO remove~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // history.push("/Ispitivanja/v_predmet");
        // dispatch(setCurrentTable("v_predmet"));
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        dispatch(fetchDocuments());
        dispatch(fetchUsluge());
        dispatch(fetchStatusi());
        dispatch(
          fetchForms(forms => {
            dispatch(requestFinished());
            history.push(defaultUrl);
          })
        );
      })
      .catch(err => {
        dispatch(requestFinished());
        dispatch(
          openModal({
            title: "Login",
            iconType: "error",
            message: err.msg,
            buttons: [{ type: "ok", text: "Ok" }]
          })
        );
      });
  };
};

export const refresh = async (dispatch, getState, callback) => {
  const access_token = getState().login.access_token;
  const isRefreshing = getState().login.isRefreshing ? Boolean(1) : Boolean(0);
  if (!access_token || isRefreshing) return;
  const jti = parseJwt(access_token).jti;
  dispatch(setIsRefreshing(true));
  const publicAddress = getState().login.publicAddress;
  return fetch(Config.API.refresh, {
    method: "POST",
    headers: {
      Authorization: `Bearer ${getState().login.refresh_token}`,
      "Content-Type": "application/json",
      publicAddress
    },
    body: JSON.stringify({ jti })
  })
    .then(res => res.json())
    .then(json => {
      dispatch(setIsRefreshing(false));
      dispatch(receiveLogin(json));
      callback && callback();
    })
    .catch(err => dispatch(setIsRefreshing(false)));
};

export const logout = () => {
  return (dispatch, getState) => {
    window.gridSettingsId = "";
    clearInterval(autoLogout);
    if (getState().login.access_token) {
      const publicAddress = getState().login.publicAddress;
      fetch(Config.API.logOut, {
        method: "POST",
        headers: {
          Authorization: `Bearer ${getState().login.access_token}`,
          "Content-Type": "application/json",
          publicAddress
        }
      })
        .then(res => res.json())
        .then(json => console.log(json))
        .catch(err => console.log(err))
        .finally(() => {
          dispatch(logoutLocally());
        });
    }
  };
};

let autoLogout;

const setAutoLogout = (dispatch, getState) => {
  autoLogout = setInterval(() => {
    const { access_token } = getState().login;
    if (!access_token || isTokenExpired(access_token)) {
      dispatch(logout());
    }
  }, 10 * 60 * 60);
};
