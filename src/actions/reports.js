import React from "react";
import fetch from "../utils/fetch";
import { openModal } from "./modal";
import Config from "../config";
import {
  FINISH_REPORT_REQUEST,
  REQUEST_REPORT,
  SET_REPORTS,
  SET_SELECTED_AZS,
  SET_SELECTED_REPORT
} from "./types";
import { reportTables } from "../utils/reports";
import { copyToClipboard } from "../utils";

export const requestReport = () => ({
  type: REQUEST_REPORT
});

export const finishReportRequest = () => ({
  type: FINISH_REPORT_REQUEST
});

export const setSelectedReport = reportName => ({
  type: SET_SELECTED_REPORT,
  payload: reportName
});

export const setSelectedAzs = azsType => ({
  type: SET_SELECTED_AZS,
  payload: azsType
});

export const generateReport = (
  tableName,
  report,
  valuesArg = {},
  openInNewTab = false
) => {
  let newTab;
  if (openInNewTab) {
    newTab = window.open("", "_blank");
  }
  return (dispatch, getState) => {
    tableName = tableName || getState().grid.currentTable;
    const repDef = getState().reports.reports[tableName];
    const selectedRow = getState().grid.selectedRow[tableName];
    const type = report || getState().reports.selectedReport || repDef[0].value;
    const rprt = (repDef || []).find(rt => rt.value === type) || {};
    const reportName = (rprt || {}).label || "";
    let values = {};
    if (selectedRow && rprt.values && rprt.values.length) {
      rprt.values.forEach(val => {
        const value = selectedRow[val.stateKey];
        if (value) {
          values[val.fetchKey] = value;
        }
      });
    }

    if (reportTables.includes(tableName) || valuesArg) {
      values = { ...values, ...valuesArg };
      dispatch(requestReport());
      fetch(Config.API.reports, {
        method: "POST",
        body: JSON.stringify({ type, values })
      })
        .then(res => {
          return res.blob();
        })
        .then(body => {
          const pdfUrl = window.URL.createObjectURL(body);
          dispatch(
            openModal({
              title: "Izveštaj",
              template: (
                <div className="reportPreview">
                  <iframe title="pdf" src={pdfUrl} />
                </div>
              ),
              buttons: [
                {
                  type: "ok",
                  text: "Sačuvaj",
                  action: () => {
                    if (!openInNewTab) {
                      var elem = window.document.createElement("a");
                      elem.target = "_blank";
                      elem.href = pdfUrl;
                      elem.download = `${reportName}.pdf`;
                      document.body.appendChild(elem);
                      elem.click();
                      document.body.removeChild(elem);
                    } else {
                      newTab.location.href = pdfUrl;
                    }
                    URL.revokeObjectURL(pdfUrl);
                  }
                },
                { type: "cancel", text: "Odbaci" }
              ]
            })
          );
        })
        .finally(() => {
          dispatch(finishReportRequest());
        });
    }
  };
};

export const generateAzs = (type, prId) => {
  return (dispatch, getState) => {
    const activeTable = getState().grid.activeTable;
    const grid = getState().grid;
    prId =
      prId || grid.selectedRow[activeTable][grid.primaryKey[activeTable][0]];
    type = type || getState().reports.selectedAzs;
    dispatch(requestReport());
    fetch(Config.API.azs, {
      method: "POST",
      body: JSON.stringify({ type, pr_id: prId })
    })
      .then(res => res.json())
      .then(json => {
        copyToClipboard(json.query);
        alert(`tekst za ${type} kopiran`);
      })
      .finally(() => {
        dispatch(finishReportRequest());
      });
  };
};
export const setReports = reports => ({
  type: SET_REPORTS,
  payload: reports
});
