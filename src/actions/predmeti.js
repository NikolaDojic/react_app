import {
  ADD_SELECT_OPTION,
  CLEAR_SELECT_OPTIONS,
  RECEIVE_FORM_FILTER_DSC,
  REQUEST_OPTIONS,
  REQUEST_OPTIONS_FINISHED,
  REQUEST_PREDMET,
  REQUEST_PREDMET_FINISHED,
  SET_IS_CREATING_P_ITEM,
  SET_PREDMET_VISIBLE,
  SET_SERT_VISIBLE,
  SET_SELECT_OPTIONS
} from "./types";
import { fetchDataPromise, changeEditedItem } from "./grid";
import { openModal } from "./modal";
import {
  rowsToOptions,
  textParamFromData,
  prepareOptionsRequest,
  INTERACTIVE_COLUMNS
} from "../utils";
import Config from "../config";
import fetch from "../utils/fetch";

//payload={col_name: [options]}
export const setSelectOptions = payload => ({
  type: SET_SELECT_OPTIONS,
  payload
});

export const fetchOptions = (
  currentColumn,
  values,
  setOnly = false,
  tableName,
  callback,
  altFnc = false
) => {
  return (dispatch, getState) => {
    const body = prepareOptionsRequest(currentColumn, altFnc, getState, values);
    if (tableName) {
      fetchDataPromise(tableName, body.values).then(json => {
        handleOptionsResponse(
          json,
          [currentColumn],
          setOnly,
          dispatch,
          getState,
          callback
        );
      });
    } else {
      fetch(Config.API.predmeti, {
        method: "POST",
        body: JSON.stringify(body)
      })
        .then(res => res.json())
        .then(json => {
          handleOptionsResponse(
            json,
            [currentColumn],
            setOnly,
            dispatch,
            getState,
            callback
          );
        })
        .catch(err => {
          console.warn("failed to fetch options");
          console.error(err);
        });
    }
  };
};

export const handleOptionsResponse = (
  json,
  clearColumnList = [],
  setOnly,
  dispatch,
  getState,
  callback
) => {
  if (json.length) {
    const currentValues = getState().grid.editedItem;
    const options = rowsToOptions(json);
    const keys = Object.keys(options);
    let shouldFetch = false;
    let newValues = {};
    dispatch(setSelectOptions(options));
    callback && callback(json);
    if (json.length === 1) {
      newValues = json[0];
    } else {
      keys.forEach(key => {
        if (options[key].length === 1) {
          const value = options[key][0].label;
          newValues[key] = value;
          if (!shouldFetch && currentValues[key] !== value) {
            shouldFetch = true;
          }
        }
      });
    }
    if (shouldFetch) {
      dispatch(fetchAllOptions());
    }
    setOnly && dispatch(changeEditedItem(newValues));
  } else {
    clearColumnList.forEach(currentColumn =>
      dispatch(clearOptions(currentColumn))
    );
  }
};

export const fetchAllOptions = (values, setOnly = false, callback) => {
  return (dispatch, getState) => {
    const isFetching = getState().predmeti.isFetchingOptions;
    if (!isFetching) {
      dispatch(requestOptions());
      const column = "mto_oznaka";
      const data = getState().grid.editedItem;
      const text = textParamFromData(data);
      const body = prepareOptionsRequest(column, true, getState, values);
      body.values = body.values ? { ...body.values, text } : { text };
      fetch(Config.API.predmeti, {
        method: "POST",
        body: JSON.stringify(body)
      })
        .then(res => res.json())
        .then(json => {
          dispatch(requestOptionsFinished());
          handleOptionsResponse(
            json,
            INTERACTIVE_COLUMNS,
            setOnly,
            dispatch,
            getState,
            callback
          );
        })
        .catch(err => {
          console.log(err);
          dispatch(requestOptionsFinished());
        });
    }
  };
};

export const requestOptions = () => ({
  type: REQUEST_OPTIONS
});

export const requestOptionsFinished = () => ({
  type: REQUEST_OPTIONS_FINISHED
});

export const requestPredmet = () => ({
  type: REQUEST_PREDMET
});

export const requestPredmetFinished = () => ({
  type: REQUEST_PREDMET_FINISHED
});

export const clearOptions = colName => ({
  type: CLEAR_SELECT_OPTIONS,
  payload: colName
});

export const addOption = (option, column) => ({
  type: ADD_SELECT_OPTION,
  payload: { column, option }
});

export const fetchFormFilters = () => {
  return dispatch => {
    fetch(Config.API.predmeti)
      .then(res => res.json())
      .then(json => dispatch(receiveFormFilterDsc(json)));
  };
};

export const receiveFormFilterDsc = filterDescriptions => ({
  type: RECEIVE_FORM_FILTER_DSC,
  payload: filterDescriptions
});

//payload={[formName]: isCreating} ex klijent: false
export const setIsCreating = payload => ({
  type: SET_IS_CREATING_P_ITEM,
  payload
});

export const selectOption = (colName, optionValue) => {
  return (dispatch, getState) => {
    const value = (
      getState().predmeti.selectOptions[colName].find(
        option => option.value === optionValue
      ) || {}
    ).label;
    typeof value !== "undefined" &&
      dispatch(changeEditedItem({ [colName]: value }));
  };
};

export const saveItem = (columns, tableName, vals) => {
  return (dispatch, getState) => {
    dispatch(requestPredmet());
    let values = {};
    let ok;
    const editedItem = getState().grid.editedItem;
    columns.forEach(col => {
      values[col] = editedItem[col];
    });
    values = { ...values, ...vals };
    fetch(`${Config.baseUrl}${tableName}`, {
      method: "POST",
      body: JSON.stringify(values)
    })
      .then(res => {
        dispatch(requestPredmetFinished());
        ok = res.ok;
        return res.json();
      })
      .then(json => {
        if (ok) {
          console.log(json);
          dispatch(changeEditedItem(json));
          dispatch(setIsCreating({ [tableName]: false }));
        } else {
          throw json;
        }
      })
      .catch(err => {
        console.log(err);
        dispatch(
          openModal({
            message: "Došlo je do greške",
            buttons: [{ type: "ok", text: "Ok" }],
            iconType: "error"
          })
        );
        dispatch(requestPredmetFinished());
      });
  };
};

export const setDefaultValues = (columns, tableName = "v_predmet") => {
  return (dispatch, getState) => {
    let defaultValues = {};
    const colDefs = [...getState().grid.columns[tableName]];
    colDefs.forEach(col => {
      if (columns.includes(col.name)) {
        defaultValues[col.name] = col.defaultValue || "";
      }
    });
    dispatch(changeEditedItem(defaultValues));
  };
};

const UPLOAD_TYPES = ["application/pdf", "image/jpeg", "image/png"];
export const uploadFiles = (files, type, prId, callback) => {
  return (dispatch, getState) => {
    const grid = getState().grid;
    prId = prId || grid.selectedRow.v_predmet.pr_id;
    const formData = new FormData();
    formData.append("pr_id", prId);
    formData.append("type", type);

    Object.keys(files).forEach(fileKey => {
      const file = files[fileKey] || {};

      if (UPLOAD_TYPES.includes(file.type)) {
        formData.append("files", file, file.name);
      }
    });
    fetch(Config.API.upload, { method: "POST", body: formData }, false, true)
      .then(res => {
        return res.json();
      })
      .then(json => callback && callback(json));
  };
};

export const predmetVisible = isVisible => ({
  type: SET_PREDMET_VISIBLE,
  payload: isVisible
});

export const setSertVisible = isVisible => ({
  type: SET_SERT_VISIBLE,
  payload: isVisible
});
