import { SET_STATUSI } from "./types";
import { fetchDataPromise } from "./grid";

export const fetchStatusi = () => dispatch =>
  fetchDataPromise("predmet_status").then(statusi =>
    dispatch(setStatusi(statusi))
  );

export const setStatusi = statusi => ({
  type: SET_STATUSI,
  payload: statusi
});
