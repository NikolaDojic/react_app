import { RECEIVE_FORMS, REQUEST_FORMS } from "./types";
import Config from "../config.js";
import { normalizeForms } from "../utils/forms";
import { formDefsToReports } from "../utils/reports";
import { setReports } from "./reports";
import fetch from "../utils/fetch";

export const requestForms = () => ({ type: REQUEST_FORMS });

export const fetchForms = callback => {
  return dispatch => {
    dispatch(requestForms());
    fetch(Config.API.forms)
      .then(res => res.json())
      .then(json => {
        dispatch(receiveForms(json));
        console.log(formDefsToReports(json));
        dispatch(setReports(formDefsToReports(json)));
        callback && callback(json);
      });
  };
};

export const receiveForms = (forms, doNotNormalize = false) => ({
  type: RECEIVE_FORMS,
  payload: !doNotNormalize ? normalizeForms(forms) : forms
});
