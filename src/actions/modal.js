import { CLOSE_MODAL, OPEN_MODAL, CHANGE_BUTTONS_STATE } from "./types";

export const closeModal = () => ({ type: CLOSE_MODAL });

export const openModal = props => ({
  type: OPEN_MODAL,
  payload: props
});

export const disableButton = (buttonType, doDisable = true) => {
  return (dispatch, getState) => {
    const buttons = getState().modal.buttons.map(button =>
      button.type === buttonType
        ? { ...button, isDisabled: doDisable }
        : { ...button }
    );
    dispatch(changeButtonsState(buttons));
  };
};

const changeButtonsState = buttons => ({
  type: CHANGE_BUTTONS_STATE,
  payload: buttons
});
