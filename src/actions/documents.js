import { SET_DOCUMENTS } from "./types";
import { fetchDataPromise } from "./grid";

export const fetchDocuments = () => dispatch =>
  fetchDataPromise("vozilo_dokument").then(documents =>
    dispatch(setDocuments(documents))
  );

export const setDocuments = documents => ({
  type: SET_DOCUMENTS,
  payload: documents
});
