import { SET_USLUGE } from "./types";
import { fetchDataPromise } from "./grid";

export const fetchUsluge = () => dispatch =>
  fetchDataPromise("usluga").then(usluge => dispatch(setUsluge(usluge)));

export const setUsluge = usluge => ({
  type: SET_USLUGE,
  payload: usluge
});
