import {
  APPEND_DATA,
  CHANGE_EDITED_ITEM,
  CLEAR_FILTERS,
  DELETE_COLUMNS,
  DELETE_DATA,
  DELETE_ITEM,
  DELETE_PRIMARY_KEY,
  DELETE_SELECTED_ROW,
  FINISH_DATA_REQUEST,
  RECEIVE_COLUMNS,
  RECEIVE_DATA,
  RECEIVE_ITEM,
  REQUEST_GRID_DATA,
  SET_DETAILS,
  SET_EDITED_ITEM,
  SET_PRIMARY_KEY,
  SET_SELECTED_ROW,
  SET_SELECTED_ITEM,
  SET_FILTERS,
  SET_CURRENT_TABLE,
  SET_ACTIVE_TABLE
} from "./types";
import {
  getPrimaryKey,
  normalizeDescriptions,
  objToQuery,
  findForm,
  filterObjectByKeys
} from "../utils";
import { openModal } from "./modal";
import Config from "../config";
import fetch from "../utils/fetch";
import { restToEdit, sToP } from "../utils/predmet";
import { filtersToGrid } from "../utils/grid";
import { refresh } from "../actions/login";
import { shouldRefresh } from "../utils/jwt";

export const fetchColumns = (currentTable, callback) => {
  return dispatch => {
    dispatch(requestGridData());
    fetchColumnsPromise(currentTable)
      .then(json => {
        dispatch(receiveColumns(json, currentTable));
        callback && callback(json);
      })
      .catch(err => dispatch(finishDataRequest()));
  };
};

export const fetchColumnsPromise = currentTable =>
  fetch(Config.baseUrl + currentTable + "/description").then(res => res.json());

export const fetchData = (currentTable, filters) => {
  return (dispatch, getState) => {
    currentTable = currentTable || getState().grid.activeTable;
    filters = filters || (getState().grid.filters[currentTable] || {});
    if (!filters.limit) {
      filters.limit = 100;
    }
    const detail = (getState().grid.details || []).find(
      detail => detail.table === currentTable
    );
    if (detail) {
      detail.parentPrimaryKey.forEach(key => {
        const parentRow = getState().grid.selectedRow[detail.master];
        if (parentRow) {
          filters[key] = parentRow[key];
        }
      });
    }
    dispatch(requestGridData());
    fetchDataPromise(currentTable, filters)
      .then(json => {
        dispatch(receiveData({ [currentTable]: json }));
      })
      .catch(err => dispatch(finishDataRequest()));
  };
};

export const fetchDataPromise = (currentTable, filters = {}) =>
  fetch(Config.baseUrl + currentTable + objToQuery(filters)).then(res => {
    if (res && res.ok) return res.json();
    else throw res;
  });

export const receiveColumns = (columns, tableName) => {
  return dispatch => {
    dispatch({
      type: RECEIVE_COLUMNS,
      payload: { [tableName]: normalizeDescriptions(columns) }
    });
    dispatch(setPrimaryKey({ [tableName]: getPrimaryKey(columns, tableName) }));
  };
};

export const deleteColumns = tableName => ({
  type: DELETE_COLUMNS,
  payload: tableName
});

export const deleteData = tableName => ({
  type: DELETE_DATA,
  payload: tableName
});

export const deletePrimaryKey = tableName => ({
  type: DELETE_PRIMARY_KEY,
  payload: tableName
});

export const deleteSelectedRow = tableName => ({
  type: DELETE_SELECTED_ROW,
  payload: tableName
});

export const receiveData = data => ({
  type: RECEIVE_DATA,
  payload: data
});

export const receiveItem = (item, tableName) => ({
  type: RECEIVE_ITEM,
  payload: { item, tableName }
});

export const requestGridData = () => ({
  type: REQUEST_GRID_DATA
});

//TODO setEditedItem
export const setEditedItem = item => {
  item = item || {};
  return (dispatch, getState) => {
    const activeTable = getState().grid.activeTable;
    const detail = getState().grid.details.find(
      detail => detail.table === activeTable
    );
    if (detail) {
      const selectedMasterRow = getState().grid.selectedRow[detail.master];
      const masterKey = getState().grid.primaryKey[detail.master];
      const foreignKeys = filterObjectByKeys(masterKey, selectedMasterRow);
      item = { ...foreignKeys, ...item };
    }

    dispatch({
      type: SET_EDITED_ITEM,
      payload: item
    });
  };
};

export const selectedToEdited = activeTable => {
  return (dispatch, getState) => {
    activeTable = activeTable || getState().grid.activeTable;

    let item = getState().grid.selectedRow[activeTable];
    activeTable !== "v_predmet" && activeTable !== "v_vozilo_sert"
      ? dispatch(setEditedItem(item))
      : dispatch(
          setEditedItem(
            restToEdit(item, activeTable === "v_predmet" ? "vz_" : "vzs_")
          )
        );
  };
};

export const setSelectedRow = (row, tableName = "") => {
  return (dispatch, getState) => {
    tableName = tableName || getState().grid.activeTable;
    const details = getState().grid.details;
    dispatch({
      type: SET_SELECTED_ROW,
      payload: { [tableName]: row }
    });
    if (details.some(detail => detail.master === tableName)) {
      details.forEach(detail => {
        if (detail.master === tableName) {
          let filters = {};
          detail.parentPrimaryKey.forEach(key => {
            filters[key] = getState().grid.selectedRow[detail.master][key];
          });
          dispatch(setFilters(filters, detail.table));

          dispatch(fetchData(detail.table, filters));
        }
      });
    }
  };
};

export const createItem = (item, tableName, callback) => {
  return (dispatch, getState) => {
    item = item || getState().grid.editedItem;
    if (tableName === "v_predmet") {
      item.kr_id_p = window.gridSettingsId;
      item.kr_id = window.gridSettingsId;
    }
    if (tableName === "v_vozilo_sert") {
      item.kr_id = window.gridSettingsId;
    }
    const path = tableName || getState().grid.activeTable;
    dispatch(requestGridData());
    let status;
    Object.keys(item).forEach(key => {
      if (item[key] === "") {
        item[key] = null;
      }
    });
    fetch(`${Config.baseUrl}${path}`, {
      method: "POST",
      body: JSON.stringify(item)
    })
      .then(response => {
        status = response.status;
        return response.json();
      })
      .then(json => {
        if (status === 200) {
          dispatch(receiveItem(json, tableName));
          dispatch(setSelectedRow(json));
          dispatch(finishDataRequest());
          callback && callback(json, tableName);
        } else throw json;
      })
      .catch(err => {
        dispatch(
          openModal({
            title: "Srbolab",
            // message: "Došlo je do greške!",
            message: err.rmsg || "Došlo je do greške!",
            buttons: [{ iconType: "ok", text: "zatvori" }],
            template: null,
            iconType: "error"
          })
        );
        dispatch(finishDataRequest());
        console.log(err);
      });
  };
};

export const editItem = (item, tableName, callback) => {
  return (dispatch, getState) => {
    item = item || getState().grid.editedItem;
    if (tableName === "v_predmet") {
      item.kr_id_p = window.gridSettingsId;
      item.kr_id = window.gridSettingsId;
    }
    let status;
    const path = tableName || getState().grid.activeTable;
    dispatch(requestGridData());
    Object.keys(item).forEach(key => {
      if (item[key] === "") {
        item[key] = null;
      }
    });
    fetch(`${Config.baseUrl}${path}`, {
      method: "PUT",
      body: JSON.stringify(item)
    })
      .then(response => {
        status = response.status;
        return response.json();
      })
      .then(json => {
        if (status === 200) {
          dispatch(receiveItem(json, tableName));
          dispatch(setSelectedRow(json));
          dispatch(finishDataRequest());
          callback && callback(json, tableName);
        } else throw json;
      })
      .catch(err => {
        dispatch(
          openModal({
            title: "Srbolab",
            // message: "Došlo je do greške!",
            message: err.rmsg || "Došlo je do greške!",
            buttons: [{ iconType: "ok", text: "zatvori" }],
            iconType: "error"
          })
        );
        dispatch(finishDataRequest());
        console.log(err);
      });
  };
};

export const saveItem = (item, tableName, callback) => {
  return (dispatch, getState) => {
    item = item || getState().grid.editedItem;
    tableName = tableName || getState().grid.activeTable;
    const key = getState().grid.primaryKey[tableName];
    const data = getState().grid.data[tableName];
    const currentItem = (data || []).some(
      row => key.every(key => row[key] == item[key]) //eslint-disable-line
    );
    delete item.id;
    if (currentItem) {
      dispatch(editItem(item, tableName, callback));
    } else {
      dispatch(createItem(item, tableName, callback));
    }
  };
};

export const copyItem = callback => {
  return (dispatch, getState) => {
    const tableName = getState().grid.activeTable;
    const values = { ...getState().grid.editedItem };
    const selectedRow = { ...getState().grid.selectedRow[tableName] };
    const copyValues = {};
    const primaryKeys = getState().grid.primaryKey[tableName];
    let status;
    // Object.keys(copyValues).forEach(key => {
    primaryKeys.forEach(prKey => {
      copyValues[prKey] = selectedRow[prKey];
      copyValues[prKey + "_to"] = values[prKey + "_to"] || values[prKey];
    });
    // });

    debugger;
    dispatch(requestGridData());
    fetch(`${Config.baseUrl}${tableName}/copy`, {
      method: "POST",
      body: JSON.stringify(copyValues)
    })
      .then(response => {
        status = response.status;
        return response.json();
      })
      .then(json => {
        if (status === 200) {
          debugger;
          dispatch(receiveItem(json, tableName));
          dispatch(finishDataRequest());
          callback && callback(json, tableName);
        } else throw json;
      })
      .catch(err => {
        dispatch(
          openModal({
            title: "Srbolab",
            // message: "Došlo je do greške!",
            message: err.rmsg || "Došlo je do greške!",
            buttons: [{ iconType: "ok", text: "zatvori" }],
            iconType: "error"
          })
        );
        dispatch(finishDataRequest());
        console.log(err);
      });
  };
};

//payload = {tabeName, item}
export const deleteItemLocally = payload => ({
  type: DELETE_ITEM,
  payload
});

export const deleteItem = (item, activeTable) => {
  let isFetchFinished = false;
  const query = objToQuery(item);
  let status;
  return (dispatch, getState) => {
    dispatch(requestGridData());
    fetch(`${Config.baseUrl}${activeTable}${query}`, {
      method: "DELETE"
    })
      .then(res => {
        status = res.status;
        if (!isFetchFinished) {
          dispatch(finishDataRequest());
          isFetchFinished = true;
        }
        if (res.status === 200) {
          dispatch(deleteItemLocally({ item, tableName: activeTable }));
        }

        return res.json();
      })
      .then(json => {
        if (status !== 200) {
          throw json;
        } else console.log(json);
      })
      .catch(err => {
        dispatch(
          openModal({
            title: "Srbolab",
            message: err.rmsg || "Došlo je do greške!",
            buttons: [{ iconType: "ok", text: "zatvori" }],
            iconType: "error"
          })
        );
        !isFetchFinished && dispatch(finishDataRequest());
        console.log(err);
      });
  };
};

export const finishDataRequest = () => ({
  type: FINISH_DATA_REQUEST
});

export const changeEditedItem = item => {
  return (dispatch, getState) => {
    try {
      const access_token = getState().login.access_token;
      if (access_token && shouldRefresh(access_token)) {
        refresh(dispatch, getState);
      }
    } catch (e) {
      console.log(e);
    }
    dispatch({
      type: CHANGE_EDITED_ITEM,
      payload: item
    });
  };
};

//payload = {[tableName]: primaryKey}
export const setPrimaryKey = payload => ({
  type: SET_PRIMARY_KEY,
  payload
});

export const setSelectedItem = itemId => ({
  type: SET_SELECTED_ITEM,
  payload: itemId
});

export const deleteSelected = activeTable => {
  return (dispatch, getState) => {
    activeTable = activeTable || getState().grid.activeTable;
    const keys = getState().grid.primaryKey[activeTable];
    const item = filterObjectByKeys(
      keys,
      getState().grid.selectedRow[activeTable]
    );
    dispatch(deleteItem(item, activeTable));
    dispatch(deleteSelectedRow(activeTable));
  };
};

//payload = {filters, activeTable}
export const setFilters = (filters, activeTable) => {
  return (dispatch, getState) => {
    activeTable = activeTable || getState().grid.activeTable;
    const payload = { filters, activeTable };
    dispatch({
      type: SET_FILTERS,
      payload
    });
  };
};

export const clearFilters = activeTable => {
  return (dispatch, getState) => {
    activeTable = activeTable || getState().grid.activeTable;
    dispatch({
      type: CLEAR_FILTERS,
      payload: activeTable
    });
  };
};

export const setCurrentTable = (tableName, callback) => {
  return (dispatch, getState) => {
    const currentTable = getState().grid.currentTable;
    dispatch({
      type: SET_CURRENT_TABLE,
      payload: tableName
    });
    dispatch(setActiveTable(tableName));
    const form = findForm(tableName, getState().formGroups.formGroups);
    const reduceDetails = (acc, detail) => {
      let subDetails = [];
      if (detail.details.length) {
        subDetails = detail.details.reduce(reduceDetails, []);
      }
      return [
        ...acc,
        {
          table: detail.source,
          parentPrimaryKey: detail.parentpk,
          master: form.tables.source
        },
        ...subDetails
      ];
    };
    if (currentTable !== tableName) {
      const currentDetails = getState().grid.details;
      currentDetails.forEach(({ table }) => {
        dispatch(deleteData(table));
        dispatch(deleteColumns(table));
        dispatch(deletePrimaryKey(table));
        dispatch(deleteSelectedRow(table));
      });
      const details = form
        ? ((form.tables || {}).details || []).reduce(reduceDetails, [])
        : [];
      dispatch(setDetails(details));
      details.forEach(detail => {
        dispatch(fetchColumns(detail.table));
      });
    }
  };
};

export const setDetails = details => ({
  type: SET_DETAILS,
  payload: details
});

export const setActiveTable = activeTable => {
  window.activeTable = activeTable;
  return {
    type: SET_ACTIVE_TABLE,
    payload: activeTable
  };
};

export const formToFilter = (editedItem, activeTable) => {
  return (dispatch, getState) => {
    editedItem = editedItem || getState().grid.editedItem;
    let filters = getState().grid.filters[activeTable] || {};
    const columns = window.columnApi
      ? window.columnApi.getAllDisplayedColumns()
      : JSON.parse(localStorage.getItem(window.gridSettingsId) || "{}")[
          "v_predmet"
        ];

    columns.forEach(column => {
      if (
        !column.hide &&
        editedItem.hasOwnProperty(column.colId) &&
        (editedItem[column.colId] || editedItem[column.colId] === 0)
      ) {
        if (!(column.colId === "vz_sasija" || column.colId === "vz_motor")) {
          filters[column.colId] = editedItem[column.colId];
        }
      }
    });
    dispatch(setFilters(filters));
    const gridFilters = filtersToGrid(filters);
    if (window.gridApi["v_predmet"]) {
      window.gridApi["v_predmet"].setFilterModel(gridFilters);
      window.gridApi["v_predmet"].onFilterChanged();
    }
  };
};

export const appendData = (data, tableName) => ({
  type: APPEND_DATA,
  payload: { data, tableName }
});
let isScrollFetching;
export const scrollFetchData = () => {
  return (dispatch, getState) => {
    if (isScrollFetching) return dispatch({ type: "" });
    isScrollFetching = true;
    const currentTable = getState().grid.activeTable;
    let filters = getState().grid.filters[currentTable] || {};
    const currentCount = (getState().grid.data[currentTable] || []).length;
    filters = {
      ...filters,
      limit: 100,
      offset: currentCount
    };
    fetchDataPromise(currentTable, filters)
      .then(json => {
        dispatch(appendData(json, currentTable));
        isScrollFetching = false;
      })
      .catch(err => {
        console.log(err);
        isScrollFetching = false;
      });
  };
};

export const sertToPredmet = sert => {
  return (dispatch, getState) => {
    sert = sert || getState().grid.selectedRow["v_vozilo_sert"];
    const predmet = sToP(sert);
    dispatch(setEditedItem(restToEdit(predmet)));
  };
};
