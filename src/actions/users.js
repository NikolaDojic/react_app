import { REGISTER, REQUEST_USERS, RECEIVE_USERS } from "./types.js";
import Config from "../config";
import fetch from "../utils/fetch";

export const requestUsers = () => ({
  type: REQUEST_USERS
});

export const receiveUsers = users => ({
  type: RECEIVE_USERS,
  payload: users
});

export const fetchUsers = () => {
  return dispatch => {
    fetch(Config.API.users)
      .then(response => response.json())
      .then(json => {
        console.log(json);
        dispatch(receiveUsers(json.users));
      });
  };
};
