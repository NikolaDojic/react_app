import React from "react";

const MenuButton = ({ onClick }) => {
  return (
    <span className="MenuButton" onClick={e => onClick(e)}>
      &#9776;
    </span>
  );
};

export default MenuButton;
