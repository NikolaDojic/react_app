import React, { useEffect } from "react";
import "./Select.css";

const Select = ({
  onChange,
  items,
  getItemName = item =>
    item.hasOwnProperty("Name")
      ? item.Name
      : item.hasOwnProperty("name")
      ? item.name
      : item,
  getItemValue = item => item.Value || item.value || item,
  getItemId = item => item.Id || item.id || item,
  label,
  colon = true,
  defaultOption,
  hasDefaultOption = false,
  initialValue,
  setFirst,
  id,
  className = "",
  selectAttrs,
  isHidden = false,
  name,
  noLabel = false,
  isCustom = false
}) => {
  const handleChange = newValue => {
    onChange(newValue);
  };

  items = items || [];

  const sufix = colon ? ":" : "";
  initialValue =
    typeof initialValue === "number" ? String(initialValue) : initialValue;
  let selectElement = (
    <select
      name={name || "select"}
      onChange={e => handleChange(e.target.value)}
      value={initialValue}
      {...selectAttrs}
    >
      {hasDefaultOption && <option {...defaultOption} value={initialValue} />}
      {(() => {
        if (items.length) {
          return items.map((item, index) => {
            getItemValue = getItemValue ? getItemValue : getItemName;
            const id =
              getItemId && getItemId(item) !== undefined
                ? getItemId(item)
                : index;
            return (
              <option
                label={getItemName(item)}
                value={getItemValue(item)}
                key={id}
                id={id}
              >
                {getItemName(item)}
              </option>
            );
          });
        }
        return null;
      })()}
    </select>
  );

  useEffect(() => {
    if (!initialValue && items.length) {
      onChange(getItemValue(items[0]));
    }
  }, []); //eslint-disable-line

  return (
    !isHidden && (
      <label className={`Select ${className}`} id={id}>
        {!noLabel && label && (
          <span>
            {label}
            {sufix}
          </span>
        )}
        {selectElement}
      </label>
    )
  );
};

export default Select;
