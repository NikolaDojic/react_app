import React from "react";

const ListItem = ({
  onClick,
  isSelected,
  children,
  isDisabled,
  className,
  id
}) => (
  <li
    onClick={onClick}
    className={[
      "listItem",
      isSelected ? "selectedListItem" : null,
      isDisabled ? "disabledListItem" : null,
      className || ""
    ]
      .join(" ")
      .trim()}
    id={id}
  >
    {children}
  </li>
);

export default ListItem;
