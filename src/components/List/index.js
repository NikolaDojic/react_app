import React from "react";
import ListItem from "./ListItem";
import "./List.css";

const List = ({
  items,
  onItemClick,
  getItemText,
  getItemId,
  isItemSelected,
  isItemDisabled,
  itemClassName,
  className,
  addId = false,
  getItemKey,
  isHidden
}) =>
  !isHidden && (
    <ul className={`list ${className || ""}`.trim()}>
      {items.map((item, index) => (
        <ListItem
          key={getItemKey ? getItemKey(item, index) : getItemId(item, index)}
          id={addId ? getItemId(item, index) : null}
          isSelected={isItemSelected ? isItemSelected(item, index) : null}
          isDisabled={isItemDisabled ? isItemDisabled(item) : null}
          onClick={() =>
            onItemClick ? onItemClick(getItemId(item, index), index) : null
          }
          className={itemClassName ? itemClassName(item) : ""}
        >
          {getItemText(item)}
        </ListItem>
      ))}
    </ul>
  );

export default List;
