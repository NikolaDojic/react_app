import React from "react";
import "./RadioButtons.css";

//buttons = [{label, value, disabled}]
const RadioButtons = ({ buttons, onClick, name, currentValue, className }) => {
  return (
    <div className={`RadioButtons ${className}`}>
      {buttons.map((button, index) => {
        const text = button.label;

        return (
          <div key={index}>
            <label>
              <input
                type="radio"
                name={name}
                checked={currentValue === button.value}
                onChange={() => onClick(button.value)}
                disabled={button.disabled || false}
              />
              <span>{text}</span>
            </label>
          </div>
        );
      })}
    </div>
  );
};

export default RadioButtons;
