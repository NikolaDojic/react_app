import React from "react";
import "./ModalDialog.css";
import Button from "../Button";
import errorImg from "../../assets/icons/dialog_error.svg";
import warningImg from "../../assets/icons/dialog_warning.svg";
import infoImg from "../../assets/icons/dialog_info.svg";

const ModalDialog = ({
  title,
  message,
  buttons,
  iconType,
  template,
  className
}) => {
  const path = {
    error: errorImg,
    warning: warningImg,
    info: infoImg
  };
  return (
    <div className={`ModalDialog ${className || ""}`.trim()}>
      <h4>{title}</h4>
      <div className="modalBody">
        <div>
          {iconType && (
            <div className="modalImageContainer">
              <img src={path[iconType]} alt={iconType} />
            </div>
          )}
          {!template && <div hidden={template}>{message}</div>}
          {template}
        </div>
      </div>
      <div className="modalFooter">
        {buttons.map((button, index) => {
          return button.template ? (
            button.template
          ) : (
            <Button
              key={index}
              className={`defaultButton ${
                button.isDisabled ? "disabled" : ""
              } ${button.className || ""}`.trim()}
              onClick={button.action}
            >
              {button.text}
            </Button>
          );
        })}
      </div>
    </div>
  );
};

export default ModalDialog;
