import React from "react";

const Button = ({
  onClick,
  children,
  className,
  isHidden = false,
  id,
  isDisabled,
  buttonKey,
  buttonRef
}) => {
  return !isHidden ? (
    <button
      ref={buttonRef}
      type="button"
      className={`defaultButton ${className} ${
        isDisabled ? "disabled" : ""
      }`.trim()}
      onClick={onClick}
      id={id}
      key={buttonKey || ""}
    >
      {children}
    </button>
  ) : null;
};

export default Button;
