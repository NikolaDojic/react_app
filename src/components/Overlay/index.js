import React from "react";
import "./Overlay.css";

const Overlay = ({ isHidden, children, className }) => {
  return (
    <div hidden={isHidden} className={`Overlay ${className} waiting`.trim()}>
      <div>{children} </div>
    </div>
  );
};

export default Overlay;
