import React from "react";
import { connect } from "react-redux";
import Checkbox from "../Checkbox";
import { changeEditedItem } from "../../actions/grid";

const FormActions = ({ allowedActions, changeEditedItem }) => {
  const actions = [
    { label: "pregled", value: "v" },
    { label: "unos", value: "i" },
    { label: "izmena", value: "u" },
    { label: "kopiranje", value: "c" },
    { label: "brisanje", value: "d" }
  ];
  const toggleAction = action => {
    if (allowedActions.actions.some(a => a === action)) {
      allowedActions.actions = allowedActions.actions.filter(a => a !== action);
    } else allowedActions.actions = [...allowedActions.actions, action];
  };
  return (
    <div className="FormActions">
      {actions.map(action => (
        <Checkbox
          key={action.value}
          isSelected={allowedActions.actions.includes(action.value)}
          label={action.label}
          onClick={() => {
            toggleAction(action.value);
            changeEditedItem({ arf_akcije_d: JSON.stringify(allowedActions) });
          }}
        />
      ))}
    </div>
  );
};

const mapStateToProps = state => ({
  allowedActions: JSON.parse(
    state.grid.editedItem.arf_akcije_d || '{"actions": ["v"]}'
  )
});

export default connect(
  mapStateToProps,
  { changeEditedItem }
)(FormActions);
