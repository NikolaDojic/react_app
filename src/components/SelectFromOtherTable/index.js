import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import Select from "../Select";
import { fetchDataPromise, changeEditedItem } from "../../actions/grid";

const SelectFromOtherTable = ({
  onChange,
  currentColumns,
  selectAttrs,
  parentTable,
  initialValue,
  name,
  primaryKey,
  currentValue,
  filterItems,
  disabledItems
}) => {
  const [items, setItems] = useState([]);
  useEffect(() => {
    fetchDataPromise(parentTable).then(json => {
      debugger;
      setItems(json.filter(item => !disabledItems.includes(item[primaryKey])));
      selectAttrs.ref.current.value = currentValue;
      if (!currentValue) {
        let item = json[0];
        Object.keys(item).forEach(key => {
          if (!currentColumns.some(col => col.name === key)) {
            delete item[key];
          }
        });
        selectAttrs.ref.current.value = item[primaryKey];
        onChange(item);
      }
    });
  }, []); //eslint-disable-line
  return (
    <Select
      items={items}
      selectAttrs={selectAttrs}
      getItemName={item => item[name]}
      getItemValue={item => item[primaryKey]}
      getItemId={item => item[primaryKey]}
      onChange={value => {
        const item = items.find(item => item[primaryKey] == value) || {}; //eslint-disable-line
        Object.keys(item).forEach(key => {
          if (!currentColumns.some(col => col.name === key)) {
            delete item[key];
          }
        });
        onChange(item);
      }}
    />
  );
};

const mapStateToProps = (state, ownProps) => {
  const currentColumns = (state.grid.columns[window.activeTable] || []).filter(
    col =>
      !col.parenttable ||
      col.parenttable === ownProps.parentTable ||
      col.name === ownProps.name ||
      col.name === ownProps.name.split("_")[0] + "_id"
  );
  const primaryKey = (
    currentColumns.find(col => col.isprimarykey) || { name: ownProps.name }
  ).name;
  return {
    currentColumns: currentColumns,
    primaryKey: primaryKey,
    currentValue: state.grid.editedItem[primaryKey]
  };
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  onChange: ownProps.onChange
    ? item => ownProps.onChange(item)
    : item => dispatch(changeEditedItem(item))
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectFromOtherTable);
