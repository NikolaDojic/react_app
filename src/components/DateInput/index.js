import React, { useEffect } from "react";
import DatePicker from "react-datepicker";
import { localeStringToDate } from "../../utils";

const DateInput = ({ dateString, onChange }) => {
  useEffect(() => {
    const date =
      dateString && dateString !== "current_date"
        ? dateString
        : new Date().toLocaleDateString("de-DE", {
            year: "numeric",
            month: "2-digit",
            day: "2-digit"
          });
    onChange(date);
  }, []); //eslint-disable-line

  return (
    <div className="dateInputWrapper">
      <DatePicker
        selected={
          dateString && dateString !== "current_date"
            ? localeStringToDate(dateString)
            : new Date()
        }
        onChange={(e, a) => {
          const localString = e.toLocaleDateString("de-DE", {
            year: "numeric",
            month: "2-digit",
            day: "2-digit"
          });
          debugger;
          onChange(localString);
        }}
        dateFormat="dd.MM.yyyy"
      />
    </div>
  );
};

export default DateInput;
