import React from "react";
import "./Checkbox.css";

const Checkbox = ({ label, className, onClick, isSelected }) => {
  return (
    <span className={`Checkbox ${className || ""}`.trim()}>
      <label>
        <input
          type="checkbox"
          onClick={e => {
            e.stopPropagation();
            onClick(e);
          }}
          checked={isSelected}
          onChange={e => {}}
        />
        <div className="customCheckbox">
          <span>
            <span className="box" />
            <span className="checked">
              {isSelected ? <React.Fragment>&#10003;</React.Fragment> : null}
            </span>
          </span>
        </div>
        <span className="checkboxLabel">{label}</span>
      </label>
    </span>
  );
};

export default Checkbox;
