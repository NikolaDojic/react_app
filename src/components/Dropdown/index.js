import React, { Component } from "react";
import "./Dropdown.css";

class Dropdown extends Component {
  constructor(props) {
    super(props);
    this.state = { isHidden: true };
  }

  onClick = e => {
    e.stopPropagation();
    if (this.state.isHidden) {
      this.setState({ isHidden: false });
    } else {
      this.setState({ isHidden: true });
    }
  };

  handleDocumentClick = () => {
    if (this.state.isHidden === false) {
      this.setState({ isHidden: true });
    }
  };

  renderButton = () => {
    return React.cloneElement(this.props.button, {
      onClick: this.onClick
    });
  };

  renderContent = () => {
    var mobileMenuListClass = this.props.children.props.className;

    if (this.state.isHidden) mobileMenuListClass += " hideMenu";
    return React.cloneElement(this.props.children, {
      ...this.props.list,
      className: mobileMenuListClass
    });
  };

  componentDidMount() {
    window.addEventListener("click", this.handleDocumentClick);
  }

  componentWillUnmount() {
    window.removeEventListener("click", this.handleDocumentClick);
  }

  render() {
    let className = this.props.className || "";
    return (
      <div className={"dropdown " + className}>
        {this.renderButton()}
        {this.renderContent()}
      </div>
    );
  }
}

export default Dropdown;
