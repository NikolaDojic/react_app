import React, { useEffect, useRef } from "react";
import Select from "../Select";
import DateInput from "../DateInput";
import SelectFromOtherTable from "../SelectFromOtherTable";
import FormActions from "../FormActions";

const Input = ({
  name,
  label,
  type,
  length,
  isnotnull,
  defaultValue,
  checkconst,
  decimal,
  onChange,
  onChangeForeign,
  currentValue,
  error,
  parenttable,
  disabledItems,
  pickItem
}) => {
  const inputEl = useRef(null);
  useEffect(() => {
    if (inputEl && inputEl.current) {
      inputEl.current.value =
        typeof currentValue === "undefined" ? defaultValue : currentValue;
    }
  }, [inputEl]); //eslint-disable-line

  const input = () => {
    if (name.includes("datum")) {
      return (
        <DateInput
          dateString={currentValue || defaultValue}
          onChange={onChange}
        />
      );
    } else if (name === "arf_akcije_d") {
      return <FormActions />;
    } else if (
      (parenttable && !window.activeTable.includes(parenttable)) ||
      pickItem
    ) {
      return (
        <SelectFromOtherTable
          name={name}
          onChange={onChangeForeign}
          parentTable={parenttable}
          selectAttrs={{ ref: inputEl }}
          disabledItems={disabledItems || []}
        />
      );
    } else if (!checkconst) {
      return (
        <input
          ref={inputEl}
          type={type === "c" ? "text" : "number"}
          onChange={e => {
            onChange(e.target.value);
          }}
        />
      );
    } else {
      return (
        <Select
          noLabel={true}
          getItemId={item => item.value}
          items={checkconst}
          onChange={onChange}
          selectAttrs={{ ref: inputEl }}
        />
      );
    }
  };

  return (
    <label htmlFor="" key={name}>
      <span>{label}:</span> {input()}
    </label>
  );
};

export default Input;
