import React from "react";
import Input from "../Input";
import "./Form.css";

const Form = ({ columns, onChange }) => {
  return (
    <div className="Form">
      {columns.map(col => (
        <Input key={col.name} {...col} />
      ))}
    </div>
  );
};

export default Form;
