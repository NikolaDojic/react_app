import React, { useState, useEffect, useRef } from "react";
import "./TextBox.css";

const TextBox = ({
  label,
  onChange,
  initialValue,
  className,
  inputAttrs = {},
  updateFromState = true,
  error,
  warning,
  implicitChange
}) => {
  const inputEl = useRef(null);
  const [isInitalized, setIsInitialized] = useState(false);
  const warningEl = useRef(null);
  useEffect(() => {
    if (
      inputEl &&
      inputEl.current &&
      (!isInitalized || updateFromState) && //TODO test
      typeof initialValue !== "undefined"
    ) {
      inputEl.current.value = initialValue;
      setIsInitialized(true);
    }
  }, [inputEl]); //eslint-disable-line

  useEffect(() => {
    if (
      implicitChange &&
      inputEl &&
      inputEl.current &&
      inputEl.current.value !== initialValue
    ) {
      inputEl.current.value = initialValue;
    }
  }, [initialValue]); //eslint-disable-line
  return (
    <div className="TextBox">
      {label ? (
        <span className={`inputLabel ${className}`}> {label} :</span>
      ) : null}
      <div
        className={`inputWrapper${
          error ? " inputError" : warning ? " inputWarning" : ""
        }`}
      >
        <input
          ref={inputEl}
          className={error ? "inputError" : warning ? "inputWarning" : ""}
          onChange={e => {
            let value = e.target.value;
            if (inputAttrs.type === "number") {
              if (inputAttrs.hasOwnProperty("max")) {
                value = value > Number(inputAttrs.max) ? inputAttrs.max : value;
                e.target.value = value;
              } else if (inputAttrs.hasOwnProperty("min")) {
                value = value < Number(inputAttrs.min) ? inputAttrs.min : value;
                e.target.value = value;
              }
            }
            onChange(e.target.value);
          }}
          type={inputAttrs.type || "text"}
          {...inputAttrs}
          onFocus={() => {
            if (warningEl && warningEl.current) {
              warningEl.current.classList.remove("closed");
            }
          }}
        />
        {error ? (
          <span className="tooltip inputErrorTooltip">{error} </span>
        ) : null}
        {warning ? (
          <span ref={warningEl} className="tooltip inputWarningTooltip">
            {warning}
            <span
              onClick={() => {
                warningEl.current.classList.add("closed");
              }}
              className="closeWarning"
            >
              x
            </span>{" "}
          </span>
        ) : null}
      </div>
    </div>
  );
};

export default TextBox;
