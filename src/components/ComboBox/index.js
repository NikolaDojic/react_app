import React, { useEffect, useState, useRef } from "react";
import "./ComboBox.css";

const ComboBox = ({
  label,
  initialValue,
  options,
  getOptionName = option => (option || {}).name || option,
  getOptionId = option => (option || {}).id || option,
  onChange,
  onSelectOption
}) => {
  const [areOptionVisible, setOptionsVisible] = useState(false);
  const [isInitalized, setIsInitialized] = useState(false);
  const inputEl = useRef(null);

  useEffect(() => {
    if (!isInitalized && inputEl.current && !inputEl.current.value) {
      inputEl.current.value = initialValue;
      setIsInitialized(true);
    }
  }, [inputEl.current]); //eslint-disable-line

  useEffect(() => {
    if (typeof inputEl.current !== "undefined") {
      console.log(inputEl.current.value);
      const currentOption = options.find(
        option => getOptionName(option) == inputEl.current.value //eslint-disable-line
      );
      console.log(currentOption);
      if (currentOption) {
        onSelectOption(currentOption);
      }
    }
  }, [(inputEl.current || {}).value]); //eslint-disable-line

  return (
    <div className="ComboBox">
      <label htmlFor="">
        {label ? <span className="comboboxLabel">{label} :</span> : null}
        <div className="inputWrapper">
          <input
            ref={inputEl}
            onFocus={() => setOptionsVisible(true)}
            onBlur={() => setOptionsVisible(false)}
            type="text"
            onChange={e => {
              onChange(e.target.value);
            }}
          />
        </div>
        {areOptionVisible ? (
          <div className="options">
            {options.map(option => (
              <div
                onClick={() => {
                  onSelectOption(option);
                  inputEl.current.value = getOptionName(option);
                }}
                key={getOptionId(option)}
                className="option"
              >
                {getOptionName(option)}
              </div>
            ))}
          </div>
        ) : null}
      </label>
    </div>
  );
};
export default ComboBox;
