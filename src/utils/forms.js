export const normalizeForms = formGroups =>
  formGroups.map(formGroup => ({
    ...formGroup,
    forms: formGroup.forms.map(form => {
      if (
        form.hasOwnProperty("useractions") &&
        form.useractions.hasOwnProperty("actions")
      ) {
        form.actions = [...form.useractions.actions];
        delete form.useractions;
      }
      return form;
    })
  }));
