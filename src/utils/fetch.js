import { logout, refresh } from "../actions/login";
import store from "../store";
import history from "../history";
import { shouldRefresh, isTokenExpired } from "./jwt";

const { dispatch, getState } = store;

const fetchAuth = async (
  url = "",
  options = {},
  disableAuth = false,
  disableHeaders = false
) => {
  const { access_token } = store.getState().login;
  if (access_token && isTokenExpired(access_token)) {
    dispatch(logout());
  } else if (shouldRefresh(access_token)) {
    await refresh(dispatch, getState);
  }
  let headers = disableHeaders
    ? {
        Authorization: `Bearer ${store.getState().login.access_token}`,
        publicAddress: store.getState().login.publicAddress
      }
    : {
        "Content-Type": "application/json",
        Authorization: `Bearer ${store.getState().login.access_token}`,
        publicAddress: store.getState().login.publicAddress
      };
  if (disableAuth) {
    delete headers.Authorization;
  }
  if (options.headers) {
    headers = { ...headers, ...options.headers };
  }
  options.headers = headers;
  return fetch(url, options)
    .then(res => {
      if (res.status === 401 || res.status === 422) {
        store.dispatch(logout());
        history.push("/login");
        throw res;
      }
      return new Promise(resolve => {
        resolve(res);
      });
    })
    .catch(err => console.log(err));
};

export default fetchAuth;
