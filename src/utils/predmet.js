export const restToEdit = (predmet, prefix) => {
  let newPredmet = transformOsovine({ ...predmet }, prefix);
  return newPredmet;
};

export const transformOsovine = (predmet, prefix = "vz_") => {
  const keyNosivost = `${prefix}os_nosivost`;
  const keyPneumatici = `${prefix}os_pneumatici`;
  const keyTockovi = `${prefix}os_tockovi`;
  const keyBroj = `${prefix}os_broj`;
  const keyOsovine = `${prefix}osovine`;
  const DELIMITER = " / ";
  const nosivost = (predmet[keyNosivost] || "").split(DELIMITER);
  const pneumatici = (predmet[keyPneumatici] || "").split(DELIMITER);
  let tockovi = (predmet[keyTockovi] || "2 / 2").split(DELIMITER);

  const osCount = Array(predmet[keyBroj] || 2).fill();
  predmet[keyOsovine] = osCount.map((a, index) => {
    const sufix = prefix === "vz_" ? "" : "s";
    const osovina = {
      [`vzo${sufix}_nosivost`]: Number(nosivost[index]) || 0,
      [`vzo${sufix}_tockova`]: Number(tockovi[index]),
      [`vzo${sufix}_pneumatik`]: pneumatici[index] || pneumatici[0] || ""
    };
    return osovina;
  });
  console.log(predmet[keyOsovine]);
  return predmet;
};

export const defaultStatus = predmet => {
  if (!predmet.prs_oznaka) {
    predmet = {
      ...predmet,
      prs_oznaka: "PRI",
      prs_naziv: "Priprema",
      prs_id: "10",
      prs_priprema: "D",
      prs_cekanje: "N",
      prs_zakljucen: "N",
      prs_neusag: "N"
    };
  }
  return predmet;
};

export const sToP = sert => {
  var predmet = {};
  Object.keys(sert).forEach(key => {
    key = key.startsWith("vzos_") ? key.replace("vzos_", "vzo_") : key;
    const pKey =
      key.startsWith("vzs_") && key !== "vzs_id" && key !== "vzs_oznaka"
        ? key.replace("vzs_", "vz_")
        : key;

    predmet[pKey] = sert[key];
  });
  return predmet;
};
