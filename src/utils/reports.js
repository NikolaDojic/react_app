export const formDefsToReports = formDefs => {
  let reports = {};
  formDefs.forEach(forms => {
    forms.forms.forEach(formDef => {
      if ((formDef.reports || []).length) {
        reports[formDef.tables.source] = formDef.reports.map(rep => ({
          value: rep.report,
          label: rep.title,
          values: Object.keys(rep.parameters).map(key => ({
            stateKey: rep.parameters[key],
            fetchKey: key
          }))
        }));
      }
    });
  });
  return reports;
};

export const azsDefs = {
  v_predmet: {
    types: [
      {
        value: "uvoz",
        label: "Uvoz polovnog vozila"
      },
      {
        value: "ispitivanje",
        label: "Ispitivanje"
      }
    ],
    stateKey: "pr_id"
  }
};

export const azsTables = ["v_predmet"];

export const reportTables = [
  "v_predmet",
  "lokacija",
  // "ag_proizvodjac",
  "v_kalendar"
];
