export const normalizeDescription = dsc => {
  dsc.defaultValue = dsc.default;
  delete dsc.default;
  if (dsc.checkconst) {
    dsc.checkconst = dsc.checkconst.map(cstr => ({
      value: cstr,
      name: cstr === "D" ? "Da" : cstr === "N" ? "Ne" : cstr
    }));
  }
  return dsc;
};

export const normalizeDescriptions = dscs =>
  dscs.map(normalizeDescription).sort((a, b) => a.order - b.order);
