export const capitalize = string => {
  if (!string) return string;
  return string.slice(0, 1).toUpperCase() + string.slice(1);
};

export const getCurrent = (currentId, listOfItems, propertyName) => {
  let key = propertyName;
  if (!listOfItems || !Array.isArray(listOfItems)) {
    console.warn(
      "Parameter 'listOfItems' is not an array! Returning empty array."
    );
    return [];
  }
  return listOfItems.reduce((acc, item) => {
    if (!propertyName) {
      key = item.hasOwnProperty("id") ? "id" : "Id";
    }
    return item[key] === currentId ? item : acc;
  }, null);
};
export const normalizeDescription = dsc => {
  dsc.defaultValue = dsc.default;
  delete dsc.default;
  if (dsc.checkconst) {
    dsc.checkconst = dsc.checkconst.map(cstr => ({
      value: cstr,
      name: cstr === "D" ? "Da" : cstr === "N" ? "Ne" : cstr
    }));
  }
  return dsc;
};

export const normalizeDescriptions = dscs =>
  dscs.map(normalizeDescription).sort((a, b) => a.order - b.order);

export const getPrimaryKey = (columns, tableName) => {
  tableName = tableName || window.activeTable;
  return columns
    .filter(
      col =>
        col.isprimarykey &&
        (col.isforeignkey ||
          (!col.parenttable || tableName.slice(2) === col.parenttable))
    )
    .map(col => col.name);
};

export const DATE_COLUMNS = ["pr_datum", "kn_datum"];
const DEFAULT_VISIBLE_COLUMNS = [
  "vz_mesta_sedenje",
  "vz_sasija",
  "vz_godina",
  "vz_elektro",
  "md_naziv_k",
  "vz_masa",
  "vz_masa_max",
  "vzpv_naziv",
  "mr_naziv",
  "gr_naziv",
  "vz_nosivost",
  "vz_os_nosivost",
  "vzpv_oznaka",
  "mto_oznaka",
  "mdt_oznaka",
  "mdvr_oznaka",
  "mdvz_oznaka",
  "vzk_oznaka",
  "mt_kw",
  "mt_cm3"
];
export const gridColumns = (columns, tableName) =>
  columns
    .filter(col => col.show)
    .map(col => {
      let baseCol = {
        field: col.name,
        colId: col.name,
        headerName: col.header,
        resizable: true,
        sortable: false,
        floatingFilterComponent: "floatingFilter", //see Grid/index.js => frameworkComponents
        floatingFilterComponentParams: {
          col: col.name,
          type: col.type,
          tableName
        },
        initialHide:
          window.tableName === "v_predmet" &&
          !DEFAULT_VISIBLE_COLUMNS.includes(col.name)
      };
      if (DATE_COLUMNS.includes(col.name)) {
        baseCol = {
          ...baseCol,
          floatingFilterComponent: "dateFilter" //see Grid/index.js => frameworkComponents
        };
      }
      if (col.name === "arf_akcije_d") {
        const valueFormatter = params => {
          const value = JSON.parse(params.value);
          return value.actions.map(
            action =>
              FORM_ACTIONS.find(formAction => formAction.key === action)
                .label || action
          );
        };
        baseCol = { ...baseCol, valueFormatter };
      } else if (col.checkconst && col.type === "c") {
        const valueFormatter = params => {
          const constaraint = col.checkconst.find(
            cst => cst.value === params.value
          );
          return (constaraint || {}).name || params.value;
        };
        baseCol = { ...baseCol, valueFormatter };
      }

      return baseCol;
    });

const FORM_ACTIONS = [
  {
    label: "Pregled",
    method: "tbl_get",
    enabled: true,
    key: "v"
  },
  {
    label: "Dodavanje",
    method: "tbl_insert",
    enabled: true,
    key: "i"
  },
  {
    label: "Izmena",
    method: "tbl_update",
    enabled: true,
    key: "u"
  },
  {
    label: "Brisanje",
    method: "tbl_delete",
    enabled: true,
    key: "d"
  },
  {
    label: "Brisanje",
    method: "tbl_delete",
    enabled: true,
    key: "d"
  },
  {
    label: "Kopiranje",
    method: "tbl_copy",
    enabled: true,
    key: "c"
  }
];

export const gridData = (data, primaryKey) => {
  if (!primaryKey) return [];
  var seen = {};
  const gridData = data
    .map(item => {
      const id = JSON.stringify(filterObjectByKeys(primaryKey, item));

      return seen.hasOwnProperty(id)
        ? null
        : {
            ...item,

            id: id
          };
    })
    .filter(Boolean);
  return gridData;
};

export const fuzzySearch = (values, filterValue, ignoreCase = true) => {
  if (!filterValue) return true;
  if (typeof values === "string") values = [values];
  if (ignoreCase) {
    values = values.map(value => value.toLowerCase());
    filterValue = filterValue.toLowerCase();
  }
  return values.some(value => {
    let remainingValue = value;
    const doesMatchFilter = [...filterValue].every(character => {
      const index = remainingValue.indexOf(character);
      if (index >= 0) {
        remainingValue = remainingValue.slice(index + 1);
        return true;
      }
      return false;
    });
    return doesMatchFilter;
  });
};

export const generateUrl = page => {
  return `/${page.title}/${page.forms[0].tables.source}`;
};

export const objToQuery = obj => {
  const keys = Object.keys(obj);
  if (!keys.length) return "";
  return (
    "?" + keys.map(key => (obj[key] ? `${key}=${obj[key]}` : "")).join("&")
  );
};

export const findForm = (table, formGroups) => {
  let currentForm;
  formGroups.some(formGroup => {
    return formGroup.forms.some(form => {
      if (form.tables.source === table) {
        currentForm = form;
        return true;
      } else if (form.tables.details && form.tables.details.length) {
        currentForm = findDetail(table, form.tables.details);
        return currentForm;
      } else return false;
    });
  });
  return currentForm;
};

export const findDetail = (table, details) => {
  let detail = null;
  details.some(form => {
    if (form.source === table) {
      detail = form;
      return true;
    } else if (form.details.length) {
      detail = findDetail(table, form.details);
      return detail;
    } else return false;
  });
  return detail;
};

export const rowsToOptions = rows => {
  let options = {};
  rows.forEach(row => {
    const columns = Object.keys(row);
    columns.forEach(key => {
      const prefix = key.split("_")[0];
      let id = columns.find(col => col === prefix + "_id") || key;
      if (options[key]) {
        if (!options[key].some(option => option.value === row[id])) {
          options[key] = [...options[key], { label: row[key], value: row[id] }];
        }
      } else {
        options[key] = [{ label: row[key], value: row[id] }];
      }
    });
  });
  return options;
};

export const getLabel = (columnName, columns) =>
  (columns.find(col => col.name === columnName) || {}).label;

// export const getTitleFromSource = (source, formGroups) => {
// let title =

// };

export const customSelectStyles = {
  control: (provided, state) => ({
    ...provided,
    background: "#fff",
    borderColor: "#9e9e9e",
    minHeight: "30px",
    height: "30px",
    boxShadow: state.isFocused ? null : null
  }),

  valueContainer: (provided, state) => ({
    ...provided,
    height: "30px",
    padding: "0 6px"
  }),

  input: (provided, state) => ({
    ...provided,
    margin: "0px"
  }),
  indicatorSeparator: state => ({
    display: "none"
  }),
  indicatorsContainer: (provided, state) => ({
    ...provided,
    height: "30px"
  })
};

export const tockoviFromVzpv = podvrsta => {
  const tockovi = {
    2: ["L1", "L3", "L4"],
    3: ["L2", "L5"],
    4: ["L7", "L6", "M1"]
  };
  return Object.keys(tockovi).find(key => tockovi[key].includes(podvrsta)) || 4;
};

export const textParamFromData = data => {
  const text = [
    data.mr_naziv,
    data.md_naziv_k,
    data.mdt_oznaka,
    data.mdvr_oznaka,
    data.mdvz_oznaka,
    data.mto_oznaka
  ]
    .map(value => value || "")
    .join(",");
  return text;
};

export const prepareOptionsRequest = (
  currentColumn,
  altFnc,
  getState,
  argValues = {}
) => {
  if (!argValues) argValues = {};
  let values = {};
  let columns;
  const colKey = altFnc ? "cols1" : "cols";
  try {
    const filterDsc = getState().predmeti.filterDescriptions;
    columns = [...filterDsc[currentColumn][colKey]];
    // columns.includes(currentColumn) || columns.push(currentColumn);
    columns.forEach(
      col =>
        (values[col] =
          col === "text"
            ? [currentColumn]
                .map(column => getState().grid.editedItem[column] || "")
                .join(",")
            : getState().grid.editedItem[col])
    );
  } catch (e) {
    console.log(e);
    console.warn(`cannot find filter description for column ${currentColumn}`);
    values = { [currentColumn]: getState().grid.editedItem[currentColumn] };
  }
  Object.keys(values).forEach(key => {
    if (typeof values[key] === "undefined" || !values[key]) {
      delete values[key];
    }
  });
  values = { ...values, ...argValues };
  const body = { values, activeColumn: currentColumn, altFnc };
  return body;
};

export const INTERACTIVE_COLUMNS = [
  "mr_id",
  "mr_naziv",
  "md_id",
  "md_naziv_k",
  "mdt_id",
  "mdt_oznaka",
  "mdvr_id",
  "mdvr_oznaka",
  "mdvz_id",
  "mdvz_oznaka"
];

export const filterObjectByKeys = (keys = [], obj = {}, defaultValue) => {
  let filteredObject = {};
  keys.forEach(key => (filteredObject[key] = obj[key] || defaultValue));
  return filteredObject;
};
export const latToCyr = word => {
  var result = "";
  for (var i = 0; i < word.length; i++) {
    var c = word.charAt(i);
    result += latToCyrObj[c] || c;
  }

  return result;
};

//prettier-ignore
const cyrAlphabet = [ "А", "Б", "В", "Г", "Д", "Ђ", "Е", "Ж", "З", "И", "Ј", "К",
  "Л", "Љ", "М", "Н", "Њ", "О", "П", "Р", "С", "Т", "Ћ", "У", "Ф", "Х", "Ц", "Ч",
  "Џ", "Ш", "а", "б", "в", "г", "д", "ђ", "е", "ж", "з", "и", "ј", "к", "л", "љ",
  "м", "н", "њ", "о", "п", "р", "с", "т", "ћ", "у", "ф", "х", "ц", "ч", "џ", "ш"
];

//prettier-ignore
const latinAlphabet = [ "A", "B", "V", "G", "D", "Đ", "E", "Ž", "Z", "I", "J", "K",
  "L", "Lj", "M", "N", "Nj", "O", "P", "R", "S", "T", "Ć", "U", "F", "H", "C", "Č",
  "Dž", "Š", "a", "b", "v", "g", "d", "đ", "e", "ž", "z", "i", "j", "k", "l", "lj",
  "m", "n", "nj", "o", "p", "r", "s", "t", "ć", "u", "f", "h", "c", "č", "dž", "š"
];

export const latToCyrObj = latinAlphabet.reduce(
  (acc, key, index) => ({ ...acc, [key]: cyrAlphabet[index] }),
  {}
);

//dateString = dd.MM.yyyy
export const localeStringToDate = dateString => {
  console.log(dateString);
  if (!dateString) return null;
  const [day, month, year] = dateString.split(".");
  return new Date(year, month - 1, day);
};

export const isActionAllowed = (action, tableName, forms) => {
  if (!forms.length) return true;
  const form = findForm(tableName, forms);
  if (!form) return false;
  const actions =
    (form.hasOwnProperty("tables")
      ? (form || {}).tables.actions
      : (form || {}).actions) || [];
  const isAllowed = actions.some(act => act.key === action && act.enabled);
  return isAllowed;
};

export const uniq = (items, key) => {
  var seen = {};
  if (!key) {
    return items.filter(item => {
      return seen.hasOwnProperty(item) ? false : (seen[item] = true);
    });
  } else
    return items.filter(item => {
      console.log(item, item[key]);
      return seen.hasOwnProperty(item[key]) ? false : (seen[item[key]] = true);
    });
};

export const copyToClipboard = text => {
  let textarea = document.createElement("textarea");
  textarea.textContent = text;
  textarea.style.position = "fixed";
  document.body.appendChild(textarea);
  textarea.select();
  try {
    return document.execCommand("copy");
  } catch (ex) {
    console.warn("Copy to clipboard failed.", ex);
    return false;
  } finally {
    document.body.removeChild(textarea);
  }
};
