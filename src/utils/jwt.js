export const parseJwt = token => {
  var base64Url = token.split(".")[1];
  var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
  var jsonPayload = decodeURIComponent(
    atob(base64)
      .split("")
      .map(function(c) {
        return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
      })
      .join("")
  );

  return JSON.parse(jsonPayload);
};
const LESS_THAN_REFRESH_MIN = 10;

export const shouldRefresh = accessToken => {
  try {
    const jsonToken = parseJwt(accessToken);
    return (
      new Date(jsonToken.exp * 1000) - new Date() <
      LESS_THAN_REFRESH_MIN * 60 * 1000
    );
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const isTokenExpired = token => {
  if (!token) return true;
  const jsonToken = parseJwt(token);
  const expDate = new Date(jsonToken.exp * 1000);
  return expDate <= new Date();
};
