export const filtersToGrid = filters => {
  let gridFilters = {};
  Object.keys(filters).forEach(key => {
    const value = filters[key];
    gridFilters[key] = {
      filter: String(value),
      filterType: "text",
      type: "contains"
    };
  });
  return gridFilters;
};

export const getCopyColumns = (
  columns,
  primaryKeys = [],
  activeTable,
  selectedRow = {}
) => {
  if (activeTable === "adm_rola") {
    return columns
      .filter(column => column.name === "arl_naziv")
      .map(column => ({
        ...column,
        pickItem: true,
        parenttable: "adm_rola",
        disabledItems: selectedRow["adm_rola"]
          ? [selectedRow["adm_rola"][primaryKeys[0]]]
          : [],
        isCopy: true
      }));
  }
  if (!columns || !columns.length || !primaryKeys || !primaryKeys.length)
    return [];
  const primaryColumns = columns.filter(col => primaryKeys.includes(col.name));
  const newColumns = [
    ...primaryColumns,
    ...primaryColumns.map(col => ({
      ...col,
      name: col.name + "_to",
      label: "Novi " + col.label,
      isnotnull: true
    }))
  ];
  return newColumns;
};
