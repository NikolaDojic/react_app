const port = 5000;
const base =
  process.env.NODE_ENV === "development"
    ? "http://localhost"
    : window.location.hostname;

const baseUrl =
  process.env.NODE_ENV === "development" ? `${base}:${port}/api/` : "/api/";
const Config = {
  baseUrl,
  API: {
    logIn: `${baseUrl}login`,
    logOut: `${baseUrl}logout`,
    register: `${baseUrl}user/register`,
    refresh: `${baseUrl}refresh`,
    users: `${baseUrl}users`,
    locations: `${baseUrl}locations`,
    tables: `${baseUrl}tables`,
    forms: `${baseUrl}forms`,
    predmeti: `${baseUrl}predmeti`,
    reports: `${baseUrl}report`,
    azs: `${baseUrl}azs`,
    upload: `${baseUrl}upload`,
    images: `${baseUrl}images`
  }
};

export default Config;
