import { REQUEST_FORMS, RECEIVE_FORMS } from "../actions/types";

const initialState = {
  isFetching: false,
  formGroups: []
};

const formGroups = (state = initialState, action) => {
  const actions = {
    [REQUEST_FORMS]: () => ({ ...initialState, isFetching: true }),
    [RECEIVE_FORMS]: () => ({
      ...state,
      formGroups: action.payload,
      isFetching: false
    })
  };
  return actions.hasOwnProperty(action.type) ? actions[action.type]() : state;
};
export default formGroups;
