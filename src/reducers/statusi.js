import { SET_STATUSI } from "../actions/types";

const initialState = [
  {
    prs_id: 10,
    prs_oznaka: "PRI",
    prs_naziv: "Priprema",
    prs_priprema: "D",
    prs_cekanje: "N",
    prs_zakljucen: "N",
    prs_neusag: "N"
  },
  {
    prs_id: 11,
    prs_oznaka: "NEU",
    prs_naziv: "Neusaglašenost",
    prs_priprema: "N",
    prs_cekanje: "D",
    prs_zakljucen: "N",
    prs_neusag: "D"
  },
  {
    prs_id: 12,
    prs_oznaka: "AS1",
    prs_naziv: "Poslati podaci u AS",
    prs_priprema: "N",
    prs_cekanje: "D",
    prs_zakljucen: "N",
    prs_neusag: "N"
  },
  {
    prs_id: 13,
    prs_oznaka: "CE1",
    prs_naziv: "Čeka se dokumentacija",
    prs_priprema: "N",
    prs_cekanje: "D",
    prs_zakljucen: "N",
    prs_neusag: "N"
  },
  {
    prs_id: 14,
    prs_oznaka: "CE2",
    prs_naziv: "Čekaju se uplate",
    prs_priprema: "N",
    prs_cekanje: "D",
    prs_zakljucen: "N",
    prs_neusag: "N"
  },
  {
    prs_id: 88,
    prs_oznaka: "ZAK",
    prs_naziv: "Zaključen",
    prs_priprema: "N",
    prs_cekanje: "N",
    prs_zakljucen: "D",
    prs_neusag: "N"
  }
];

const statusi = (state = initialState, action) => {
  const actions = {
    [SET_STATUSI]: () => action.payload
  };
  return actions.hasOwnProperty(action.type) ? actions[action.type]() : state;
};
export default statusi;
