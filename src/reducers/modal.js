import {
  OPEN_MODAL,
  CLOSE_MODAL,
  CHANGE_BUTTONS_STATE
} from "../actions/types";

const initialState = {
  isHidden: true,
  title: "",
  message: "",
  translateData: {},
  iconType: "",
  buttons: [],
  template: null
};

const modal = (state = initialState, action) => {
  const actions = {
    [CLOSE_MODAL]: () => ({ ...initialState, isHidden: true }),
    [OPEN_MODAL]: () => ({
      ...initialState,
      ...action.payload,
      isHidden: false
    }),
    [CHANGE_BUTTONS_STATE]: () => ({ ...state, buttons: action.payload })
  };
  return actions.hasOwnProperty(action.type) ? actions[action.type]() : state;
};
export default modal;
