import { RECEIVE_USERS, REQUEST_USERS } from "../actions/types";

const initialState = {
  isFetching: false,
  users: [],
  editedUser: { ime: "", prezime: "", id: "" }
};

const users = (state = initialState, action) => {
  const actions = {
    [REQUEST_USERS]: () => ({ ...state, isFetching: true }),
    [RECEIVE_USERS]: () => ({
      ...state,
      users: action.payload,
      isFetching: false
    })
  };
  return actions.hasOwnProperty(action.type) ? actions[action.type]() : state;
};
export default users;
