import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import login from "./login";
import modal from "./modal";
import users from "./users";
import formGroups from "./formGroups";
import grid from "./grid";
import predmeti from "./predmeti";
import reports from "./reports";
import usluge from "./usluge";
import statusi from "./statusi";
import documents from "./documents";

const reducers = history =>
  combineReducers({
    documents,
    formGroups,
    grid,
    login,
    modal,
    predmeti,
    reports,
    statusi,
    users,
    usluge,
    router: connectRouter(history)
  });
export default reducers;
