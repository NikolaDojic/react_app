import {
  RECEIVE_LOGIN,
  REQUEST_LOGIN,
  FINISHED_LOGIN_REQUEST,
  SET_CLIENT_IP,
  SET_IS_REFRESHING_TOKEN,
  SET_LOGGED_IN,
  SET_LOGIN_PASSWORD,
  SET_LOGIN_USERNAME
} from "../actions/types";

const initialState = {
  isFetching: false,
  username: "",
  password: "",
  loggedIn: false,
  isRefreshing: false,
  publicAddress: ""
};

const login = (state = initialState, action) => {
  const actions = {
    [REQUEST_LOGIN]: () => ({ ...state, isFetching: true }),
    [RECEIVE_LOGIN]: () => ({ ...state, ...action.payload }),
    [SET_CLIENT_IP]: () => ({ ...state, publicAddress: action.payload }),
    [SET_LOGIN_USERNAME]: () => ({ ...state, username: action.payload }),
    [SET_LOGIN_PASSWORD]: () => ({ ...state, password: action.payload }),
    [SET_LOGGED_IN]: () => ({ ...state, loggedIn: action.payload }),
    [FINISHED_LOGIN_REQUEST]: () => ({
      ...state,
      isFetching: false
    }),
    [SET_IS_REFRESHING_TOKEN]: () => ({
      ...state,
      isRefreshing: action.payload
    })
  };
  return actions.hasOwnProperty(action.type) ? actions[action.type]() : state;
};
export default login;
