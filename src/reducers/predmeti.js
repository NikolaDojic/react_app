import {
  ADD_SELECT_OPTION,
  CLEAR_SELECT_OPTIONS,
  RECEIVE_FORM_FILTER_DSC,
  REQUEST_OPTIONS,
  REQUEST_OPTIONS_FINISHED,
  REQUEST_PREDMET,
  REQUEST_PREDMET_FINISHED,
  SET_IS_CREATING_P_ITEM,
  SET_PREDMET_VISIBLE,
  SET_SELECT_OPTIONS,
  SET_SERT_VISIBLE
} from "../actions/types";

const initialState = {
  isFetching: false,
  selectOptions: {},
  filterDescriptions: {},
  isCreating: {},
  isFetchingOptions: false,
  isVisible: false,
  isSertVisible: false
};

const formGroups = (state = initialState, action) => {
  const actions = {
    [SET_SELECT_OPTIONS]: () => ({
      ...state,
      selectOptions: { ...state.selectOptions, ...action.payload }
    }),
    [ADD_SELECT_OPTION]: () => ({
      ...state,
      selectOptions: {
        ...state.selectOptions,
        [action.payload.column]: [
          ...(state.selectOptions[action.payload.column] || []),
          action.payload.option
        ]
      }
    }),
    [CLEAR_SELECT_OPTIONS]: () => ({
      ...state,
      selectOptions: { ...state.selectOptions, [action.payload]: [] }
    }),
    [RECEIVE_FORM_FILTER_DSC]: () => ({
      ...state,
      filterDescriptions: action.payload
    }),
    [SET_IS_CREATING_P_ITEM]: () => ({
      ...state,
      isCreating: { ...state.isCreating, ...action.payload }
    }),
    [REQUEST_PREDMET]: () => ({ ...state, isFetching: true }),
    [REQUEST_PREDMET_FINISHED]: () => ({ ...state, isFetching: false }),
    [REQUEST_OPTIONS]: () => ({ ...state, isFetchingOptions: true }),
    [REQUEST_OPTIONS_FINISHED]: () => ({ ...state, isFetchingOptions: false }),
    [SET_PREDMET_VISIBLE]: () => ({
      ...state,
      isVisible: action.payload,
      selectOptions: {}
    }),
    [SET_SERT_VISIBLE]: () => ({
      ...state,
      isSertVisible: action.payload,
      selectOptions: {}
    })
  };
  return actions.hasOwnProperty(action.type) ? actions[action.type]() : state;
};
export default formGroups;
