import { SET_DOCUMENTS } from "../actions/types";

const initialState = [];

const documents = (state = initialState, action) => {
  const actions = {
    [SET_DOCUMENTS]: () => action.payload
  };
  return actions.hasOwnProperty(action.type) ? actions[action.type]() : state;
};
export default documents;
