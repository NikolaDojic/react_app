import { SET_USLUGE } from "../actions/types";

const initialState = [];

const usluge = (state = initialState, action) => {
  const actions = {
    [SET_USLUGE]: () => action.payload
  };
  return actions.hasOwnProperty(action.type) ? actions[action.type]() : state;
};
export default usluge;
