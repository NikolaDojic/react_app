import {
  FINISH_REPORT_REQUEST,
  REQUEST_REPORT,
  SET_SELECTED_REPORT,
  SET_SELECTED_AZS,
  SET_REPORTS
} from "../actions/types";

const initialState = {
  isFetching: false,
  selectedReport: "",
  selectedAzs: "uvoz",
  reports: {}
};

const reports = (state = initialState, action) => {
  const actions = {
    [REQUEST_REPORT]: () => ({ ...state, isFetching: true }),
    [FINISH_REPORT_REQUEST]: () => ({
      ...state,
      isFetching: false
    }),
    [SET_SELECTED_REPORT]: () => ({
      ...state,
      selectedReport: action.payload
    }),
    [SET_SELECTED_AZS]: () => ({
      ...state,
      selectedAzs: action.payload
    }),
    [SET_REPORTS]: () => ({
      ...state,
      reports: action.payload
    }),
    "@@router/LOCATION_CHANGE": () => ({
      ...state,
      selectedReport: ""
    })
  };
  return actions.hasOwnProperty(action.type) ? actions[action.type]() : state;
};
export default reports;
