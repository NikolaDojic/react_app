import {
  APPEND_DATA,
  CHANGE_EDITED_ITEM,
  CLEAR_FILTERS,
  DELETE_COLUMNS,
  DELETE_DATA,
  DELETE_ITEM,
  DELETE_PRIMARY_KEY,
  DELETE_SELECTED_ROW,
  FINISH_DATA_REQUEST,
  RECEIVE_COLUMNS,
  RECEIVE_DATA,
  RECEIVE_ITEM,
  REQUEST_GRID_DATA,
  SET_CURRENT_TABLE,
  SET_DETAILS,
  SET_EDITED_ITEM,
  SET_FILTERS,
  SET_PRIMARY_KEY,
  SET_SELECTED_ROW,
  SET_ACTIVE_TABLE
} from "../actions/types";
import { getCurrent } from "../utils";

const initialState = {
  data: {},
  columns: {},
  isFetching: 0,
  editedItem: {},
  selectedRow: {},
  primaryKey: {},
  filters: {},
  currentTable: "",
  activeTable: "",
  details: []
};

const grid = (state = initialState, action) => {
  const actions = {
    [FINISH_DATA_REQUEST]: () => ({
      ...state,
      isFetching: state.isFetching - 1
    }),
    [REQUEST_GRID_DATA]: () => ({
      ...state,
      isFetching: state.isFetching + 1
    }),
    [RECEIVE_COLUMNS]: () => ({
      ...state,
      columns: { ...state.columns, ...action.payload },
      isFetching: state.isFetching - 1
    }),
    [RECEIVE_DATA]: () => ({
      ...state,
      data: { ...state.data, ...action.payload },
      isFetching: state.isFetching - 1
    }),
    [APPEND_DATA]: () => ({
      ...state,
      data: {
        ...state.data,
        [action.payload.tableName]: [
          ...state.data[action.payload.tableName],
          ...action.payload.data
        ]
      }
    }),
    [SET_EDITED_ITEM]: () => {
      let item = action.payload;
      if (typeof action.payload !== "object") {
        item = getCurrent(action.payload, [...(state.data || [])]);
      }
      return { ...state, editedItem: item };
    },
    [SET_SELECTED_ROW]: () => ({
      ...state,
      selectedRow: { ...state.selectedRow, ...action.payload }
    }),
    [RECEIVE_ITEM]: () => {
      const data = state.data[action.payload.tableName].some(item => {
        const keys = state.primaryKey[action.payload.tableName];
        return keys.every(
          key => action.payload.item[key] == item[key] //eslint-disable-line
        );
      })
        ? state.data[action.payload.tableName].map(item => {
            const keys = state.primaryKey[action.payload.tableName];
            return keys.every(
              key => action.payload.item[key] == item[key] //eslint-disable-line
            )
              ? action.payload.item
              : item;
          })
        : [...state.data[action.payload.tableName], action.payload.item];
      return {
        ...state,
        data: { ...state.data, [action.payload.tableName]: data }
      };
    },
    [DELETE_ITEM]: () => ({
      ...state,
      data: {
        ...state.data,
        [action.payload.tableName]: state.data[action.payload.tableName].filter(
          item =>
            !state.primaryKey[action.payload.tableName].every(
              key => item[key] === action.payload.item[key]
            )
        )
      }
    }),
    [DELETE_COLUMNS]: () => {
      let columns = { ...state.columns };
      let primaryKey = { ...state.primaryKey };
      delete columns[action.payload];
      delete primaryKey[action.payload];
      return { ...state, columns, primaryKey };
    },
    [DELETE_DATA]: () => {
      let data = { ...state.data };
      delete data[action.payload];
      return { ...state, data };
    },
    [DELETE_PRIMARY_KEY]: () => {
      let primaryKey = { ...state.primaryKey };
      delete primaryKey[action.payload];
      return { ...state, primaryKey };
    },
    [DELETE_SELECTED_ROW]: () => {
      let selectedRow = { ...state.selectedRow };
      delete selectedRow[action.payload];
      return { ...state, selectedRow };
    },
    [CHANGE_EDITED_ITEM]: () => ({
      ...state,
      editedItem: { ...state.editedItem, ...action.payload }
    }),
    [SET_PRIMARY_KEY]: () => ({
      ...state,
      primaryKey: { ...state.primaryKey, ...action.payload }
    }),
    [SET_FILTERS]: () => ({
      ...state,
      filters: {
        ...state.filters,
        [action.payload.activeTable]: action.payload.filters
      }
    }),
    [CLEAR_FILTERS]: () => ({
      ...state,
      filters: { ...state.filters, [action.payload]: {} }
    }),

    [SET_CURRENT_TABLE]: () => ({ ...state, currentTable: action.payload }),
    [SET_DETAILS]: () => ({ ...state, details: action.payload }),
    [SET_ACTIVE_TABLE]: () => ({ ...state, activeTable: action.payload })
  };
  return actions.hasOwnProperty(action.type) ? actions[action.type]() : state;
};
export default grid;
